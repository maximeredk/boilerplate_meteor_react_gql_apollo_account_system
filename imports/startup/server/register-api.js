
import { ApolloServer, gql } from 'apollo-server-express'
import { WebApp } from 'meteor/webapp'
import { getUser } from 'meteor/apollo'
import cors from 'cors';
import merge from 'lodash/merge';

import UserSchema from '../api/user/User.graphql';
import UserResolvers from '../api/user/resolvers.js';

import CalendarSchema from '../api/calendar/Calendar.graphql';
import CalendarResolvers from '../api/calendar/resolvers.js';

import ConsomableSchema from '../api/consomable/Consomable.graphql';
import ConsomableResolvers from '../api/consomable/resolvers.js';

import CommandeSchema from '../api/commande/Commande.graphql';
import CommandeResolvers from '../api/commande/resolvers.js';

import FournisseurSchema from '../api/fournisseur/Fournisseur.graphql';
import FournisseurResolvers from '../api/fournisseur/resolvers.js';

import HistoriqueConsoSchema from '../api/historiqueConso/HistoriqueConso.graphql';
import HistoriqueConsoResolvers from '../api/historiqueConso/resolvers.js';

import RoleSchema from '../api/role/Role.graphql';
import RoleResolvers from '../api/role/resolvers.js';

import DemandeConsoSchema from '../api/demandeConso/DemandeConso.graphql';
import DemandeConsoResolvers from '../api/demandeConso/resolvers.js';

import SpecificSchema from '../api/specific/Specific.graphql';
import SpecificResolvers from '../api/specific/resolvers.js';

import DeliveryConsoSchema from '../api/deliveryConso/DeliveryConso.graphql';

import DeliverySchema from '../api/delivery/Delivery.graphql';

import CommandeConsoSchema from '../api/commandeConso/CommandeConso.graphql';

import InitSchema from '../api/init/Init.graphql';
import InitResolvers from '../api/init/resolvers.js';

// #0121

const typeDefs = [
    UserSchema,
    CalendarSchema,
    ConsomableSchema,
    CommandeSchema,
    FournisseurSchema,
    HistoriqueConsoSchema,
    DeliveryConsoSchema,
    DeliverySchema,
    RoleSchema,
    DemandeConsoSchema,
    SpecificSchema,
    CommandeConsoSchema,
    InitSchema
];

const resolvers = merge(
    UserResolvers,
    CalendarResolvers,
    ConsomableResolvers,
    CommandeResolvers,
    FournisseurResolvers,
    HistoriqueConsoResolvers,
    RoleResolvers,
    DemandeConsoResolvers,
    SpecificResolvers,
    InitResolvers
);

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: async ({ req }) => {
        const token = req.headers["meteor-login-token"] || '';
        const user = await getUser(token);
        return { user };
    }
})

const corsOptions = {
    origin: 'http://localhost:3000',
    credentials: true
}

server.applyMiddleware({
    app: WebApp.connectHandlers,
    path: '/graphql',
    cors: corsOptions
})

WebApp.rawConnectHandlers.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Authorization,Content-Type");
    return next();
});
  
WebApp.connectHandlers.use('/graphql', (req, res) => {
    if (req.method === 'GET') {
        res.end()
    }
})