import Fournisseurs from '../fournisseur/fournisseurs';
import Consomables from '../consomable/consomables';
import Commandes from '../commande/commandes';
import CommandeConsos from '../commandeConso/commandesConso';
import Roles from '../role/roles';
import Deliveries from '../delivery/delivery';
import DeliveryConsos from '../deliveryConso/deliveryConso';
import DemandeConsos from '../demandeConso/demandeConso';
import HistoriqueConsos from '../historiqueConso/historiqueConso';
import Specifics from '../specific/specifics';
import { Mongo } from 'meteor/mongo';

export default {
    Query : {
        exportFullDB(obj, args,{userId}){
            return{
                users:Meteor.users.find().fetch().map(x=>JSON.stringify(x)),
                roles:Roles.find().fetch().map(x=>JSON.stringify(x)),
                consomables:Consomables.find().fetch().map(x=>JSON.stringify(x)),
                fournisseurs:Fournisseurs.find().fetch().map(x=>JSON.stringify(x)),
                demandeConsos:DemandeConsos.find().fetch().map(x=>JSON.stringify(x)),
                specifics:Specifics.find().fetch().map(x=>JSON.stringify(x)),
                commandes:Commandes.find().fetch().map(x=>JSON.stringify(x)),
                commandeConsos:CommandeConsos.find().fetch().map(x=>JSON.stringify(x)),
                deliveries:Deliveries.find().fetch().map(x=>JSON.stringify(x)),
                deliveryConsos:DeliveryConsos.find().fetch().map(x=>JSON.stringify(x)),
                historiqueConsos:HistoriqueConsos.find().fetch().map(x=>JSON.stringify(x)),
            }
        }
    },
    Mutation:{
        initFournisseur(obj, {site,fournisseur},{user}){
            if(user._id){
                Fournisseurs.insert({
                    _id: new Mongo.ObjectID(),
                    name: fournisseur,
                    deliveryDelay: 1,
                    site:site
                });
                return {
                    consomables:Consomables.find({site:site}).fetch().length,
                    fournisseurs:Fournisseurs.find({site:site}).fetch().length,
                }
            }
            throw new Error('Unauthorized')
        },
        initConso(obj, {site,consomable},{user}){
            if(user._id){
                const consoParsed = JSON.parse(consomable);
                if(consoParsed['Prix'] == null || consoParsed['Prix'] == undefined){
                    consoParsed['Prix'] = 0;
                }
                let des;
                let packagingFormat;
                let refS;
                if(consoParsed['Désignations'] || consoParsed['Désignations']==0){
                    des = consoParsed['Désignations'].toString();
                }else{
                    des = consoParsed['Modèle']
                }
                if(consoParsed['Conditionnement']){
                    packagingFormat = consoParsed['Conditionnement'].toString();
                }else{
                    packagingFormat = 'Aucune information';
                }
                if(consoParsed['refS']){
                    refS = consoParsed['refS'].toString()
                }else{
                    refS = "-";
                }
                let fourn = Fournisseurs.find().fetch().filter(f=> f.name == consoParsed['Fournisseurs'])[0];
                // INSERTING CONSOMABLES
                Consomables.insert({
                    _id: new Mongo.ObjectID(),
                    ref: consoParsed['ref'].toString(),
                    refS: refS,
                    designation: des,
                    service: consoParsed['SERVICE'],
                    categorie: consoParsed['Catégorie'],
                    modele: consoParsed['Modèle'],
                    currentlyStored:0,
                    avCS:1,
                    packagingFormat: packagingFormat,
                    minOrder:1,
                    fournisseur:fourn._id._str,
                    site:site,
                    prix:parseFloat(consoParsed['Prix']).toFixed(2)
                });
                return {
                    consomables:Consomables.find({site:site}).fetch().length,
                    fournisseurs:Fournisseurs.find({site:site}).fetch().length,
                }
            }
            throw new Error('Unauthorized');
        },
        cleanSite(obj, {site,collection},{user}){
            if(user._id){
                if(collection == "commandes"){
                    Commandes.remove({site:site});
                    return Commandes.find({site:site}).fetch().length;
                }
                if(collection == "commandeConsos"){
                    CommandeConsos.remove({site:site});
                    return CommandeConsos.find({site:site}).fetch().length;
                }
                if(collection == "consomables"){
                    Consomables.remove({site:site});
                    return Consomables.find({site:site}).fetch().length;
                }
                if(collection == "deliveries"){
                    Deliveries.remove({site:site});
                    return Deliveries.find({site:site}).fetch().length;
                }
                if(collection == "deliveryConsos"){
                    DeliveryConsos.remove({site:site});
                    return DeliveryConsos.find({site:site}).fetch().length;
                }
                if(collection == "fournisseurs"){
                    Fournisseurs.remove({site:site});
                    return Fournisseurs.find({site:site}).fetch().length;
                }
                if(collection == "demandeConsos"){
                    DemandeConsos.remove({site:site});
                    return DemandeConsos.find({site:site}).fetch().length;
                }
                if(collection == "historiqueConsos"){
                    HistoriqueConsos.remove({site:site});
                    return HistoriqueConsos.find({site:site}).fetch().length;
                }
                if(collection == "specifics"){
                    Specifics.remove({site:site});
                    return Specifics.find({site:site}).fetch().length;
                }
            }
            throw new Error('Unauthorized');
        },
        clean(obj, {collection},{user}){
            if(user._id){
                if(collection == "users"){
                    Meteor.users.remove({"settings.isOwner":false});
                    return Meteor.users.find().fetch().length;
                }
                if(collection == "roles"){
                    Roles.remove({});
                    return Roles.find().fetch().length;
                }
                if(collection == "commandes"){
                    Commandes.remove({});
                    return Commandes.find().fetch().length;
                }
                if(collection == "commandeConsos"){
                    CommandeConsos.remove({});
                    return CommandeConsos.find().fetch().length;
                }
                if(collection == "consomables"){
                    Consomables.remove({});
                    return Consomables.find().fetch().length;
                }
                if(collection == "deliveries"){
                    Deliveries.remove({});
                    return Deliveries.find().fetch().length;
                }
                if(collection == "deliveryConsos"){
                    DeliveryConsos.remove({});
                    return DeliveryConsos.find().fetch().length;
                }
                if(collection == "fournisseurs"){
                    Fournisseurs.remove({});
                    return Fournisseurs.find().fetch().length;
                }
                if(collection == "demandeConsos"){
                    DemandeConsos.remove({});
                    return DemandeConsos.find().fetch().length;
                }
                if(collection == "historiqueConsos"){
                    HistoriqueConsos.remove({});
                    return HistoriqueConsos.find().fetch().length;
                }
                if(collection == "specifics"){
                    Specifics.remove({});
                    return Specifics.find().fetch().length;
                }
            }
            throw new Error('Unauthorized');
        },
        writeEntry(obj, {collection,payload},{user}){
            if(user._id){
                payload = JSON.parse(payload)
                if(collection == "users"){
                    if(!payload.settings.isOwner){
                        Meteor.users.insert(payload);
                    }
                    return Meteor.users.find().fetch().length;
                }
                if(payload._id._str){
                    payload._id = new Mongo.ObjectID(payload._id._str);
                }else{
                    payload._id = new Mongo.ObjectID(payload._id);
                }
                if(collection == "roles"){
                    Roles.insert(payload);
                    return Roles.find().fetch().length;
                }
                if(collection == "commandes"){
                    payload.creationDate = new Date(payload.creationDate)
                    payload.orderDate = new Date(payload.orderDate)
                    Commandes.insert(payload);
                    return Commandes.find().fetch().length;
                }
                if(collection == "commandeConsos"){
                    CommandeConsos.insert(payload);
                    return CommandeConsos.find().fetch().length;
                }
                if(collection == "consomables"){
                    Consomables.insert(payload);
                    return Consomables.find().fetch().length;
                }
                if(collection == "deliveries"){
                    Deliveries.insert(payload);
                    return Deliveries.find().fetch().length;
                }
                if(collection == "deliveryConsos"){
                    DeliveryConsos.insert(payload);
                    return DeliveryConsos.find().fetch().length;
                }
                if(collection == "fournisseurs"){
                    Fournisseurs.insert(payload);
                    return Fournisseurs.find().fetch().length;
                }
                if(collection == "demandeConsos"){
                    DemandeConsos.insert(payload);
                    return DemandeConsos.find().fetch().length;
                }
                if(collection == "historiqueConsos"){
                    HistoriqueConsos.insert(payload);
                    return HistoriqueConsos.find().fetch().length;
                }
                if(collection == "specifics"){
                    Specifics.insert(payload);
                    return Specifics.find().fetch().length;
                }
            }
            throw new Error('Unauthorized');
        }
    }
}
