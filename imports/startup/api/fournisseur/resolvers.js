import Fournisseurs from './fournisseurs';

export default {
    Query : {
        fournisseurs(obj, {site}){
            return Fournisseurs.find({site:site}).fetch() || {};
        }
    },
    Mutation:{
        addFournisseur(obj, {name,site},{user}){
            if(user._id){
                Fournisseurs.insert({
                    _id:new Mongo.ObjectID(),
                    name:name
                });
                return Fournisseurs.find({site:site}).fetch() || {};
            }
            throw new Error('Unauthorized');
        },
        editFournisseur(obj,{_id,name,deliveryDelay,site},{user}){
            if(user._id){
                Fournisseurs.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "name":name,
                            "deliveryDelay":deliveryDelay
                        }
                    }
                );
                return Fournisseurs.find({site:site}).fetch() || {};
            }
            throw new Error('Unauthorized');
        },
        removeFournisseur(obj, {_id,site},{user}){
            if(user._id){
                Fournisseurs.remove(_id);
                return Fournisseurs.find({site:site}).fetch() || {};
            }
            throw new Error('Unauthorized')
        }
    }
}