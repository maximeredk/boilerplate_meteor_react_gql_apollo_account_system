import Consomables from './consomables';
import Fournisseurs from '../fournisseur/fournisseurs';
import HistoriqueConso from '../historiqueConso/historiqueConso';
import { Mongo } from 'meteor/mongo';
export default {
    Query : {
        consomables(obj, {site}){
            let consomables = Consomables.find({site:site}).fetch() || {};
            consomables.forEach((c,i) => {
                if(c.categorie == null){
                    c.categorie = ""
                }
                if(c.modele == null){
                    c.modele = ""
                }
                if(c.fournisseur != null && c.fournisseur.length > 0){
                    consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                }else{
                    consomables[i].fournisseur = {_id:""};
                }
            });
            return consomables;
        },
        usualConsomables(obj,{ids,site}){
            if(ids===null){
                ids =[]
            }
            let consomables = Consomables.find({site:site,_id : { $in : ids.map(_id => new Mongo.ObjectID(_id)) } }).fetch() || {};
            consomables.forEach((c,i) => {
                if(c.categorie == null){
                    c.categorie = ""
                }
                if(c.modele == null){
                    c.modele = ""
                }
                if(c.fournisseur != null && c.fournisseur.length > 0){
                    consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                }else{
                    consomables[i].fournisseur = {_id:""};
                }
            });
            return consomables;
        }
    },
    Mutation:{
        addConsomable(obj, {ref,refS,designation,categorie,modele,prix,avCS,fournisseur,site,packagingFormat},{user}){
            if(user._id){
                Consomables.insert({
                    _id: new Mongo.ObjectID(),
                    ref:ref,
                    refS:refS,
                    designation:designation,
                    categorie:categorie,
                    modele:modele,
                    currentlyStored:0,
                    avCS:avCS,
                    prix:prix,
                    packagingFormat:packagingFormat,
                    minOrder:0,
                    fournisseur:fournisseur,
                    site:site
                });
                let consomables = Consomables.find({site:site}).fetch() || {};
                consomables.forEach((c,i) => {
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                return consomables;
            }
            throw new Error('Unauthorized');
        },
        editConsomable(obj, {_id,ref,refS,minOrder,categorie,designation,modele,avCS,prix,packagingFormat,fournisseur,site},{user}){
            if(user._id){
                Consomables.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "ref":ref,
                            "refS":refS,
                            "minOrder":minOrder,
                            "categorie":categorie,
                            "designation":designation,
                            "modele":modele,
                            "avCS":avCS,
                            "prix":prix,
                            "packagingFormat":packagingFormat,
                            "fournisseur":fournisseur
                        }
                    }
                );
                let consomables = Consomables.find({site:site}).fetch() || {};
                consomables.forEach((c,i) => {
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                return consomables;
            }
            throw new Error('Unauthorized');
        },
        removeConsomable(obj, {_id,site},{user}){
            if(user._id){
                Consomables.remove(new Mongo.ObjectID(_id));
                let consomables = Consomables.find({site:site}).fetch() || {};
                consomables.forEach((c,i) => {
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                return consomables;
            }
            throw new Error('Unauthorized')
        },
        addToStock(obj, {_id,quantity,isCorrection,note,site},{user}){
            if(user._id){
                const cons = Consomables.findOne({_id:new Mongo.ObjectID(_id)});
                Consomables.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "currentlyStored":cons.currentlyStored + quantity
                        }
                    }
                );
                HistoriqueConso.insert({
                    _id: new Mongo.ObjectID(),
                    consomable:_id,
                    entryDay: new Date().getDate(),
                    entryMonth: new Date().getMonth(),
                    entryYear: new Date().getFullYear(),
                    entryHour: new Date().getHours(),
                    entryMinute: new Date().getMinutes(),
                    entrySecond: new Date().getSeconds(),
                    quantity: quantity,
                    type: isCorrection+"/+",
                    note: note,
                    site:site
                });
                let consomables = Consomables.find({site:site}).fetch() || {};
                consomables.forEach((c,i) => {
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                return consomables;
            }
            throw new Error('Unauthorized');
        },
        takeFromStock(obj, {_id,quantity,isCorrection,note,site},{user}){
            if(user._id){
                const cons = Consomables.findOne({_id:new Mongo.ObjectID(_id)});
                Consomables.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "currentlyStored":cons.currentlyStored - quantity
                        }
                    }
                );
                HistoriqueConso.insert({
                    _id: new Mongo.ObjectID(),
                    consomable:_id,
                    entryDay: new Date().getDate(),
                    entryMonth: new Date().getMonth(),
                    entryYear: new Date().getFullYear(),
                    entryHour: new Date().getHours(),
                    entryMinute: new Date().getMinutes(),
                    entrySecond: new Date().getSeconds(),
                    quantity: quantity,
                    type: isCorrection +"/-",
                    note: note,
                    site:site
                });
                let consomables = Consomables.find({site:site}).fetch() || {};
                consomables.forEach((c,i) => {
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                return consomables;
            }
            throw new Error('Unauthorized');
        },
        addToUserUsual(obj,{userId,_id},{user}){
            if(user._id){
                Meteor.users.update(
                    {"_id":userId},
                    {
                        "$push": {
                            "usualConsosIds": _id
                        }
                    }
                )
            }else{
                throw new Error('Unauthorized')
            }
        },
        removeFromUserUsual(obj,{userId,_id,site},{user}){
            if(user._id){
                Meteor.users.update(
                    {"_id":userId},
                    {
                        "$pull": {
                            "usualConsosIds": _id
                        }
                    }
                )
                const usualConsosIds =  Meteor.users.findOne({_id:userId}).usualConsosIds.map(_id => new Mongo.ObjectID(_id))
                return usualConsosIds;
            }else{
                throw new Error('Unauthorized');
            }
        }
    }
}