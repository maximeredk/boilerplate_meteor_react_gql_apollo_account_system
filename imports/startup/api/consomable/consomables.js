import { Mongo } from 'meteor/mongo';

const Consomables = new Mongo.Collection("consomables");

export default Consomables;
