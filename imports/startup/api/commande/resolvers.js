import Commandes from './commandes';
import Consomables from '../consomable/consomables';
import Fournisseurs from '../fournisseur/fournisseurs';
import HistoriqueConso from '../historiqueConso/historiqueConso';
import DeliveryConso from '../deliveryConso/deliveryConso';
import Delivery from '../delivery/delivery';
import CommandeConso from '../commandeConso/commandesConso';
import DemandeConso from '../demandeConso/demandeConso';

export default {
    Query : {
        commandes(obj,{site}){
            const commandes = Commandes.find({site:site}).fetch() || {};
            commandes.forEach(c => {
                c.creationDay = c.creationDate.getDate();
                c.creationMonth = c.creationDate.getMonth();
                c.creationYear = c.creationDate.getFullYear();
                if(c.orderDate != null){
                    c.orderDay = c.orderDate.getDate();
                    c.orderMonth = c.orderDate.getMonth();
                    c.orderYear = c.orderDate.getFullYear();
                }
                c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                c.deliveries.forEach(del => {
                    del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                    del.deliveryConsos.forEach(delC => {
                        delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                    })
                })
                c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                c.commandeConsos.forEach(cons => {
                    cons.fromNote = JSON.stringify(cons.fromNote);
                    cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                })
            });
            return commandes;
        }
    },
    Mutation:{
        createCommande(obj, {consomables,fournisseur,internalRef,site},{user}){
            if(user._id){
                commandeId = Commandes.insert({
                    _id:new Mongo.ObjectID(),
                    fournisseur:fournisseur,
                    internalRef:internalRef,
                    creationDate: new Date(),
                    orderDate: null,
                    archived: false,
                    validatedDSL: false,
                    status:1,
                    site:site
                });
                JSON.parse(consomables).forEach(c => {
                    CommandeConso.insert({
                        _id:new Mongo.ObjectID(),
                        commande:commandeId._str,
                        nbPackages:c.quantity,
                        received:0,
                        consomable:c._id,
                        fromNote:c.demandes
                    });
                    c.demandes.forEach(d =>{
                        let rmDC = DemandeConso.remove(new Mongo.ObjectID(d._id));
                    });
                })

                let cons = [];
                let consomablesId = [];
                let fournisseurs = Fournisseurs.find({site:site}).fetch();
                let demandesConso = DemandeConso.find({site:site}).fetch() || {};
                demandesConso.forEach(c => {
                    consomablesId.push(c.consomable);
                });
                [...new Set(consomablesId)].map(cId=>{
                    cons.push(Consomables.findOne(new Mongo.ObjectID(cId)))
                })
                cons.forEach((c,i) => {
                    c.demandes=[];
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        cons[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        cons[i].fournisseur = {_id:""};
                    }
                });
                fournisseurs.forEach(f => {
                    f.agglomeratedConsomable = [];
                });
                demandesConso.map(dC=>{
                    cons.find(x=>x._id == dC.consomable).demandes.push(dC);
                })
                cons.map(c=>{
                    fournisseurs.find(x=>x._id._str == c.fournisseur._id._str).agglomeratedConsomable.push(c);
                })
                return fournisseurs.filter(x=>x.agglomeratedConsomable.length>0);
            }
            throw new Error('Unauthorized');
        },
        editCommande(obj, {_id,site},{user}){
            if(user._id){
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveryConsos = DeliveryConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }
            throw new Error('Unauthorized');
        },
        removeCommande(obj, {_id,site},{user}){
            if(user._id){
                CommandeConso.remove({commande:_id});
                Delivery.find({commande:_id}).fetch().forEach(d => {
                    DeliveryConso.remove({delivery:d._id._str});
                });
                Delivery.remove({commande:_id});
                Commandes.remove({_id:new Mongo.ObjectID(_id)});
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        archiveCommande(obj, {_id,site},{user}){
            if(user._id){
                const com = Commandes.findOne({_id: new Mongo.ObjectID(_id)})
                Commandes.update(
                    {
                        _id: new Mongo.ObjectID(_id),
                    }, {
                        $set: {
                            "archived":!com.archived
                        }
                    }
                );
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        recepCommande(obj, {_id,quantities,note,site},{user}){
            if(user._id){
                if(note.length < 1){
                    note = "Aucune note de réception";
                }
                const delId = Delivery.insert({
                    _id: new Mongo.ObjectID(),
                    commande: _id,
                    deliveryDay: new Date().getDate(),
                    deliveryMonth: new Date().getMonth(),
                    deliveryYear: new Date().getFullYear(),
                    bl: note
                });
                JSON.parse(quantities).map(consomable=>{
                    DeliveryConso.insert({
                        _id: new Mongo.ObjectID(),
                        delivery: delId._str,
                        quantite: consomable.recepQuantity,
                        consomable: consomable._id
                    });
                    const comCons = CommandeConso.findOne({
                        commande: _id,
                        consomable : consomable._id
                    })
                    comCons.received + consomable.recepQuantity
                    CommandeConso.update({
                        commande: _id,
                        consomable : consomable._id
                    }, {
                        $set: {
                            "received":comCons.received + consomable.recepQuantity,
                        }
                    });
                    const cons = Consomables.findOne({
                        _id : new Mongo.ObjectID(consomable._id)
                    })
                    Consomables.update({
                        _id : new Mongo.ObjectID(consomable._id)
                    }, {
                        $set: {
                            "currentlyStored":cons.currentlyStored + consomable.recepQuantity,
                        }
                    });
                    HistoriqueConso.insert({
                        _id:new Mongo.ObjectID(),
                        consomable: consomable._id,
                        entryDay: new Date().getDate(),
                        entryMonth: new Date().getMonth(),
                        entryYear: new Date().getFullYear(),
                        entryHour: new Date().getHours(),
                        entryMinute: new Date().getMinutes(),
                        entrySecond: new Date().getSeconds(),
                        quantity: consomable.recepQuantity,
                        type: "recep",
                        note: note,
                        site:site
                    });
                })
                if(CommandeConso.find({commande: _id}).fetch().reduce((sum, next) => sum && next.received >= next.nbPackages, true)){
                    Commandes.update(
                        {
                            _id: new Mongo.ObjectID(_id)
                        }, {
                            $set: {
                                "status":5
                            }
                        }
                    );
                }else{
                    if(CommandeConso.find({commande: _id}).fetch().reduce((sum, next) => sum && next.received == 0, true)){
                        Commandes.update(
                            {
                                _id: new Mongo.ObjectID(_id)
                            }, {
                                $set: {
                                    "status":3
                                }
                            }
                        );
                                    }else{
                        Commandes.update(
                            {
                                _id: new Mongo.ObjectID(_id)
                            }, {
                                $set: {
                                    "status":4
                                }
                            }
                        );
                    }
                }
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        removeDelivery(obj, {_id,site},{user}){
            if(user._id){
                const del = Delivery.findOne(new Mongo.ObjectID(_id));
                const commande = del.commande;
                const delC = DeliveryConso.find({delivery:_id}).fetch();
                delC.map(dc=>{
                    const comCons = CommandeConso.findOne({
                        commande: commande._id,
                        consomable : dc.consomable
                    })
                    CommandeConso.update({
                        commande: commande._id,
                        consomable : dc.consomable
                    }, {
                        $set: {
                            "received":comCons.received - dc.quantite,
                        }
                    });
                    const cons = Consomables.findOne({
                        _id : new Mongo.ObjectID(dc.consomable)
                    })
                    Consomables.update({
                        _id : new Mongo.ObjectID(dc.consomable)
                    }, {
                        $set: {
                            "currentlyStored":cons.currentlyStored - dc.quantite,
                        }
                    });
                    //input historique
                    HistoriqueConso.insert({
                        _id:new Mongo.ObjectID(),
                        consomable: dc.consomable,
                        entryDay: new Date().getDate(),
                        entryMonth: new Date().getMonth(),
                        entryYear: new Date().getFullYear(),
                        entryHour: new Date().getHours(),
                        entryMinute: new Date().getMinutes(),
                        entrySecond: new Date().getSeconds(),
                        quantity: dc.quantite,
                        type: "cancel/-",
                        note: "Annulation de la reception",
                        site:site
                    });
                });
                //check status
                if(CommandeConso.find({commande: commande._id}).fetch().reduce((sum, next) => sum && next.received >= next.nbPackages, true)){
                    Commandes.update(
                        {
                            _id: new Mongo.ObjectID(commande)
                        }, {
                            $set: {
                                "status":5
                            }
                        }
                    );
                }else{
                    if(CommandeConso.find({commande: commande._id}).fetch().reduce((sum, next) => sum && next.received == 0, true)){
                        Commandes.update(
                            {
                                _id: new Mongo.ObjectID(commande)
                            }, {
                                $set: {
                                    "status":3
                                }
                            }
                        );
                    }else{
                        Commandes.update(
                            {
                                _id: new Mongo.ObjectID(commande)
                            }, {
                                $set: {
                                    "status":4
                                }
                            }
                        );
                    }
                }
                DeliveryConso.remove({delivery:_id});
                Delivery.remove({_id:new Mongo.ObjectID(_id)});
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        validationDSL(obj, {_id,site},{user}){
            if(user._id){
                Commandes.update(
                    {
                        _id: new Mongo.ObjectID(_id),
                    }, {
                        $set: {
                            "status":2
                        }
                    }
                );
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        shippingCommande(obj, {_id,site},{user}){
            if(user._id){
                Commandes.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "status":3,
                            "orderDate": new Date(),
                        }
                    }
                );
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized')
            }
        },
        editCommandeConso(obj, {_id,commande,quantity,site},{user}){
            if(user._id){
                CommandeConso.update(
                    {
                        _id: new Mongo.ObjectID(_id)
                    }, {
                        $set: {
                            "nbPackages":quantity
                        }
                    }
                );
                if(Commandes.findOne({_id: new Mongo.ObjectID(commande)}).status >= 3){
                    if(CommandeConso.find({commande: commande}).fetch().reduce((sum, next) => sum && next.received >= next.nbPackages, true)){
                        Commandes.update(
                            {
                                _id: new Mongo.ObjectID(commande)
                            }, {
                                $set: {
                                    "status":5
                                }
                            }
                        );
                    }else{
                        if(CommandeConso.find({commande: commande}).fetch().reduce((sum, next) => sum && next.received == 0, true)){
                            Commandes.update(
                                {
                                    _id: new Mongo.ObjectID(commande)
                                }, {
                                    $set: {
                                        "status":3
                                    }
                                }
                            );
                        }else{
                            Commandes.update(
                                {
                                    _id: new Mongo.ObjectID(commande)
                                }, {
                                    $set: {
                                        "status":4
                                    }
                                }
                            );
                        }
                    }
                }
                const commandes = Commandes.find({site:site}).fetch() || {};
                commandes.forEach(c => {
                    c.creationDay = c.creationDate.getDate();
                    c.creationMonth = c.creationDate.getMonth();
                    c.creationYear = c.creationDate.getFullYear();
                    if(c.orderDate != null){
                        c.orderDay = c.orderDate.getDate();
                        c.orderMonth = c.orderDate.getMonth();
                        c.orderYear = c.orderDate.getFullYear();
                    }
                    c.fournisseur = Fournisseurs.findOne(new Mongo.ObjectID(c.fournisseur));
                    c.deliveries = Delivery.find({commande:c._id._str}).fetch() || [];
                    c.deliveries.forEach(del => {
                        del.deliveryConsos = DeliveryConso.find({delivery:del._id._str}).fetch();
                        del.deliveryConsos.forEach(delC => {
                            delC.consomable = Consomables.findOne({_id:new Mongo.ObjectID(delC.consomable)});
                        })
                    })
                    c.commandeConsos = CommandeConso.find({commande:c._id._str}).fetch() || [];
                    c.commandeConsos.forEach(cons => {
                        cons.fromNote = JSON.stringify(cons.fromNote);
                        cons.consomable = Consomables.findOne({_id:new Mongo.ObjectID(cons.consomable)});
                    })
                });
                return commandes;
            }else{
                throw new Error('Unauthorized');
            }
        }
    }
}