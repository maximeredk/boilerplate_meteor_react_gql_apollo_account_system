
import HistoriqueConso from '../historiqueConso/historiqueConso';
import Consomables from '../consomable/consomables';

export default {
    Query : {
        historiqueConso(obj, {site}){
            let historiqueConso = HistoriqueConso.find({site:site}).fetch() || {};
            historiqueConso.forEach(h => {
                h.type = h.type.toString();
                h.consomable = Consomables.findOne({_id:new Mongo.ObjectID(h.consomable)});
            });
            return historiqueConso;
        },
        exportHistorique(obj, {month,year,site}){
            let exportHistorique = HistoriqueConso.find({site:site,entryMonth:month,entryYear:year}).fetch() || {};
            exportHistorique.forEach(h => {
                h.type = h.type.toString();
                h.consomable = Consomables.findOne({_id:new Mongo.ObjectID(h.consomable)});
            });
            return exportHistorique;
        }
    }
}