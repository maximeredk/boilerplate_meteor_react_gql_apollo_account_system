import DemandeConso from './demandeConso';
import Consomables from '../consomable/consomables';
import Fournisseurs from '../fournisseur/fournisseurs';

export default {
    Query : {
        demandesConso(obj,{site}){
            let consomables = [];
            let consomablesId = [];
            let fournisseurs = Fournisseurs.find({site:site}).fetch();
            let demandesConso = DemandeConso.find({site:site}).fetch() || {};
            demandesConso.forEach(c => {
                consomablesId.push(c.consomable);
            });
            [...new Set(consomablesId)].map(cId=>{
                consomables.push(Consomables.findOne(new Mongo.ObjectID(cId)))
            })
            consomables.forEach((c,i) => {
                c.demandes=[];
                if(c.categorie == null){
                    c.categorie = ""
                }
                if(c.modele == null){
                    c.modele = ""
                }
                if(c.fournisseur != null && c.fournisseur.length > 0){
                    consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                }else{
                    consomables[i].fournisseur = {_id:""};
                }
            });
            fournisseurs.forEach(f => {
                f.agglomeratedConsomable = [];
            });
            demandesConso.map(dC=>{
                consomables.find(x=>x._id == dC.consomable).demandes.push(dC);
            })
            consomables.map(c=>{
                fournisseurs.find(x=>x._id._str == c.fournisseur._id._str).agglomeratedConsomable.push(c);
            })
            return fournisseurs.filter(x=>x.agglomeratedConsomable.length>0);
        }
    },
    Mutation:{
        createDemandesConsos(obj, {consosQuantities,from,site},{user}){
            if(user._id){
                JSON.parse(consosQuantities).map(cons=>{
                    DemandeConso.insert({
                        _id:new Mongo.ObjectID(),
                        consomable:cons._id,
                        user:user._id,
                        quantity:(parseInt(cons.expressQuantity) > 0 ? parseInt(cons.expressQuantity) : 0),
                        from:from,
                        entryDay: new Date().getDate(),
                        entryMonth: new Date().getMonth(),
                        entryYear: new Date().getFullYear(),
                        entryHour: new Date().getHours(),
                        entryMinute: new Date().getMinutes(),
                        entrySecond: new Date().getSeconds(),
                        site:site
                    });  
                })
                return true;
            }
            throw new Error('Unauthorized');
        },
        createDemandeConso(obj, {consomable,quantity,from,site},{user}){
            if(user._id){
                DemandeConso.insert({
                    _id:new Mongo.ObjectID(),
                    consomable:consomable,
                    quantity:quantity,
                    user:user._id,
                    from:from,
                    entryDay: new Date().getDate(),
                    entryMonth: new Date().getMonth(),
                    entryYear: new Date().getFullYear(),
                    entryHour: new Date().getHours(),
                    entryMinute: new Date().getMinutes(),
                    entrySecond: new Date().getSeconds(),
                    site:site
                });
                return true;
            }
            throw new Error('Unauthorized');
        },
        removeDemandeConso(obj, {consomable},{user}){
            if(user._id){
                DemandeConso.remove({
                    consomable:consomable
                });
                return true
            }
            throw new Error('Unauthorized');
        },
        deleteDemande(obj, {demande,site},{user}){
            if(user._id){
                DemandeConso.remove({
                    _id: new Mongo.ObjectID(demande)
                });
                let consomables = [];
                let consomablesId = [];
                let fournisseurs = Fournisseurs.find({site:site}).fetch();
                let demandesConso = DemandeConso.find({site:site}).fetch() || {};
                demandesConso.forEach(c => {
                    consomablesId.push(c.consomable);
                });
                [...new Set(consomablesId)].map(cId=>{
                    consomables.push(Consomables.findOne(new Mongo.ObjectID(cId)))
                })
                consomables.forEach((c,i) => {
                    c.demandes=[];
                    if(c.categorie == null){
                        c.categorie = ""
                    }
                    if(c.modele == null){
                        c.modele = ""
                    }
                    if(c.fournisseur != null && c.fournisseur.length > 0){
                        consomables[i].fournisseur = Fournisseurs.findOne({_id:new Mongo.ObjectID(c.fournisseur)});
                    }else{
                        consomables[i].fournisseur = {_id:""};
                    }
                });
                fournisseurs.forEach(f => {
                    f.agglomeratedConsomable = [];
                });
                demandesConso.map(dC=>{
                    consomables.find(x=>x._id == dC.consomable).demandes.push(dC);
                })
                consomables.map(c=>{
                    fournisseurs.find(x=>x._id._str == c.fournisseur._id._str).agglomeratedConsomable.push(c);
                })
                return fournisseurs.filter(x=>x.agglomeratedConsomable.length>0);
            }
            throw new Error('Unauthorized');
        }
    }
}