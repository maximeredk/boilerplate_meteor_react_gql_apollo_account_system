import Roles from './roles';

export default {
    Query : {
        roles(obj, args){
            return Roles.find().fetch() || {};
        },
        getUserForRole(obj, {site,role,main}){
            const r = Roles.findOne({site:site,role:role,main:main});
            if(r !== undefined){
                return Meteor.users.findOne({_id:r.user});
            }else{
                return null;
            }
        },
        getRoleForUser(obj, {user}){
            return Roles.find({user});
        }
    },
    Mutation:{
        setRole(obj, {site,role,targetUser,main},{user}){
            if(user._id){
                Roles.remove({
                    site:site,
                    role:role,
                    main:main
                });
                Roles.insert({
                    _id:new Mongo.ObjectID(),
                    site:site,
                    role:role,
                    user:targetUser,
                    main:main
                });
                const r = Roles.findOne({site:site,role:role,main:main});
                return r;
            }
            throw new Error('Unauthorized');
        }
    }
}