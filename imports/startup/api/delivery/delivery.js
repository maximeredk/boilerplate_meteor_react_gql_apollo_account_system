import { Mongo } from 'meteor/mongo';

const Delivery = new Mongo.Collection("delivery");

export default Delivery;
