import Specifics from './specifics';

export default {
    Query : {
        specifics(obj,{site}){
            const specifics = Specifics.find({site:site}).fetch() || {};
            return specifics;
        }
    },
    Mutation:{
        createSpecific(obj, {object,fromNote,comment,site},{user}){
            if(user._id){
                Specifics.insert({
                    _id:new Mongo.ObjectID(),
                    site:site,
                    object:object,
                    fromNote:fromNote,
                    comment:comment
                });
                return true;
            }
            throw new Error('Unauthorized');
        },
        removeSpecific(obj, {_id,site},{user}){
            if(user._id){
                Specifics.remove(new Mongo.ObjectID(_id));
                const specifics = Specifics.find({site:site}).fetch() || {};
                return specifics;
            }
            throw new Error('Unauthorized')
        }
    }
}