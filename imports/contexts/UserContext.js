import React, { Component } from 'react'
import gql from 'graphql-tag';
import { graphql, withApollo, compose } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Session } from 'meteor/session';
import { toast } from 'react-toastify';

export const UserContext = React.createContext();

    const userQuery = gql`
        query User{
            user{
                _id
                email
                isAdmin
                isOwner
                activated
                usualConsosIds
            }
            users{
                _id
                email
                isAdmin
                isOwner
                verified
                firstname
                lastname
                createdAt
                lastLogin
                activated
            }
        }
    `;

class Provider extends Component {
    state = {
        getRoleForUserQuery : gql`
            query getRoleForUser($user:String!){
                getRoleForUser(user:$user){
                    _id
                    site
                    role
                    user
                    main
                }
            }
        `,
        sites:[
            {   
                key:"AL1",text:"Airlog 1",value:"AL1",
                secteurs:[
                    {text:"",key:"",value:""},
                    {text:"INBOUND",key:"INBOUND",value:"INBOUND"},
                    {text:"MAGASINS",key:"MAGASINS",value:"MAGASINS"},
                    {text:"T2M",key:"T2M",value:"T2M"},
                    {text:"EXPE-GV350-REFUS",key:"EXPE-GV350-REFUS",value:"EXPE-GV350-REFUS"},
                    {text:"GDS",key:"GDS",value:"GDS"}
                ]
            },
            {   
                key:"AL2",text:"Airlog 2",value:"AL2",
                secteurs:[
                    {text:"",key:"",value:""},
                    {text:"Inbound A350",key:"Inbound A350",value:"Inbound A350"},
                    {text:"Outbound A350",key:"Outbound A350",value:"Outbound A350"},
                    {text:"Inbound A320-A330",key:"Inbound A320-A330",value:"Inbound A320-A330"},
                    {text:"Outbound A320-A330",key:"Outbound A320-A330",value:"Outbound A320-A330"}
                ]
            },
            {   
                key:"FAL",text:"FAL",value:"FAL",
                secteurs:[
                    {text:"",key:"",value:""},
                    {text:"Fal 380",key:"Fal 380",value:"Fal 380"},
                    {text:"Fal 330",key:"Fal 330",value:"Fal 330"},
                    {text:"Fal 350",key:"Fal 350",value:"Fal 350"},
                    {text:"Fal 320",key:"Fal 320",value:"Fal 320"}
                ]
            },
            {
                key:"SE",text:"St Eloi",value:"SE",
                secteurs:[
                    {text:"",key:"",value:""},
                ]
            },
            {
                key:"NPRS",text:"NPRS",value:"NPRS",
                secteurs:[
                    {text:"",key:"",value:""},
                ]
            },
            {
                key:"EC",text:"Eurocentre",value:"EC",
                secteurs:[
                    {text:"",key:"",value:""},
                ]
            },
            {
                key:"SUP",text:"Support",value:"SUP",
                secteurs:[
                    {text:"",key:"",value:""},
                ]
            }
        ],
        sitesKeys: ["AL1","AL2","FAL","SE","NPRS","EC","SUP"]
    }

    setSite = site => {
        this.setState({
            site:site
        })
        Session.set('site',site);
        localStorage.setItem('site',site)
    }

    getSite = () => {
        if(this.state != null){
            if(this.state.sitesKeys.includes(this.state.site)){
                return this.state.site;
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    updateUsualConsosId = () => {
        this.props.client.query({
            query : gql`
                query User{
                    user{
                        _id
                        usualConsosIds
                    }
                }
            `,
            fetchPolicy:"network-only"
        }).then(({data})=>{
            this.setState({
                usualConsosIds:data.user.usualConsosIds
            });
        });
    }

    getRoleForUser = () => {
        this.props.client.mutate({
            mutation : this.state.getRoleForUserQuery,
            variables:{
                user:Meteor.userId()
            }
        }).then(({data})=>{
            return data.getRoleForUser
        })
    }

    toast = ({message,type}) => {
        if(type == 'error'){
            toast.error(message);
        }
        if(type == 'success'){
            toast.success(message);
        }
        if(type == 'info'){
            toast.info(message);
        }
        if(type == 'warning'){
            toast.warn(message);
        }
    }

    componentWillMount = () => {
        if(this.state.sitesKeys.includes(localStorage.getItem('site'))){
            this.setState({
                site:localStorage.getItem('site')
            })
        }
    }

    render(){
        return (
            <UserContext.Provider value={{
                user: (this.props.user == null ? {_id:null} : this.props.user),
                users : this.props.users,
                client : this.props.client,
                site: this.getSite(),
                setSite:this.setSite,
                sites:this.state.sites,
                usualConsosIds:(this.props.user == null ? [] : this.props.user.usualConsosIds),
                updateUsualConsosId:this.updateUsualConsosId,
                toast:this.toast
            }}>
                {this.props.children}
            </UserContext.Provider>
        );
    }
}

export const UserProvider =
    graphql(userQuery,{
        props:({data}) =>({...data})
    })
(withApollo(withRouter(Provider)))