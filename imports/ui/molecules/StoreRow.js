import React, { Component, Fragment } from 'react'
import { Table,Button,Icon,Form,Modal,Message,Input,Dropdown,Label } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import SecteurPicker from '../atoms/SecteurPicker';
import gql from 'graphql-tag';
import _ from 'lodash';

class StockRow extends Component {

    state={
        askedQuantity:"",
        outputStock:"",
        fromNote:"",
        noteS:"",
        openCommand:false,
        openOut:false,
        emergencyColors:["red","orange","green"]
    }

    showOut = () =>{
        this.setState({ openOut: true })
    }  
    closeOut = () => {
        this.setState({ openOut: false })
    }

    handleSecteurChange = (key,secteur) => {
        this.setState({
            [key]:secteur
        })
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    
    openCommand = () => {
        this.setState({
            openCommand:true,
        });
    }

    closeCommand = () => {
        this.setState({
            openCommand:false,
        })
    }

    askForCommand = () => {
        this.props.askForCommand({
            _id:this.props.item._id,
            quantity:parseInt(this.state.askedQuantity),
            from:this.state.fromNote
        });
        this.setState({
            openCommand:false,
            fromNote:""
        })
    }

    getRefSupplierIfOne = () => {
        if(this.props.item.refS != "-"){
            return(
                <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"0.9em"}}>Fournisseur : {this.props.item.refS}</span>
            )
        }
    }

    takeFromStock = () =>{
        if(this.state.noteS.length > 0){
            this.props.takeFromStock(this.props.item._id,this.state.outputStock,false,this.state.noteS);
            this.closeOut();
        }
    }
    
    removeFromUserUsual = () => {
        this.props.removeFromUserUsual(this.props.item._id);
    }

    render() {
        return (
            <Fragment>
                <Table.Row style={{fontSize:"1.2em"}} key={this.props.item._id} >
                    <Table.Cell width={2} textAlign="center" style={{padding:"2px 4px"}}>
                        <p style={{margin:"2px"}}>{this.props.item.currentlyStored} u.</p>
                        <Label style={{margin:"2px"}} color={this.state.emergencyColors[this.props.item.emergencyCode]} horizontal>
                            {this.props.item.left}
                        </Label>
                    </Table.Cell>
                    <Table.Cell style={{padding:"4px 32px"}}>
                        {this.props.item.ref}<br/>
                        {this.getRefSupplierIfOne()}
                    </Table.Cell>
                    <Table.Cell style={{padding:"4px 32px"}}>
                        {this.props.item.categorie+" - "+this.props.item.modele}<br/>
                        <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"0.9em"}}>{this.props.item.designation}</span>
                    </Table.Cell>
                    <Table.Cell style={{padding:"4px 32px"}}>
                        {this.props.item.packagingFormat}
                    </Table.Cell>
                    <Table.Cell textAlign="center" style={{padding:"4px 16px"}}>
                        <Dropdown pointing='right' style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                            <Dropdown.Menu>
                                <Dropdown.Item style={{color:"#a29bfe"}} icon="minus" text="Retirer des favoris" onClick={this.removeFromUserUsual}/>
                                <Dropdown.Item style={{color:"#2980b9"}} icon="talk" text="Demande de commande" onClick={this.openCommand}/>
                                <Dropdown.Item style={{color:"#e67e22"}} icon="sign out" text="Retrait stock" onClick={this.showOut}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Table.Cell>
                </Table.Row>
                <Modal closeOnDimmerClick={false} dimmer="inverted" open={this.state.openCommand} onClose={this.closeCommand} closeIcon>
                    <Modal.Header>
                        Demande de commande du consomable :
                    </Modal.Header>
                    <Modal.Content >
                        <div style={{fontSize:"1.1em"}}>
                            Réference :
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.ref}
                            </span><br/>
                            Categorie & modèle:
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.categorie + " - " + this.props.item.modele}
                            </span><br/>
                            Désignation:
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.designation}
                            </span>
                        </div>
                        <Message color="blue">
                            <Form onSubmit={e=>{e.preventDefault();}} style={{display:'grid',gridTemplateColumns:"5fr 2fr 2fr",gridGap:"16px"}}>
                                <Form.Input type="number" label="Quantité" name="askedQuantity" onChange={this.handleChange} placeholder="Quantité demandée ..." size="huge"/>
                                <p style={{alignSelf:"center"}}> {this.props.item.packagingFormat}</p>
                                <Button disabled={this.state.fromNote == "" || this.state.askedQuantity <= 0} style={{fontSize:"1.4em"}} onClick={this.askForCommand} color="blue" animated='fade'>
                                    <Button.Content hidden>Créer la demande</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='talk' />
                                    </Button.Content>
                                </Button>
                                <span>
                                    Demande en provenance de {' '}
                                    <SecteurPicker onChange={this.handleSecteurChange} target={"fromNote"}/>
                                </span>
                            </Form>
                        </Message>
                    </Modal.Content>
                </Modal>
                <Modal closeOnDimmerClick={false} dimmer="inverted" open={this.state.openOut} onClose={this.closeOut} closeIcon>
                    <Modal.Header>
                        Sortie du stock :
                    </Modal.Header>
                    <Modal.Content >
                        <div style={{fontSize:"1.1em"}}>
                            Réference :
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.ref}
                            </span><br/>
                            Categorie & modèle:
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.categorie + " - " + this.props.item.modele}
                            </span><br/>
                            Désignation:
                            <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"1.3em"}}>
                                {this.props.item.designation}
                            </span>
                        </div>
                        <Message color="green">
                            <Form onSubmit={e=>{e.preventDefault();this.takeFromStock()}} style={{display:'grid',gridTemplateColumns:"3fr 1fr",gridGap:"16px"}}>
                                <Input type="number" name="outputStock" onChange={this.handleChange} placeholder="Quantité prélevée ..." size="huge"/>
                                <Button disabled={this.state.noteS == "" || this.state.outputStock <= 0} style={{fontSize:"1.4em"}} onClick={this.takeFromStock} color="green" animated='fade' >
                                    <Button.Content hidden>Valider la sortie</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='sign out' />
                                    </Button.Content>
                                </Button>
                                <span>
                                    Consomation en provenance de {' '}
                                    <SecteurPicker onChange={this.handleSecteurChange} target={"noteS"}/>
                                </span>
                            </Form>
                        </Message>
                    </Modal.Content>
                </Modal>
            </Fragment>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default wrappedInUserContext = withUserContext(StockRow);