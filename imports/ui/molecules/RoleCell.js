import React, { Component } from 'react'
import { Table, Dropdown } from 'semantic-ui-react';
import gql from 'graphql-tag';
import { withRouter } from 'react-router-dom';
import { UserContext } from '../../contexts/UserContext';

class RoleCell extends Component {

    state={
        getUserForRoleQuery : gql`
            query getUserForRole($site: String!,$role: String!,$main:Boolean!) {
                getUserForRole(site:$site,role:$role,main:$main) {
                    _id
                    email
                    firstname
                    lastname
                }
            }
        `,
        setRoleQuery: gql` mutation setRole($site:String!,$role:String!,$targetUser:String!,$main:Boolean!){
            setRole(site: $site,role: $role,targetUser: $targetUser,main: $main){
                _id
                site
                role
                user
                main
            }
        }`,
        selectedMain : null,
        selectedBackup : null
    }

    componentDidMount = () => {
        this.loadRole(true);
        this.loadRole(false);
    }

    loadRole = main => {
        this.props.client.query({
            query: this.state.getUserForRoleQuery,
            fetchPolicy:"network-only",
            variables:{
                site:this.props.site.value,
                role:this.props.role.value,
                main:main
            }
        }).then(({data}) => {
            if(data.getUserForRole != null){
                if(main){
                    this.setState({
                        selectedMain:data.getUserForRole._id
                    })
                }else{
                    this.setState({
                        selectedBackup:data.getUserForRole._id
                    })
                }
            }
        })
    }

    handleChangeMain = (e,{value}) => {
        this.props.client.mutate({
          mutation : this.state.setRoleQuery,
          variables:{
              site:this.props.site.value,
              role:this.props.role.value,
              targetUser:value,
              main:true
          }
        }).then(({data})=>{
            this.setState({
                selectedMain:data.setRole.user
            })
        })
    }

    handleChangeBackup = (e,{value}) => {
        this.props.client.mutate({
          mutation : this.state.setRoleQuery,
          variables:{
              site:this.props.site.value,
              role:this.props.role.value,
              targetUser:value,
              main:false
          }
        }).then(({data})=>{
            this.setState({
                selectedBackup:data.setRole.user
            })
        })
    }

    render() {
        return (
            <Table.Cell>
                <div style={{display:"grid",margin:"24px auto",gridTemplateRows:"auto 1fr auto"}}>
                    <Dropdown onChange={this.handleChangeMain} placeholder='Affecter un titulaire' search selection name={this.props.site.value+"/"+this.props.role.value+"/main"} value={this.state.selectedMain} options={this.props.accounts}/>
                    <p style={{placeSelf:"center",color:"#a4b0be",margin:"4px 0"}}>backup:</p>
                    <Dropdown style={{fontSize:"0.9em",width:"80%",placeSelf:"center"}} onChange={this.handleChangeBackup} placeholder='Affecter un back up' search selection name={this.props.site.value+"/"+this.props.role.value+"/backup"} value={this.state.selectedBackup} options={this.props.accounts}/>
                </div>
            </Table.Cell>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(RoleCell));