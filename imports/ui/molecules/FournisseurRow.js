import React, { Component, Fragment } from 'react'
import { Table,Dropdown,Button,Icon,Form,Grid,Modal,Message,Input } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import _ from 'lodash';

class FournisseurRow extends Component {

    state={
        editing:false,
        _id:this.props.fournisseur._id,
        newName:this.props.fournisseur.name,
        newDeliveryDelay:this.props.fournisseur.deliveryDelay,
        openDelete:false
    }

    showDelete = () =>{
        this.setState({ openDelete: true })
    }  
    closeDelete = () => {
        this.setState({ openDelete: false })
    }

    removeFournisseur = () => {
        this.props.removeFournisseur(this.state._id);
    }

    saveChange = () => {
        this.props.editFournisseur(this.state._id,this.state.newName,this.state.newDeliveryDelay);
        this.toggleEdit();
    }
    
    cancelChange = () => {
        this.toggleEdit();
    }

    toggleEdit = () => {
        this.setState({
            editing:!this.state.editing
        })
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

  render() {
    const { _id } = this.state;
    if(!this.state.editing){
        return (
            <Fragment>
                <Table.Row key={_id}>
                <Table.Cell width={10} style={{padding:"0 32px",fontSize:"1.3em"}}>{this.props.fournisseur.name}</Table.Cell>
                <Table.Cell width={4} style={{padding:"0 32px",fontSize:"1.3em"}}>{this.props.fournisseur.deliveryDelay} jours</Table.Cell>
                    <Table.Cell textAlign="center" width={2}>
                        <Dropdown pointing='right' style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                            <Dropdown.Menu>
                                <Dropdown.Item style={{color:"#2980b9"}} icon="edit outline" text="Editer" onClick={this.toggleEdit}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Table.Cell>
                </Table.Row>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openDelete} onClose={this.closeDelete} closeIcon>
                    <Modal.Header>
                        Confirmez la suppression du fournisseur:
                    </Modal.Header>
                    <Modal.Content style={{textAlign:"center"}}>
                        <Message color='red' icon>
                            <Icon name='warning sign'/>
                            <Message.Content style={{display:"grid",gridTemplateColumns:"1fr 2fr",gridTemplateRows:"1fr 1fr"}}>
                                <p style={{margin:"8px 4px",placeSelf:"center end"}}>Nom :</p>
                                <p style={{margin:"8px 4px",fontWeight:"800",placeSelf:"center start"}}>
                                    {this.props.fournisseur.name}
                                </p>
                            </Message.Content>
                        </Message>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="red" onClick={()=>{this.removeFournisseur(_id)}}>Supprimer</Button>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }else{
        return (
            <Fragment>
                <Table.Row key={_id}>
                    <Table.Cell colSpan="8" style={{padding:"16px 16px 16px 32px"}}>
                        <Form style={{width:"100%"}}>
                            <Grid>
                                <Grid.Column width={8}>
                                    <Form.Input size="huge" label='Nom' placeholder={this.props.fournisseur.name} defaultValue={this.props.fournisseur.name} name='newName' onChange={this.handleChange}/>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <Form.Input type="number" size="huge" label='Délai de livraison' placeholder={this.props.fournisseur.deliveryDelay} defaultValue={this.props.fournisseur.deliveryDelay} name='newDeliveryDelay' onChange={this.handleChange}/>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <div style={{height:"100%",display:"grid",gridTemplateColumns:"1fr 1fr",padding:"16px",gridGap:"28px"}}>
                                        <Button style={{fontSize:"1.4em"}} onClick={this.saveChange} color="blue" animated='fade'>
                                            <Button.Content hidden>Valider</Button.Content>
                                            <Button.Content visible>
                                                <Icon name='check' />
                                            </Button.Content>
                                        </Button>
                                        <Button style={{fontSize:"1.4em"}} onClick={this.cancelChange} color="red" animated='fade'>
                                            <Button.Content hidden>Annuler</Button.Content>
                                            <Button.Content visible>
                                                <Icon name='cancel' />
                                            </Button.Content>
                                        </Button>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Table.Cell>
                </Table.Row>
            </Fragment>
        )
    }
  }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default wrappedInUserContext = withUserContext(FournisseurRow);
  