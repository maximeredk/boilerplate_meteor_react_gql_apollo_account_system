import React, { Component, Fragment } from 'react'
import { Table } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import moment from 'moment';

class HistoriqueRow extends Component {

    state={
        _id:this.props.entry._id,
        entry:this.props.entry
    }
    
    getColor = () => {
        if(this.state.entry.type == "recep"){
            return "rgba(46, 204, 113,.15)"
        }else{
            if(this.state.entry.type.split("/")[1] == "+"){
                return "rgba(46, 204, 113,.15)"
            }else{
                return "rgba(255, 107, 107,.15)"
            }
        }
    }

    getPlusOrMinus = () => {
        if(this.state.entry.type == "recep"){
            return ("+");
        }else{
            return this.state.entry.type.split("/")[1];
        }
    }

    getLabel = () => {
        if(this.state.entry.type == "recep"){
            return "Reception de livraison"
        }else{
            if(this.state.entry.type.split("/")[1] == "+"){
                if(this.state.entry.type.split("/")[0] == "false"){
                    return "Retour du consomable au stock"
                }else{
                    return "Correction vers le haut"
                }
            }else{
                if(this.state.entry.type.split("/")[0] == "false"){
                    return "Sortie de stock, consomation"
                }else{
                    if(this.state.entry.type.split("/")[0] == "cancel"){
                        return "Annulation de reception"
                    }else{
                        return "Correction vers le bas"
                    }
                }
            }
        }
    }

    render() {
        const { _id,entry } = this.state;
        return (
            <Table.Row style={{backgroundColor:this.getColor()}} key={_id}>
                <Table.Cell width={2} textAlign="center" style={{whiteSpace:"nowrap",padding:"6px 6px",fontSize:"1.2em"}}>{moment(new Date(entry.entryYear,entry.entryMonth,entry.entryDay,entry.entryHour,entry.entryMinute,entry.entrySecond).toString()).format('DD/MM/YY - HH:mm:ss')}</Table.Cell>
                <Table.Cell width={3} style={{padding:"6px 16px",fontSize:"1em"}}>
                    {this.props.consomable.categorie+" - "+this.props.consomable.modele}<br/>
                    <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"0.9em"}}>{this.props.consomable.designation}</span>
                </Table.Cell>
                <Table.Cell width={2} textAlign="center" style={{fontWeight:"bold",padding:"6px 16px",fontSize:"1.2em"}}>{this.getPlusOrMinus() + " " + entry.quantity} u.</Table.Cell>
                <Table.Cell width={3} textAlign="center" style={{padding:"6px 16px",fontSize:"0.95em"}}>{this.getLabel()}</Table.Cell>
                <Table.Cell width={6} style={{padding:"6px 8px",fontSize:"0.95em"}}>{entry.note}</Table.Cell>
            </Table.Row>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default wrappedInUserContext = withUserContext(HistoriqueRow);
  