import React, { Component,Fragment } from 'react'
import { Table,Button,Modal,Form,Progress,Input,Icon,Header,List,Card,Segment,Accordion,Label } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import moment from 'moment';
import 'moment/locale/fr';

class CommandeRow extends Component {
    
    state={
        expanded:[],
        expandedDeliveries:[],
        commande:this.props.commande,
        consomable:this.props.consomable,
        selectedCommandeConso:{consomable:{categorie:"",modele:"",designation:""}},
        selectedDeliveryConsoId:null,
        newCommandeConsoQuantity:0,
        newBl:"",
        detailed:false,
        openIn:false,
        openRemove:false,
        openArchive:false,
        openShipping:false,
        openDSL:false,
        openEditConso:false,
        openRemoveDelivery:false,
        statusColors:["red","orange","yellow","green","blue"]
    }

    componentWillMount = () => {
        moment.locale('fr');
        this.props.loadRoles();
    }
    componentDidMount = () => {
        let recepConsos = [];
        {this.props.commande.commandeConsos.map(c=>{
            recepConsos.push({_id:c.consomable._id,recepQuantity:0});
        })}
        this.setState({
            recepConsos:recepConsos
        })
    }

    //UI
    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    handleRecepQuantityChange = (n,_id) => {
        let recepConsos = [];
        this.state.recepConsos.map(cons=>{
            if(cons._id == _id){
                recepConsos.push({_id:cons._id,recepQuantity:parseInt(n)})
            }else{
                recepConsos.push({_id:cons._id,recepQuantity:cons.recepQuantity})    
            }
        });
        this.setState({
            recepConsos:recepConsos
        })
    }
    showIn = () => {
        let recepConsos = [];
        this.state.recepConsos.map(cons=>{
            recepConsos.push({_id:cons._id,recepQuantity:0});
        });
        this.setState({ recepConsos:recepConsos,openIn:true })
    }  
    closeIn = () => {
        this.setState({ openIn: false, newBl: "" })
    }
    showRemove = () => {
        this.setState({ openRemove: true })
    }  
    closeRemove = () => {
        this.setState({ openRemove: false })
    }
    showDSL = () =>{
        this.setState({ openDSL: true })
    }  
    closeDSL = () => {
        this.setState({ openDSL: false })
    }
    showArchive = () =>{
        this.setState({ openArchive: true })
    }  
    closeArchive = () => {
        this.setState({ openArchive: false })
    }
    showShipping = () =>{
        this.setState({ openShipping: true })
    }  
    closeShipping = () => {
        this.setState({ openShipping: false })
    }
    showDetails = () => {
        this.setState({
            detailed : !this.state.detailed
        })
    }
    showEditConso = c => {
        this.setState({
            selectedCommandeConso : c,
            openEditConso : !this.state.openEditConso
        })
    }
    closeEditConso = () => {
        this.setState({ openEditConso: false })
    }
    showRemoveDelivery = _id => {
        this.setState({
            selectedDeliveryConsoId : _id,
            openRemoveDelivery : !this.state.openEditConso
        })
    }
    closeRemoveDelivery = () => {
        this.setState({ openRemoveDelivery: false })
    }
    expand = i => {
        if(this.state.expanded.includes(i)){
            let exp = this.state.expanded;
            exp.splice(exp.indexOf(i), 1);
            this.setState({expanded:exp});
        }else{
            let exp = this.state.expanded;
            exp.push(i);
            this.setState({expanded:exp});
        }
    }
    isExpanded = i => {
        return this.state.expanded.includes(i);
    }
    expandDelivery = i => {
        if(this.state.expandedDeliveries.includes(i)){
            let exp = this.state.expandedDeliveries;
            exp.splice(exp.indexOf(i), 1);
            this.setState({expandedDeliveries:exp});
        }else{
            let exp = this.state.expandedDeliveries;
            exp.push(i);
            this.setState({expandedDeliveries:exp});
        }
    }
    isDeliveryExpanded = i => {
        return this.state.expandedDeliveries.includes(i);
    }
    setQuantityTo = (e,n) =>{
        e.target.parentNode.parentNode.querySelector("input").value = n;
    }
    getProgressBarColor = (progress,total) => {
        if(progress == total){return 'blue'}
        if(progress == 0){return 'grey'}
        if(progress > total){return 'red'}
        if(progress > 0 && progress < total){return 'green'}
    }

    //ACTIONS
    removeDelivery = _id => {
        this.props.removeDelivery(_id);
        this.closeRemoveDelivery();
    }
    recepCommande = () => {
        this.props.recepCommande(this.props.commande._id,this.state.recepConsos,this.state.newBl);
        this.closeIn();
    }
    removeCommande = () => {
        this.props.removeCommande(this.props.commande._id);
        this.closeRemove();
    }
    archiveCommande = () => {
        this.props.archiveCommande(this.props.commande._id);
        this.closeArchive();
        this.showDetails();
    }
    validateDSL = () => {
        this.props.validateDSL(this.props.commande._id);
        this.closeDSL();
    }
    shippingCommande = () => {
        this.props.shippingCommande(this.props.commande._id);
        this.closeShipping();
    }
    editCommandeConso = _id => {
        this.props.editCommandeConso(_id,this.props.commande._id,this.state.newCommandeConsoQuantity);
        this.closeEditConso();
    }

    //GET CONTENT
    getStatus = () => {
        let frag = [];
        for (let i = 0; i < 5; i++) {
            if(i<this.props.commande.status){
                frag.push(<Icon key={this.props.commande._id + "c" + i} name="certificate" color={this.state.statusColors[this.props.commande.status-1]}/>)
            }else{
                frag.push(<Icon key={this.props.commande._id + "c" + i} name="certificate"/>)
            }
        }
        return frag
    }
    getRetard = c => {
        if(c.status >= 3){
            if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isSame(moment(), 'day')){
                return "dernier jour";
            }
            if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isAfter(moment(), 'day')){
                return moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').toNow(true) + " restant";
            }
            if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isBefore(moment(), 'day')){
                return moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').toNow(true) + " de retard";
            }
            return "error";
        }else{
            return "aucun";
        }
    }
    getConsoQuantityInfos = (quantities,nbPackages) => {
        if(quantities == nbPackages){
            return (
                <List.Item>
                    <List.Content>
                        <List.Content floated='right'>
                            <Label color="blue">La quantité demandé équivaut à la quantité commandée</Label>
                        </List.Content>
                    </List.Content>
                </List.Item>
            );
        }
        if(quantities < nbPackages){
            return (
                <List.Item>
                    <List.Content>
                        <List.Content floated='right'>
                            <Label color="green">{nbPackages-quantities} en surplus par rapport aux demandes</Label>
                        </List.Content>
                    </List.Content>
                </List.Item>
            )
        }
        if(quantities > nbPackages){
            return (
                <List.Item>
                    <List.Content>
                        <List.Content floated='right'>
                            <Label color="red">{quantities-nbPackages} manquant par rapport aux demandes</Label>
                        </List.Content>
                    </List.Content>
                </List.Item>
            )
        }
    }
    getNbPackagesLeft = _id => {
        const c = this.props.commande.commandeConsos.find(x=>x._id == _id);
        return c.nbPackages - c.received;
    }
    getEditConsoButton = c => {
        if(this.props.commande.status < 3){
            return (
                <Label color="blue" basic style={{cursor:"pointer",fontSize:"1.1em",padding:"2px 4px",placeSelf:"start center"}} onClick={()=>{this.showEditConso(c)}}><p>Modifier la quantité</p></Label>
            )
        }
    }
    getDeliveryList = () => {
        if(this.props.commande.deliveries.length == 0){
            return (
                <Header as="h1">Aucune réception enregistrée</Header>
            )
        }else{
            return (
                this.props.commande.deliveries.map((d,i)=>
                    <Segment raised key={d._id}>
                        <div style={{display:"grid",gridGap:"8px",gridTemplateRows:"auto auto 1fr",gridTemplateColumns:"1fr auto"}}>
                            <p style={{gridRowEnd:"span 3",textAlign:"justify"}}><strong>Note de livraison :</strong> {d.bl}</p>
                            <Label basic color="black">
                                <p style={{margin:"0"}} align="center">réception le : {d.deliveryDay.toString().padStart(2,'0') + "/" + (d.deliveryMonth+1).toString().padStart(2,'0')  + "/" +  d.deliveryYear}</p>
                                <p style={{margin:"0"}} align="center">valeur : {(d.deliveryConsos.reduce((a,b)=>{return(a+b.quantite * b.consomable.prix)},0) || 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</p>
                            </Label>
                            <Label basic style={{cursor:"pointer"}} onClick={()=>{this.showRemoveDelivery(d._id)}} color="red"><p align="center">supprimer</p></Label>
                        </div>
                        <Accordion>
                            <Accordion.Title active={this.isDeliveryExpanded(i)} index={i} onClick={()=>{this.expandDelivery(i)}}>
                                <Icon name='dropdown' />
                                <Label>Contenu de la livraison</Label>
                            </Accordion.Title>
                            <Accordion.Content active={this.isDeliveryExpanded(i)}>
                                <List divided verticalAlign='middle' style={{padding:"16px 32px",textAlign:"left"}}>
                                    {d.deliveryConsos.map(dc=>{
                                        return(
                                            <List.Item key={dc._id}>
                                                <List.Content>
                                                    <List.Content floated='right'>
                                                        <strong>{dc.quantite}</strong>
                                                    </List.Content>
                                                    <List.Content>
                                                        <p>{dc.consomable.categorie + " " + dc.consomable.modele + " " + dc.consomable.designation}</p>
                                                    </List.Content>
                                                </List.Content>
                                            </List.Item>
                                        )
                                    })}
                                </List>
                            </Accordion.Content>
                        </Accordion>
                    </Segment>
                )
            )
        }
    }
    getCommandContentList = () => {
        return (
            this.props.commande.commandeConsos.map((c,i)=>{
                return(
                    <Segment raised key={this.props.commande._id+"/"+c._id}>
                        <div style={{display:"grid",gridTemplateColumns:"1fr auto auto"}}>
                            <p>{c.consomable.categorie + " " + c.consomable.modele + " " + c.consomable.designation}<br/><span style={{color:"#95a5a6"}}>Conditionement : {c.consomable.packagingFormat}</span></p>
                            <Label style={{fontSize:"1.1em",padding:"2px 4px",placeSelf:"start center"}} color="black" basic><p>valeur : {(c.nbPackages * c.consomable.prix).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</p></Label>
                            {this.getEditConsoButton(c)}
                        </div>
                        <Progress color={this.getProgressBarColor(c.received,c.nbPackages)} style={{gridRowStart:"2",gridColumnStart:"2",gridColumnEnd:"span 2",marginBottom:"24px"}} percent={Math.ceil((c.received/c.nbPackages)*100)} label={c.received + " / " + c.nbPackages} progress/>
                        <Accordion>
                            <Accordion.Title active={this.isExpanded(i)} index={i} onClick={()=>{this.expand(i)}}>
                                <Icon name='dropdown' />
                                <Label>Provenance des demandes : <strong>{JSON.parse(c.fromNote).length}</strong> demandes</Label>
                            </Accordion.Title>
                            <Accordion.Content active={this.isExpanded(i)}>
                                <List divided verticalAlign='middle' style={{padding:"16px 32px",textAlign:"left"}}>
                                    {JSON.parse(c.fromNote).map(f=>{
                                        return(
                                            <List.Item key={f._id}>
                                                <List.Content>
                                                    <List.Content floated='right'>
                                                        <strong>{f.quantity}</strong>
                                                    </List.Content>
                                                    <List.Content>
                                                        {f.from}
                                                    </List.Content>
                                                </List.Content>
                                            </List.Item>
                                        )
                                    })}
                                    {this.getConsoQuantityInfos(JSON.parse(c.fromNote).reduce(function(a,b){return a + b.quantity;},0),c.nbPackages)}
                                </List>
                            </Accordion.Content>
                        </Accordion>
                    </Segment>
                )
            })
        )
    }
    getDSLCardContent = () => {
        let firstnameMain,lastnameMain,firstnameBackup,lastnameBackup,idMain,idBackup = "";
        if(this.props.validationDSL_main != ""){
            firstnameMain = this.props.validationDSL_main.firstname
            lastnameMain = this.props.validationDSL_main.lastname
            idMain = this.props.validationDSL_main._id
        }
        if(this.props.validationDSL_backup != ""){
            firstnameBackup = this.props.validationDSL_backup.firstname
            lastnameBackup = this.props.validationDSL_backup.lastname
            idBackup = this.props.validationDSL_backup._id
        }
        if(this.props.commande.status>1){
            return(
                <Fragment>
                    <Card.Content textAlign="center">
                        <Card.Header>Validation DSL</Card.Header>
                        <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                        <Card.Description>
                            La validation par le Directeur de Site Logistique à été fournie
                        </Card.Description>
                    </Card.Content>
                </Fragment>
            )
        }else{
            if(Meteor.userId() == idMain || Meteor.userId() == idBackup){
                return(
                    <Fragment>
                        <Card.Content textAlign="center">
                            <Card.Header>Validation DSL</Card.Header>
                            <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                            <Card.Description>
                                La validation par le Directeur de Site Logistique est requise pour le lancement de cette commande
                            </Card.Description>
                        </Card.Content>
                        <Card.Content textAlign="center" extra>
                            <Button style={{width:"160px"}} onClick={this.showDSL} color="green" animated='fade'>
                                <Button.Content hidden>Valider</Button.Content>
                                <Button.Content visible>
                                    <Icon name='check'/>
                                </Button.Content>
                            </Button>
                        </Card.Content>
                    </Fragment>
                )
            }else{
                return(
                    <Fragment>
                        <Card.Content textAlign="center">
                            <Card.Header>Validation DSL</Card.Header>
                            <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                            <Card.Description>
                                La validation par le Directeur de Site Logistique est requise pour le lancement de cette commande
                            </Card.Description>
                        </Card.Content>
                        <Card.Content textAlign="center" extra>
                            <Button style={{width:"160px"}} disabled color="grey" animated='fade'>
                                <Button.Content hidden>Valider</Button.Content>
                                <Button.Content visible>
                                    <Icon name='check'/>
                                </Button.Content>
                            </Button>
                        </Card.Content>
                    </Fragment>
                )
            }
        }
    }
    getShippingCardContent = () => {
        let firstnameMain,lastnameMain,firstnameBackup,lastnameBackup,idMain,idBackup = "";
        if(this.props.commandAdmin_main != ""){
            firstnameMain = this.props.commandAdmin_main.firstname
            lastnameMain = this.props.commandAdmin_main.lastname
            idMain = this.props.commandAdmin_main._id
        }
        if(this.props.commandAdmin_backup != ""){
            firstnameBackup = this.props.commandAdmin_backup.firstname
            lastnameBackup = this.props.commandAdmin_backup.lastname
            idBackup = this.props.commandAdmin_backup._id
        }
        if(this.props.commande.status<2){
            return(
                <Fragment>
                    <Card.Content textAlign="center">
                        <Card.Header>Confirmation de passage de la commande</Card.Header>
                        <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                        <Card.Description>
                            La confirmation DSL est requise pour passer la commande
                        </Card.Description>
                    </Card.Content>
                    <Card.Content textAlign="center" extra>
                        <Button style={{width:"160px"}} disabled color="grey" animated='fade'>
                            <Button.Content hidden>Confirmer</Button.Content>
                            <Button.Content visible>
                                <Icon name='shipping'/>
                            </Button.Content>
                        </Button>
                    </Card.Content>
                </Fragment>
            )
        }
        if(this.props.commande.status>2){
            return(
                <Fragment>
                    <Card.Content textAlign="center">
                        <Card.Header>Confirmation de passage de la commande</Card.Header>
                        <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                        <Card.Description>
                           La commande à été passée
                        </Card.Description>
                    </Card.Content>
                </Fragment>
            )
        }
        if(this.props.commande.status == 2){
            if(Meteor.userId() == idMain || Meteor.userId() == idBackup){
                return(
                    <Fragment>
                        <Card.Content textAlign="center">
                            <Card.Header>Confirmation de passage de la commande</Card.Header>
                            <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                            <Card.Description>
                                La confirmation DSL est requise pour passer la commande
                            </Card.Description>
                        </Card.Content>
                        <Card.Content textAlign="center" extra>
                            <Button style={{width:"160px"}} onClick={this.showShipping} color="blue" animated='fade'>
                                <Button.Content hidden>Confirmer</Button.Content>
                                <Button.Content visible>
                                    <Icon name='shipping'/>
                                </Button.Content>
                            </Button>
                        </Card.Content>
                    </Fragment>
                )
            }else{
                return(
                    <Fragment>
                        <Card.Content textAlign="center">
                            <Card.Header>Confirmation de passage de la commande</Card.Header>
                            <Card.Meta>{ firstnameMain+" "+lastnameMain }<br/>(backup : { firstnameBackup+" "+lastnameBackup })</Card.Meta>
                            <Card.Description>
                                Veuillez confirmer que la commande à été passée
                            </Card.Description>
                        </Card.Content>
                        <Card.Content textAlign="center" extra>
                        <Button style={{width:"160px"}} disabled color="grey" animated='fade'>
                                <Button.Content hidden>Confirmer</Button.Content>
                                <Button.Content visible>
                                    <Icon name='shipping'/>
                                </Button.Content>
                            </Button>
                        </Card.Content>
                    </Fragment>
                )
            }
        }
    }
    getOrderDate = c => {
        if(c.orderDay != null){
            return c.orderDay.toString().padStart(2,'0') + "/" + (c.orderMonth+1).toString().padStart(2,'0') + "/" + c.orderYear;
        }else{
            return "-";
        }
    }

    render() {
        const { commande } = this.props;
        const fournisseur = commande.fournisseur;
        const { detailed } = this.state;
        if(detailed){
            return(
                <Fragment>
                    <Modal dimmer="inverted" style={{width:"90vw",margin:"auto"}} open={this.state.detailed} onClose={this.showDetails}>
                        <Modal.Content>
                            <div style={{width:"96%",height:"90%",margin:"8px auto",display:"grid",gridGap:"16px",gridTemplateColumns:"1fr auto auto auto",gridTemplateRows:"1fr 1fr"}}>
                                <Segment color="blue" style={{gridRowEnd:"span 2",fontSize:"1.2em",width:"100%",margin:"0 auto",justifySelf:"stretch",display:"grid",gridGap:"0 24px",gridTemplateColumns:"auto 1fr auto 1fr",gridTemplateRows:"1fr 1fr 1fr 1fr"}}>
                                    <p style={{gridColumnStart:"1",gridRowStart:"1",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Fournisseur :</p>
                                    <p style={{gridColumnStart:"2",gridRowStart:"1",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{fournisseur.name}</p>
                                    <p style={{gridColumnStart:"1",gridRowStart:"2",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Délai de livraison :</p>
                                    <p style={{gridColumnStart:"2",gridRowStart:"2",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{fournisseur.deliveryDelay} jours</p>
                                    <p style={{gridColumnStart:"1",gridRowStart:"3",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Commande crée le :</p>
                                    <p style={{gridColumnStart:"2",gridRowStart:"3",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{commande.creationDay.toString().padStart(2,'0') + "/" + (commande.creationMonth+1).toString().padStart(2,'0') + "/" + commande.creationYear}</p>
                                    <p style={{gridColumnStart:"1",gridRowStart:"4",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Commande passé le :</p>
                                    <p style={{gridColumnStart:"2",gridRowStart:"4",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{this.getOrderDate(commande)}</p>
                                    <p style={{gridColumnStart:"3",gridRowStart:"1",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Ponctualité :</p>
                                    <p style={{gridColumnStart:"4",gridRowStart:"1",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{this.getRetard(commande)}</p>
                                    <p style={{gridColumnStart:"3",gridRowStart:"2",alignSelf:"center",justifySelf:"end",marginBottom:"8px"}}>Valeur de la commande :</p>
                                    <p style={{gridColumnStart:"4",gridRowStart:"2",alignSelf:"center",fontWeight:"bold",marginBottom:"8px"}}>{this.props.commande.commandeConsos.reduce((a,b)=>{return a + b.nbPackages * b.consomable.prix},0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</p>
                                    <span style={{gridColumnStart:"3",gridRowStart:"3",gridColumnEnd:"span 2",gridRowEnd:"span 2",placeSelf:"center",fontSize:"1.4em"}}>{this.getStatus()}</span>
                                </Segment>
                                <Card color="blue" style={{gridRowEnd:"span 2",margin:"0 auto"}}>
                                    {this.getDSLCardContent()}
                                </Card>
                                <Card color="green" style={{gridRowEnd:"span 2",margin:"0 auto"}}>
                                    {this.getShippingCardContent()}
                                </Card>
                                <Button style={{gridColumnStart:"4",width:"140px",fontSize:"1.4em"}} onClick={this.showArchive} color="orange" animated='fade'>
                                    <Button.Content hidden>Archiver</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='archive'/>
                                    </Button.Content>
                                </Button>
                                <Button disabled={this.props.commande.status < 3} style={{gridColumnStart:"4",width:"140px",fontSize:"1.4em"}} onClick={this.showIn} color="green" animated='fade'>
                                    <Button.Content hidden>Réceptionner</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='sign in'/>
                                    </Button.Content>
                                </Button>
                            </div>
                            <div style={{display:"grid",gridGap:"48px",margin:"48px",gridTemplateColumns:"1fr 1fr"}}>
                                <div style={{gridColumnStart:"1"}}>
                                    <Header as='h2'>
                                        <Icon name='list layout'/>
                                        Récapitulatif
                                    </Header>
                                    {this.getCommandContentList()}
                                </div>
                                <div style={{gridColumnStart:"2"}}>
                                    <Header as='h2'>
                                        <Icon name='shipping' />
                                        Réceptions
                                    </Header>
                                    {this.getDeliveryList()}
                                </div>
                            </div>

                        </Modal.Content>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="large" open={this.state.openIn} onClose={this.closeIn} closeIcon>
                        <Modal.Header>
                            Réception de la commande :
                        </Modal.Header>
                        <Modal.Content >
                            <Form onSubmit={e=>{e.preventDefault();this.recepCommande()}} style={{display:'grid',gridTemplateColumns:"4fr 1fr",gridGap:"16px"}}>
                                {commande.commandeConsos.map((c)=>{
                                    return (
                                        <div style={{display:'grid',gridColumnEnd:"span 2",gridTemplateColumns:"4fr 4fr 2fr 64px",gridGap:"16px",placeSelf:"stretch"}} key={this.props.commande._id+"recep"+c._id}>
                                            <p>{c.consomable.categorie + " " + c.consomable.modele + " " + c.consomable.designation}</p>
                                            <Input style={{gridColumnEnd:"span 2"}} type="number" style={{gridColumnEnd:"span 2"}} onChange={e=>{this.handleRecepQuantityChange(e.target.value,c.consomable._id)}} placeholder={this.getNbPackagesLeft(c._id) + " encore attendu"} size="huge"/>
                                            <div style={{display:"grid",gridGap:"2px",gridTemplateRows:"1fr 1fr"}}>
                                                <Label style={{textAlign:"center",cursor:"pointer",margin:"0",placeSelf:"stretch"}} onClick={e=>{this.setQuantityTo(e,0);this.handleRecepQuantityChange(0,c.consomable._id)}} color="green">0</Label>
                                                <Label style={{textAlign:"center",cursor:"pointer",margin:"0",placeSelf:"stretch"}} onClick={e=>{this.setQuantityTo(e,this.getNbPackagesLeft(c._id));this.handleRecepQuantityChange(this.getNbPackagesLeft(c._id),c.consomable._id)}} color="red">max</Label>
                                            </div>
                                        </div>
                                    )
                                })}
                                <Input name="newBl" onChange={this.handleChange} placeholder="Reference BL ..." size="huge"/>
                                <Button style={{fontSize:"1.4em",padding:"auto 16px"}} onClick={this.recepCommande} color="green" animated='fade'>
                                    <Button.Content visible>Réception</Button.Content>
                                    <Button.Content hidden>
                                        <Icon name='sign-in'/>
                                    </Button.Content>
                                </Button>
                            </Form>
                        </Modal.Content>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openDSL} onClose={this.closeDSL} closeIcon>
                            <Modal.Header>
                                Approuver la commande ?
                            </Modal.Header>
                            <Modal.Content style={{fontSize:"1.4em"}}>
                                <Segment textAlign="center">
                                    <div style={{fontSize:"1.4em"}}>
                                        <p>{"Fournisseur : "} <strong> {fournisseur.name}</strong></p>
                                    </div>
                                </Segment>
                            </Modal.Content>
                            <Modal.Actions>
                                <Button style={{fontSize:"1.4em"}} onClick={this.validateDSL} color="green" animated='fade'>
                                    <Button.Content visible>Valider</Button.Content>
                                    <Button.Content hidden>
                                        <Icon name='check'/>
                                    </Button.Content>
                                </Button>
                            </Modal.Actions>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openArchive} onClose={this.closeArchive} closeIcon>
                        <Modal.Header>
                            Archive la commande ?
                        </Modal.Header>
                        <Modal.Content style={{fontSize:"1.4em"}}>
                            <Segment textAlign="center">
                                <div style={{fontSize:"1.4em"}}>
                                    <p>{"Fournisseur : "} <strong> {fournisseur.name + ""}</strong></p>
                                </div>
                            </Segment>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button style={{fontSize:"1.4em",padding:"auto 16px"}} onClick={this.archiveCommande} color="orange" animated='fade'>
                                <Button.Content visible>Archiver</Button.Content>
                                <Button.Content hidden>
                                    <Icon name='archive'/>
                                </Button.Content>
                            </Button>
                        </Modal.Actions>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openRemove} onClose={this.closeRemove} closeIcon>
                        <Modal.Header>
                            Supprimer la commande ?
                        </Modal.Header>
                        <Modal.Content style={{fontSize:"1.4em"}}>
                            <Segment textAlign="center">
                                <div style={{fontSize:"1.4em"}}>
                                    <p>{"Fournisseur : "} <strong> {fournisseur.name + ""}</strong></p>
                                </div>
                            </Segment>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button style={{fontSize:"1.4em",padding:"auto 16px"}} onClick={this.removeCommande} color="red" animated='fade'>
                                <Button.Content visible>Supprimer</Button.Content>
                                <Button.Content hidden>
                                    <Icon name='trash'/>
                                </Button.Content>
                            </Button>
                        </Modal.Actions>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openShipping} onClose={this.closeShipping} closeIcon>
                        <Modal.Header>
                           Confirmer que la commande à été envoyée au fournisseur ?
                        </Modal.Header>
                        <Modal.Content style={{fontSize:"1.4em"}}>
                            <Segment textAlign="center">
                                <div style={{fontSize:"1.4em"}}>
                                    <p>{"Fournisseur : "} <strong> {fournisseur.name + ""}</strong></p>
                                </div>
                            </Segment>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button style={{fontSize:"1.4em"}} onClick={this.shippingCommande} color="blue" animated='fade'>
                                <Button.Content visible>Confirmer</Button.Content>
                                <Button.Content hidden>
                                    <Icon name='shipping'/>
                                </Button.Content>
                            </Button>
                        </Modal.Actions>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="mini" open={this.state.openEditConso} onClose={this.closeEditConso} closeIcon>
                        <Header icon='edit' content='Modifier la quantité de commande :' />
                        <Modal.Content style={{display:"grid",gridTemplateRows:"1fr 1fr"}}>
                            <p style={{justifySelf:"center"}}>{this.state.selectedCommandeConso.consomable.categorie + " " + this.state.selectedCommandeConso.consomable.modele + " " + this.state.selectedCommandeConso.consomable.designation}</p>
                            <Input type="number" style={{justifySelf:"center"}} name="newCommandeConsoQuantity" onChange={this.handleChange} placeholder='Nouvelle quantité ...' />
                        </Modal.Content>
                        <Modal.Actions style={{display:"grid",gridTemplateColumns:"1fr 1fr"}}>
                            <Button style={{justifySelf:"center"}} basic color='red' onClick={this.closeEditConso}>
                                <Icon name='remove' /> Annuler
                            </Button>
                            <Button style={{justifySelf:"center"}} basic color='green' onClick={()=>{this.editCommandeConso(this.state.selectedCommandeConso._id)}}>
                                <Icon name='checkmark' /> Modifier
                            </Button>
                        </Modal.Actions>
                    </Modal>
                    <Modal closeOnDimmerClick={false} dimmer="inverted" size="mini" open={this.state.openRemoveDelivery} onClose={this.closeRemoveDelivery} closeIcon>
                        <Header icon='edit' content='Supprimer cette livraison :' />
                        <Modal.Actions style={{display:"grid",gridTemplateColumns:"1fr 1fr"}}>
                            <Button style={{justifySelf:"center"}} basic color='red' onClick={this.closeRemoveDelivery}>
                                <Icon name='remove' /> Annuler
                            </Button>
                            <Button style={{justifySelf:"center"}} basic color='green' onClick={()=>{this.removeDelivery(this.state.selectedDeliveryConsoId)}}>
                                <Icon name='checkmark' /> Confirmer
                            </Button>
                        </Modal.Actions>
                    </Modal>
                </Fragment>
            )
        }else{
            return (
                <Table.Row key={fournisseur._id}>
                    <Table.Cell width={3} textAlign="center" style={{whiteSpace:"nowrap",padding:"6px 6px",fontSize:"1.2em"}}>{fournisseur.name}</Table.Cell>
                    <Table.Cell width={3} textAlign="center" style={{padding:"6px 16px",fontSize:"1.2em"}}>{commande.internalRef}</Table.Cell>
                    <Table.Cell width={2} textAlign="center" style={{padding:"6px 16px",fontSize:"1.2em"}}>{this.props.commande.commandeConsos.reduce((a,b)=>{return a + b.nbPackages * b.consomable.prix},0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</Table.Cell>
                    <Table.Cell width={2} textAlign="center" style={{padding:"6px 16px",fontSize:"1.2em"}}>{commande.creationDay.toString().padStart(2,'0') + "/" + commande.creationMonth.toString().padStart(2,'0') + "/" + commande.creationYear}</Table.Cell>
                    <Table.Cell width={2} textAlign="center" style={{padding:"6px 16px",fontSize:"1.2em"}}>{this.getRetard(commande)}</Table.Cell>
                    <Table.Cell width={2} textAlign="center" style={{padding:"6px 16px",fontSize:"1.2em"}}>{this.getStatus()}</Table.Cell>
                    <Table.Cell width={2} style={{padding:"4px",fontSize:"0.95em"}}>
                        <div style={{width:"100%"}}>
                            <Progress color={this.getProgressBarColor(commande.commandeConsos.reduce(function(a,b){return a + b.received;},0),commande.commandeConsos.reduce(function(a,b){return a + b.nbPackages;},0))} style={{padding:"auto 16px",width:"80%",margin:"auto"}} percent={Math.ceil(((commande.commandeConsos.reduce(function(a,b){return a + b.received;},0))/(commande.commandeConsos.reduce(function(a,b){return a + b.nbPackages;},0)))*100)} size="small"  />
                        </div>
                    </Table.Cell>
                    <Table.Cell width={2} textAlign="center" style={{padding:"6px 8px",fontSize:"0.95em"}}>
                        <Button  style={{height:"28px",width:"117px",padding:"7px 0"}} content='Details' icon='caret square down' labelPosition='right' onClick={this.showDetails}/>
                    </Table.Cell>
                </Table.Row>
            )
        }
    }
}


const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
)
  
export default wrappedInUserContext = withUserContext(CommandeRow);