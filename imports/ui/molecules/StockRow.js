import React, { Component, Fragment } from 'react';
import SecteurPicker from '../atoms/SecteurPicker';
import { Table,Dropdown,Button,Icon,Form,Grid,Modal,Message,Input,Radio,Label } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import _ from 'lodash';

class StockRow extends Component {

    state={
        editing:false,
        _id:this.props.consomable._id,
        newRef:this.props.consomable.ref,
        newRefS:this.props.consomable.refS,
        newMinOrder:this.props.consomable.minOrder,
        newDesignation:this.props.consomable.designation,
        newCategorie:this.props.consomable.categorie,
        newModele:this.props.consomable.modele,
        newCurrentlyStored:this.props.consomable.currentlyStored,
        newAvCS:this.props.consomable.avCS,
        newPrix:this.props.consomable.prix,
        newPackagingFormat:this.props.consomable.packagingFormat,
        fromNote:"",
        noteE:"",
        noteS:"",
        isCorrectionE:false,
        isCorrectionS:false,
        selectedFournisseur:this.props.consomable.fournisseur._id,
        outputStock:0,
        inputStock:0,
        orderQuantity:0,
        openIn:false,
        openOut:false,
        openDelete:false,
        openOrder:false,
        createDemandeConsoQuery : gql`
            mutation createDemandeConso($consomable:String!,$quantity:Int!,$from:String!,$site:String!){
                createDemandeConso(consomable:$consomable,quantity:$quantity,from:$from,site:$site)
            }
        `,
        addToUserUsualQuery : gql`
            mutation addToUserUsual($_id:String!,$userId:String!){
                addToUserUsual(_id:$_id,userId:$userId)
            }
        `,
        emergencyColors:["red","orange","green"]
    }

    showIn = () =>{
        this.setState({ openIn: true })
    }  
    closeIn = () => {
        this.setState({ noteE:"",openIn: false })
    }

    handleSecteurChange = (key,secteur) => {
        this.setState({
            [key]:secteur
        })
    }

    showOut = () =>{
        this.setState({ openOut: true })
    }  
    closeOut = () => {
        this.setState({ noteS:"",openOut: false })
    }

    showDelete = () =>{
        this.setState({ openDelete: true })
    }  
    closeDelete = () => {
        this.setState({ openDelete: false })
    }

    showOrder = () =>{
        this.setState({ openOrder: true })
    }  
    closeOrder = () => {
        this.setState({ fromNote:"",openOrder: false })
    }

    takeFromStock = () =>{
        this.props.takeFromStock(this.state._id,this.state.outputStock,this.state.isCorrectionS,this.state.noteS);
        this.closeOut();
    }
    addToStock = () =>{
        this.props.addToStock(this.state._id,this.state.inputStock,this.state.isCorrectionE,this.state.noteE);
        this.closeIn();
    }

    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    removeConsomable = () => {
        this.props.removeConsomable(this.state._id);
    }

    addToUserUsual = () => {
        this.props.client.mutate({
            mutation : this.state.addToUserUsualQuery,
            variables:{
                userId:Meteor.userId(),
                _id:this.state._id
            }
        }).then(({data})=>{
            this.props.updateUsualConsosId();
            this.props.toast({message:"Consommable ajouté aux favoris",type:"success"});
        })
    }

    handleFournisseurSelection = (e,{value}) =>{
        this.setState({
            selectedFournisseur:value
        });
    }

    getFournisseurIfOne = () =>{
        if(this.props.consomable.fournisseur != null){
            return this.props.consomable.fournisseur._id
        }else{
            return "";
        }
    }

    triggerOrder = () => {
        if(parseInt(Math.ceil(this.state.orderQuantity)) > 0){
            this.props.client.mutate({
                mutation : this.state.createDemandeConsoQuery,
                variables:{
                consomable:this.state._id,
                quantity:parseInt(Math.ceil(this.state.orderQuantity)),
                from:this.state.fromNote,
                site:this.props.site
                }
            });
        }
        this.closeOrder();
    }

    saveChange = () => {
        this.props.editConsomable(this.state._id,this.state.newRef,this.state.newRefS,this.state.newMinOrder,this.state.newCategorie,this.state.newDesignation,this.state.newModele,this.state.newCurrentlyStored,this.state.newAvCS,this.state.newPrix,this.state.newPackagingFormat,this.state.selectedFournisseur);
        this.toggleEdit();
    }
    
    cancelChange = () => {
        this.toggleEdit();
    }

    toggleEdit = () => {
        this.setState({
            editing:!this.state.editing
        })
    }

    toggleE = () => {
        this.setState({
            isCorrectionE:!this.state.isCorrectionE
        })
    }
    toggleS = () => {
        this.setState({
            isCorrectionS:!this.state.isCorrectionS
        })
    }

    getEmergency = () => {
        if(this.props.consomable.currentlyStored <= (this.props.consomable.fournisseur.deliveryDelay*this.props.consomable.avCS)){
            return 2;
        }
        if((this.props.consomable.currentlyStored <= ((this.props.consomable.fournisseur.deliveryDelay+this.state.delayBeforeEmergency)*this.props.consomable.avCS)) && (this.props.consomable.currentlyStored > (this.props.consomable.fournisseur.deliveryDelay*this.props.consomable.avCS))){
            return 1;
        }
        if(this.props.consomable.currentlyStored > ((this.props.consomable.fournisseur.deliveryDelay+this.state.delayBeforeEmergency)*this.props.consomable.avCS)){
            return 0;
        }
    }

    getRefSupplierIfOne = () => {
        if(this.props.consomable.refS != "-"){
            return(
                <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"0.9em"}}>Fournisseur : {this.props.consomable.refS}</span>
            )
        }
    }

  render() {
    const { _id } = this.state;
    if(!this.state.editing){
        return (
            <Fragment>
                <Table.Row key={_id}>
                    <Table.Cell width={2} textAlign="center" style={{fontSize:"1.2em",padding:"2px 4px"}}>
                        <p style={{margin:"2px"}}>{this.props.consomable.currentlyStored} u.</p>
                        <Label style={{margin:"2px"}} color={this.state.emergencyColors[this.props.consomable.emergencyCode]} horizontal>
                            {this.props.consomable.left}
                        </Label>
                    </Table.Cell>
                    <Table.Cell width={2} style={{fontSize:"1.3em",padding:"4px 32px"}}>
                        {this.props.consomable.ref}<br/>
                        {this.getRefSupplierIfOne()}
                    </Table.Cell>
                    <Table.Cell width={5} style={{fontSize:"1.3em",padding:"4px 32px"}}>
                        {this.props.consomable.categorie+" - "+this.props.consomable.modele}<br/>
                        <span style={{color:"#7f8c8d",marginLeft:"16px",fontSize:"0.9em"}}>{this.props.consomable.designation}</span>
                    </Table.Cell>
                    <Table.Cell textAlign="right" width={2} style={{padding:"0 32px",fontSize:"1.3em"}}>{this.props.consomable.prix} €/u.</Table.Cell>
                    <Table.Cell width={1} style={{padding:"0 32px",fontSize:"1.3em"}}>{this.props.consomable.fournisseur.deliveryDelay} j.</Table.Cell>
                    <Table.Cell width={1} style={{padding:"0 32px",fontSize:"1.3em"}}>{this.props.consomable.avCS} u/j</Table.Cell>
                    <Table.Cell textAlign="center" width={2}>
                        <Dropdown pointing='right' style={{height:"28px",width:"117px",padding:"7px 0"}} text='Actions' icon='caret square down' floating labeled button className='icon'>
                            <Dropdown.Menu>
                                <Dropdown.Item style={{color:"#a29bfe"}} icon="plus" text="Ajouter aux favoris" onClick={this.addToUserUsual}/>
                                <Dropdown.Item style={{color:"#e67e22"}} icon="sign out" text="Sortie" onClick={this.showOut}/>
                                <Dropdown.Item style={{color:"#27ae60"}} icon="sign-in" text="Entrée" onClick={this.showIn}/>
                                <Dropdown.Item style={{color:"#2d3436"}} icon="talk" text="Demander" onClick={this.showOrder} disabled={this.props.consomable.fournisseur._id.length == 0 || this.props.consomable.packagingFormat == 0}/>
                                <Dropdown.Item style={{color:"#2980b9"}} icon="edit outline" text="Editer" onClick={this.toggleEdit}/>
                                <Dropdown.Item style={{color:"#e74c3c"}} icon="trash" text="Supprimer" onClick={this.showDelete}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Table.Cell>
                </Table.Row>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openOut} onClose={this.closeOut} closeIcon>
                    <Modal.Header>
                        Sortie du stock :
                    </Modal.Header>
                    <Modal.Content >
                        <Message color="orange">
                            <Form onSubmit={e=>{e.preventDefault();this.takeFromStock()}} style={{display:'grid',gridTemplateColumns:"3fr 1fr",gridGap:"16px"}}>
                                <Input type="number" name="outputStock" onChange={this.handleChange} placeholder="Quantité concernée ..." size="huge"/>
                                <Button disabled={this.state.noteS == "" || this.state.outputStock <= 0} style={{fontSize:"1.4em"}} onClick={this.takeFromStock} color="orange" animated='fade'>
                                    <Button.Content hidden>Valider la sortie</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='sign out' />
                                    </Button.Content>
                                </Button>
                                <div style={{gridColumnEnd:"span 2"}}>
                                    <Radio onChange={this.toggleS} name="isCorrectionS" defaultChecked={this.state.isCorrectionS} toggle label="Cette sortie est une correction de la quantité en stock"/>
                                </div>
                                <span>
                                    Demande en provenance de {' '}
                                    <SecteurPicker onChange={this.handleSecteurChange} target={"noteS"}/>
                                </span>
                            </Form>
                        </Message>
                    </Modal.Content>
                </Modal>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openIn} onClose={this.closeIn} closeIcon>
                    <Modal.Header>
                        Entrée dans le stock :
                    </Modal.Header>
                    <Modal.Content >
                        <Message color="green">
                            <Form onSubmit={e=>{e.preventDefault();this.addToStock()}} style={{display:'grid',gridTemplateColumns:"3fr 1fr",gridGap:"16px"}}>
                                <Input type="number" name="inputStock" onChange={this.handleChange} placeholder="Quantité concernée ..." size="huge"/>
                                <Button disabled={this.state.noteE == "" || this.state.inputStock <= 0} style={{fontSize:"1.4em"}} onClick={this.addToStock} color="green" animated='fade'>
                                    <Button.Content hidden>Valider l'entrée</Button.Content>
                                    <Button.Content visible>
                                        <Icon name='sign-in' />
                                    </Button.Content>
                                </Button>
                                <div style={{gridColumnEnd:"span 2"}}>
                                    <Radio onChange={this.toggleE} name="isCorrectionE" defaultChecked={this.state.isCorrectionE} toggle label="Cette entrée est une correction de la quantité en stock"/>
                                </div>
                                <span>
                                    Demande en provenance de {' '}
                                    <SecteurPicker onChange={this.handleSecteurChange} target={"noteE"}/>
                                </span>
                            </Form>
                        </Message>
                    </Modal.Content>
                </Modal>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="small" open={this.state.openDelete} onClose={this.closeDelete} closeIcon>
                    <Modal.Header>
                        Confirmez la suppression du consomable:
                    </Modal.Header>
                    <Modal.Content style={{textAlign:"center"}}>
                        <Message color='red' icon>
                            <Icon name='warning sign'/>
                            <Message.Content style={{display:"grid",gridTemplateColumns:"1fr 2fr",gridTemplateRows:"1fr 1fr"}}>
                                <p style={{margin:"8px 4px",placeSelf:"center end"}}>Designation :</p>
                                <p style={{margin:"8px 4px",fontWeight:"800",placeSelf:"center start"}}>
                                    {this.props.consomable.designation}
                                </p>
                                <p style={{margin:"8px 4px",placeSelf:"center end"}}>Reference :</p>
                                <p style={{margin:"8px 4px",fontWeight:"800",placeSelf:"center start"}}>
                                    {this.props.consomable.ref}
                                </p>
                            </Message.Content>
                        </Message>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="red" onClick={()=>{this.removeConsomable(_id)}}>Supprimer</Button>
                    </Modal.Actions>
                </Modal>
                <Modal closeOnDimmerClick={false} dimmer="inverted" open={this.state.openOrder} onClose={this.closeOrder} closeIcon>
                    <Modal.Header>
                        Demande de commande du consomable :
                    </Modal.Header>
                    <Modal.Content >
                        <Message color="blue">
                            <Form onSubmit={e=>{e.preventDefault();this.triggerOrder()}}>
                                <div style={{display:'grid',gridTemplateColumns:"2fr 1fr 1fr",gridTemplateRows:"auto 1fr",gridGap:"16px"}}>
                                    <Input type="number" name="orderQuantity" onChange={this.handleChange} placeholder="Quantité concernée ..." size="huge"/>
                                    <div style={{padding:"24px 4px"}}>
                                        <p>{this.state.orderQuantity + "  " + this.props.consomable.packagingFormat}</p>
                                        <p>{(this.state.orderQuantity * this.props.consomable.prix).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</p>
                                    </div>
                                    <Button disabled={this.state.fromNote == "" || this.state.orderQuantity <= 0} style={{fontSize:"1.4em"}} onClick={this.triggerOrder} color="blue" animated='fade'>
                                        <Button.Content hidden>Poursuivre</Button.Content>
                                        <Button.Content visible>
                                            <Icon name='talk' />
                                        </Button.Content>
                                    </Button>
                                    <span>
                                        Demande en provenance de {' '}
                                        <SecteurPicker onChange={this.handleSecteurChange} target={"fromNote"}/>
                                    </span>
                                </div>
                            </Form>
                        </Message>
                    </Modal.Content>
                </Modal>
            </Fragment>
        )
    }else{
        return (
            <Fragment>
                <Table.Row key={_id}>
                    <Table.Cell colSpan="8" style={{padding:"16px 16px 16px 32px"}}>
                        <Form style={{width:"100%"}}>
                            <Grid>
                                <Grid.Column width={12}>
                                    <div style={{display:"grid",gridGap:"16px",gridTemplateColumns:"1fr 1fr 1fr",gridTemplateRows:"1fr 1fr 1fr 1fr"}}>
                                        <div style={{gridArea:"1/1/1/span 1"}}>
                                            <Form.Input size="huge" label='Réference' placeholder={this.props.consomable.ref} defaultValue={this.props.consomable.ref} name='newRef' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"1/2/1/span 1"}}>
                                            <Form.Input size="huge" label='Réference Fournisseur' placeholder={this.props.consomable.refS} defaultValue={this.props.consomable.refS} name='newRefS' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"1/3/1/span 1"}}>
                                            <Dropdown clearable style={{fontSize:"1.4em",marginTop:"22px",height:"53px",width:"100%"}} placeholder="Définir un fournisseur ..." defaultValue={this.getFournisseurIfOne()} onChange={this.handleFournisseurSelection} fluid search scrolling selection options={this.props.fournisseursMappedList} />
                                        </div>
                                        <div style={{gridArea:"2/1/2/span 1"}}>
                                            <Form.Input size="huge" label='Catégorie' placeholder={this.props.consomable.categorie} defaultValue={this.props.consomable.categorie} name='newCategorie' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"2/2/2/span 1"}}>
                                            <Form.Input size="huge" label='Désignation' placeholder={this.props.consomable.designation} defaultValue={this.props.consomable.designation} name='newDesignation' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"2/3/3/span 1"}}>
                                            <Form.Input size="huge" label='Modèle' placeholder={this.props.consomable.modele} defaultValue={this.props.consomable.modele} name='newModele' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"3/1/3/span 1"}}>
                                            <Form.Input type="number" size="huge" label='Consomation journalière moyenne' placeholder={this.props.consomable.avCS} defaultValue={this.props.consomable.avCS} name='newAvCS' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"3/2/3/span 1"}}>
                                            <Form.Input type="number" size="huge" label='Prix' placeholder={this.props.consomable.prix} defaultValue={this.props.consomable.prix} name='newPrix' onChange={this.handleChange}/>
                                        </div>
                                        <div style={{gridArea:"3/3/3/span 1"}}>
                                            <Form.Input type="number" size="huge" label='Seuil de commande minimum' placeholder={this.props.consomable.minOrder} defaultValue={this.props.consomable.minOrder} name='newMinOrder' onChange={this.handleChange}/>                                        
                                        </div>
                                        <div style={{gridArea:"4/1/4/span 3"}}>
                                            <Form.Input size="huge" label='Conditionement' placeholder={this.props.consomable.packagingFormat} defaultValue={this.props.consomable.packagingFormat} name='newPackagingFormat' onChange={this.handleChange}/>
                                        </div>
                                    </div>
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <div style={{height:"100%",display:"grid",gridTemplateRows:"1fr 1fr",padding:"16px",gridGap:"28px"}}>
                                        <Button style={{fontSize:"1.4em"}} onClick={this.saveChange} color="blue" animated='fade'>
                                            <Button.Content hidden>Valider</Button.Content>
                                            <Button.Content visible>
                                                <Icon name='check' />
                                            </Button.Content>
                                        </Button>
                                        <Button style={{fontSize:"1.4em"}} onClick={this.cancelChange} color="red" animated='fade'>
                                            <Button.Content hidden>Annuler</Button.Content>
                                            <Button.Content visible>
                                                <Icon name='cancel' />
                                            </Button.Content>
                                        </Button>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Table.Cell>
                </Table.Row>
            </Fragment>
        )
    }
  }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default wrappedInUserContext = withUserContext(StockRow);
  