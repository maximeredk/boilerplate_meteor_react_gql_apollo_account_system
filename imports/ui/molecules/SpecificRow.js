import React, { Component, Fragment } from 'react'
import { Table,Button,Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import _ from 'lodash';

export class SpecificRow extends Component {

    state = {
        _id : this.props.specific._id,
        removeSpecificQuery : gql`
            mutation removeSpecific($_id:String!){
                removeSpecific(_id:$_id){
                    _id
                    object
                    fromNote
                    comment
                }
            }
        `
    }

    removeSpecific = () => {
        this.props.client.mutate({
            mutation : this.state.removeSpecificQuery,
            variables:{
                _id:this.state._id
            }
        }).then(({data})=>{
            this.props.reloadSpecific();
        })
    }

    render() {
        return (
            <Table.Row>
                <Table.Cell style={{textAlign:"justify"}}>
                    {this.props.specific.object}
                </Table.Cell>
                <Table.Cell style={{textAlign:"justify"}}>
                    {this.props.specific.fromNote}
                </Table.Cell>
                <Table.Cell style={{textAlign:"justify"}}>
                    {this.props.specific.comment}
                </Table.Cell>
                <Table.Cell textAlign="center">
                    <Button color="red" onClick={this.removeSpecific} content={"Supprimer"} icon='trash'/>
                </Table.Cell>
            </Table.Row>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default wrappedInUserContext = withUserContext(SpecificRow);