import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';

export class MenuTile extends Component {
  render() {
    return (
        <div onClick={()=>{this.props.callback(this.props.menuItem.link)}} style={{
            fontFamily:"'Quicksand', sans-serif",
            color:"#1b2845",
            fontWeight:"bold",
            fontSize:"1.5em",
            placeSelf:"stretch",
            flexBasis:"180px",
            flexGrow:"0",
            height:"100px",
            backgroundColor:"#2f4353",
            backgroundSize:"cover",
            backgroundImage:"linear-gradient(160deg, #b8c6db 0%, #f5f7fa 74%)",
            borderRadius:"4px",
            cursor:"pointer",
            margin:"16px",
            padding:"8px",
            display:"grid",
            gridTemplateRows:"1fr auto"
        }}>
            <Icon style={{placeSelf:"center",gridRowStart:"1",}} name={this.props.menuItem.icon} size='large' />
            <p style={{placeSelf:"center",gridRowStart:"2"}}>{this.props.menuItem.label.toUpperCase()}</p>
        </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
<UserContext.Consumer>
    {ctx => <WrappedComponent {...ctx} {...props}/>}
</UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(MenuTile);