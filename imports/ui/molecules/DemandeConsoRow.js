import React, { Component, Fragment } from 'react'
import { Table,Button,Modal,Form,Input,Segment,List,Header,Label } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import _ from 'lodash';
import moment from 'moment';

class DemandeConsoRow extends Component {

    state={
        _id:this.props.demandesConso._id,
        open:false,
        internalRef:"",
        orderQuantity:this.props.demandesConso.agglomeratedConsomable.map(consoFromFournisseur=>{
            return({
                    _id:consoFromFournisseur._id,
                    quantity:consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0),
                    demandes:consoFromFournisseur.demandes
            })
        }),
        createCommandeQuery : gql`
            mutation createCommande($consomables:String!,$internalRef:String!,$fournisseur:String!,$site:String!){
                createCommande(consomables:$consomables,internalRef:$internalRef,fournisseur:$fournisseur,site:$site){
                    _id
                    name
                    deliveryDelay
                    agglomeratedConsomable{
                        _id
                        ref
                        refS
                        categorie
                        modele
                        designation
                        currentlyStored
                        avCS
                        packagingFormat
                        minOrder
                        fournisseur{
                            _id
                        }
                        demandes{
                            _id
                            quantity
                            from
                            entryDay
                            entryMonth
                            entryYear
                            entryHour
                            entryMinute
                            entrySecond  
                        }
                    }
                }
            }
        `
    }

    handleChange = e =>{
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    handleCorrection = (c,e) => {
        let v = e.target.value;
        if(v.length == 0){v = 0}
        let orderQuantity = this.state.orderQuantity;        
        orderQuantity.filter(cons=>cons._id == c._id)[0].quantity = this.getAtLeastMinOrder(c,parseInt(v)) || 0;
        this.setState({
            orderQuantity:orderQuantity
        })
    }

    getAtLeastMinOrder = (c,v) => {
        if(v < c.minOrder){
            return c.minOrder;
        }else{
            return v;
        }
    }

    getFromNote = commandNotes => {
        if(JSON.parse(commandNotes).length > 0){
            return(
                <List style={{padding:"32px",textAlign:"left"}} divided>
                    {JSON.parse(commandNotes).map(d=>
                        <List.Item key={d}>
                            <List.Content>
                                <List.Header>{d.fromNote}<span style={{float:"right"}}>{d.dateTime}</span></List.Header>
                                <List.Description>{d.quantity}</List.Description>
                            </List.Content>
                        </List.Item>
                    )}
                </List>
            )
        }else{
            return (
                <Header as="h3">Aucune note de provenance</Header>
            )
        }
    }

    createCommand = () => {
        this.props.client.mutate({
            mutation : this.state.createCommandeQuery,
            variables:{
                consomables: JSON.stringify(this.state.orderQuantity),
                internalRef:this.state.internalRef,
                fournisseur:this.props.demandesConso._id,
                site:this.props.site
            }
        }).then(({data})=>{
            this.props.setDemandesConsoRaw(data.createCommande);
        });
        this.close();
    }

    getCancelButton = d => {
        if(this.props.user.isAdmin || this.props.user.isOWner || this.props.user._id == d.user){
            return <Label onClick={()=>{this.props.deleteDemandeConso(d._id)}} style={{cursor:"pointer"}} color="red">Annuler</Label>
        }
    }

    componentDidMount = () => {
        let commandQuantity = [];
        {this.props.demandesConso.agglomeratedConsomable.map(consoFromFournisseur=>{
            commandQuantity.push({_id:consoFromFournisseur._id,quantity:consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0),demandes:consoFromFournisseur.demandes});
        })}
        this.setState({
            orderQuantity:commandQuantity
        })
    }

    close = () => {
        this.setState({
          open:false
        })
    }
    
    open = () => {
        this.setState({
            open:true,
            orderQuantity:this.props.demandesConso.agglomeratedConsomable.map(consoFromFournisseur=>{
                return({
                        _id:consoFromFournisseur._id,
                        quantity:consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0),
                        demandes:consoFromFournisseur.demandes
                })
            })
        })
    }

    render() {
        let firstnameMain,lastnameMain,firstnameBackup,lastnameBackup,idMain,idBackup = "";
        if(this.props.GDS_main != ""  && this.props.GDS_main != undefined){
            firstnameMain = this.props.GDS_main.firstname
            lastnameMain = this.props.GDS_main.lastname
            idMain = this.props.GDS_main._id
        }
        if(this.props.GDS_backup != "" && this.props.GDS_backup != undefined){
            firstnameBackup = this.props.GDS_backup.firstname
            lastnameBackup = this.props.GDS_backup.lastname
            idBackup = this.props.GDS_backup._id
        }
        return (
            <Fragment>
                <Segment raised style={{marginBottom:"32px"}}>
                    <div style={{display:"flex",justifyContent:"space-between"}}>
                        <div>
                            fournisseur : <Header as="h2">{this.props.demandesConso.name}</Header>
                        </div>
                        <div style={{display:"grid",gridTemplateColumns:"auto auto",gridGap:"16px"}}>
                            <div>
                                <p style={{color:"#95a5a6"}}>main : {firstnameMain +" "+ lastnameMain}</p>
                                <hr/>
                                <p style={{color:"#95a5a6"}}>backup : {firstnameBackup +" "+ lastnameBackup}</p>
                            </div>
                            <Button disabled={(Meteor.userId() != idMain && Meteor.userId() != idBackup)} color="blue" onClick={()=>{this.open()}} content={"Passer commande"} icon='shipping' labelPosition='right'/>
                        </div>
                    </div>
                    {this.props.demandesConso.agglomeratedConsomable.map(consoFromFournisseur=>{
                        return(
                            <Table key={"conso"+consoFromFournisseur._id} color="blue" celled selectable compact>
                                <Table.Header>
                                    <Table.Row textAlign='center'>
                                        <Table.HeaderCell colSpan="2" width={12}>
                                            <div style={{display:"grid",gridTemplateColumns:"1fr 4fr"}}>
                                                <div style={{placeSelf:"center",margin:"0 16px",color:"#7f8c8d",fontSize:"1.05em"}}>
                                                    Consommable :
                                                </div>
                                                <div style={{margin:"0 16px",fontSize:"1.4em",display:"grid",gridTemplateColumns:"auto auto auto"}}>
                                                    <p style={{placeSelf:"center",marginBottom:"0"}}>{consoFromFournisseur.categorie}</p>
                                                    <p style={{placeSelf:"center",marginBottom:"0"}}>{consoFromFournisseur.modele}</p>
                                                    <p style={{placeSelf:"center",marginBottom:"0"}}>{consoFromFournisseur.designation}</p>
                                                </div>
                                            </div>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell colSpan="2" width={4}>
                                            <span style={{margin:"0 16px",color:"#7f8c8d",fontSize:"1.05em"}}>
                                                Conditionnement :
                                            </span>
                                            <span style={{margin:"0 16px",fontSize:"1.3em"}}>
                                                {consoFromFournisseur.packagingFormat}
                                            </span>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell textAlign="center" width={4}>
                                            Date de demande
                                        </Table.HeaderCell>
                                        <Table.HeaderCell textAlign="center" width={6}>
                                            Quantité demandée
                                        </Table.HeaderCell>
                                        <Table.HeaderCell textAlign="center" width={4}>
                                            Secteur de provenance
                                        </Table.HeaderCell>
                                        <Table.HeaderCell textAlign="center" width={2}>
                                            Annulation
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {consoFromFournisseur.demandes.map(d=>{
                                        return(
                                            <Table.Row key={d._id}>
                                                <Table.Cell textAlign="center" width={4}>
                                                    {moment(new Date(d.entryYear,d.entryMonth,d.entryDay,d.entryHour,d.entryMinute,d.entrySecond).toString()).format('DD/MM/YY - HH:mm:ss')}
                                                </Table.Cell>
                                                <Table.Cell textAlign="center" width={6}>
                                                        <p style={{justifySelf:"center",marginBottom:"0"}}>
                                                            {d.quantity}
                                                        </p>
                                                </Table.Cell>
                                                <Table.Cell style={{paddingLeft:"64px"}} width={4}>
                                                    {d.from}
                                                </Table.Cell>
                                                <Table.Cell textAlign="center" width={2}>
                                                    {this.getCancelButton(d)}
                                                </Table.Cell>
                                            </Table.Row>
                                        )
                                    })}
                                </Table.Body>
                                <Table.Header>
                                    <Table.Row style={{fontSize:"1.3em",fontWeight:"bold"}}>
                                        <Table.HeaderCell textAlign="center" width={6}>
                                            Totaux :
                                        </Table.HeaderCell>
                                        <Table.HeaderCell textAlign="center" width={6}>
                                            {consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0)} u.<br/>
                                            <p style={{fontSize:"0.8em",color:"#95a5a6"}}>(minimum : {consoFromFournisseur.minOrder})</p>
                                        </Table.HeaderCell>
                                        <Table.HeaderCell textAlign="center" colSpan="2" width={4}>
                                            {((consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0)) * consoFromFournisseur.prix).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                            </Table>
                        )
                    })}
                </Segment>
                <Modal closeOnDimmerClick={false} dimmer="inverted" size="large" open={this.state.open} onClose={this.close}>
                    <Modal.Header>
                        Créer une commande d'après les demandes pour les consomables du fournisseur {this.props.demandesConso.name}
                    </Modal.Header>
                    <Modal.Content>
                        <Modal.Description style={{margin:"0 32px 24px 32px"}}>
                            Vous pouvez commander un montant different du total des demandes en modifiant les quantités dans la colone de correction à droite
                        </Modal.Description>
                        <Form onSubmit={e=>{e.preventDefault();this.createCommand()}}>
                            <Table color="blue" celled selectable compact>
                                <Table.Header>
                                    <Table.Row textAlign='center'>
                                        <Table.HeaderCell width={8}>
                                            Consomable
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={4}>
                                            Quantité demandé
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={4}>
                                            Correction quantité
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.props.demandesConso.agglomeratedConsomable.map(consoFromFournisseur=>{
                                        return(
                                            <Table.Row key={"order"+consoFromFournisseur._id}>
                                                <Table.Cell>
                                                    <p style={{placeSelf:"center",marginBottom:"0"}}>{consoFromFournisseur.categorie + " " + consoFromFournisseur.modele + " " + consoFromFournisseur.designation}</p><br/>
                                                    <p style={{color:"#95a5a6"}}>({consoFromFournisseur.packagingFormat})</p>
                                                </Table.Cell>
                                                <Table.Cell textAlign="center">
                                                    <div>
                                                        {consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0)}<br/>
                                                        <p style={{color:"#95a5a6"}}>(minimum : {consoFromFournisseur.minOrder})</p>
                                                    </div>
                                                </Table.Cell>
                                                <Table.Cell textAlign="center">
                                                    <div>
                                                        <Input type="number" style={{marginBottom:"4px"}} placeholder='Quantité' defaultValue={this.getAtLeastMinOrder(consoFromFournisseur,consoFromFournisseur.demandes.reduce(function(a,b){return a + b.quantity;},0))} onChange={e=>{this.handleCorrection(consoFromFournisseur,e)}}/><br/>
                                                        soit : <Label style={{fontSize:"1.1em",margin:"0",padding:"2px 4px"}}>{(this.state.orderQuantity.find(cons=>cons._id == consoFromFournisseur._id).quantity * consoFromFournisseur.prix || 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €</Label>
                                                    </div>
                                                </Table.Cell>
                                            </Table.Row>
                                        )
                                    })}
                                </Table.Body>
                                <Table.Header>
                                    <Table.Row style={{fontSize:"1.3em",fontWeight:"bold"}}>
                                    <Table.HeaderCell textAlign="center">
                                            Total :
                                        </Table.HeaderCell>
                                        <Table.HeaderCell colSpan="2" textAlign="center">
                                            {this.props.demandesConso.agglomeratedConsomable.reduce((a,b)=>{return(a+(b.prix*(this.state.orderQuantity.find(cons=>cons._id == b._id).quantity)))},0).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'")} €
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                            </Table>
                            <Form.Input label="Référence interne de la commande" name="internalRef" onChange={this.handleChange}/>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.createCommand()}} disabled={this.state.internalRef.length == 0} content='Créer la commande' icon='right arrow' labelPosition='right'/>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
)
  
export default wrappedInUserContext = withUserContext(DemandeConsoRow);
  