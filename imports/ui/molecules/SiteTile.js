import React, { Component } from 'react'
import { UserContext } from '../../contexts/UserContext';

//backgroundImage:"linear-gradient(220deg, #485461 0%, #28313b 100%)",

export class SiteTile extends Component {
    
    state={
        ss:this.props.site
    }

    getSelectedStyle = () => {
        if(this.props.s.key == this.props.selected)
        return ({
            fontSize:"2.2em",
            flexBasis:"260px",
            height:"150px",
            backgroundImage:"linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ),url('/res/wall/storage.jpg')",
            boxShadow:"3px 0 5px black",
            filter: 'grayscale(0%)'
        })
    }

    render() {
        return (
            <div onClick={()=>{this.props.callback(this.props.s.key)}} style={Object.assign({
                fontFamily:"'Quicksand', sans-serif",
                color:"#fff",
                fontWeight:"bold",
                fontSize:"2em",
                alignItems:"center",
                filter: 'grayscale(100%)',
                flexBasis:"240px",
                height:"120px",
                backgroundColor:"#2f4353",
                backgroundSize:"cover",
                backgroundImage:"linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4) ),url('/res/wall/storage.jpg')",
                borderRadius:"4px",
                cursor:"pointer",
                margin:"16px",
                padding:"8px",
                display:"grid",
                gridTemplateRows:"1fr auto"
            },this.getSelectedStyle())}>
                <p style={{placeSelf:"start",gridRowStart:"2"}}>{this.props.s.text.toUpperCase()}</p>
            </div>
        )
    }
}

const withUserContext = WrappedComponent => props => (
<UserContext.Consumer>
    {ctx => <WrappedComponent {...ctx} {...props}/>}
</UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(SiteTile);