import React, { Component } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Session } from 'meteor/session'

import PageBody from '../../pages/PageBody';
import Menu from '../../menu/Menu';
import Home from '../../pages/Home';
import NeedActivation from '../../pages/NeedActivation';
import { UserContext } from '../../../contexts/UserContext';
import './AppBody.css';

class AppBody extends Component{

    componentDidMount = () => {
        toast.configure()
    }

    logout = () => {
        Meteor.logout();
        this.props.client.resetStore();
    }

    render(){
        if(this.props.user._id != null){
            if(this.props.user.activated){
                return(
                    <div className="connectedWrapper">
                        <Menu/>
                        <ToastContainer position="top-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl pauseOnVisibilityChange draggable pauseOnHover/>
                        <PageBody site={this.props.site}/>
                    </div>
                );
            }else{
                return(
                    <div className="appBody">
                        <div className="titleScreenLayout">
                            <img className="needActivationWrapper" src={"/res/title.png"} alt="titleLogo"/>
                            <NeedActivation/>
                            <Button basic className="needActivationLogoutButton" onClick={this.logout} color="red">
                                Déconnexion
                            </Button>
                        </div>
                    </div>
                );
            }
        }else{
            return(
                <div className="appBody">
                    <ToastContainer position="bottom-right" autoClose={5000} hideProgressBar={false} newestOnTop={false} closeOnClick rtl pauseOnVisibilityChange draggable pauseOnHover/>
                    <Switch>
                        <Route path='/' component={Home}/>
                        <Redirect from='*' to={'/'}/>
                    </Switch>
                </div>
            )
        }
    }
}

const withUserContext = WrappedComponent => props => (
    <UserContext.Consumer>
        {ctx => <WrappedComponent {...ctx} {...props}/>}
    </UserContext.Consumer>
  )
  
  export default withUserContext(AppBody);