import React, { Component } from 'react';
import { Menu, Table } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import RoleCell from '../molecules/RoleCell';
import 'moment/locale/fr';

export class Roles extends Component {

  state = {
    roles:[
        {key:"GDS",text:"GDS",value:"GDS"},
        {key:"commandAdmin",text:"Chargé.e de commande",value:"commandAdmin"},
        {key:"validationDSL",text:"Validation DSL",value:"validationDSL"}
    ],
    rolesUsers:[],
    accounts:[],
    usersQuery:gql`
      query Users{
        users{
          _id
          email
          isAdmin
          isOwner
          verified
          firstname
          lastname
          createdAt
          lastLogin
          activated
        }
      }
    `,
    rolesQuery: gql` query Roles{
      roles{
        _id
        site
        role
        user
      }
    }`
  }

  componentDidMount = () => {
    this.props.client.query({
      query: this.state.usersQuery
    }).then(({data}) => {
      this.setState({
        accounts:data.users.map(u=>{return{key:u._id,text:u.firstname + " " + u.lastname,value:u._id}})
      })
    })
  }

  getMenu = () => {
    if(this.props.user.isOwner){
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' active onClick={()=>{this.props.history.push("/administration/roles")}} />
          <Menu.Item color="red" name='Réinitialisation' onClick={()=>{this.props.history.push("/administration/init")}} />
          <Menu.Item color="green" name='Import/Export' onClick={()=>{this.props.history.push("/administration/impexp")}} />
        </Menu>
      )
    }else{
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' active onClick={()=>{this.props.history.push("/administration/roles")}} />
        </Menu>
      )
    }
  }

  render() {
    return (
      <div>
        <div style={{display:"flex",marginBottom:"64px",justifyContent:"space-between"}}>
          {this.getMenu()}
        </div>
        <Table basic='very' celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell style={{fontSize:"1.6em",fontWeight:"700",color:"#95afc0"}}>roles</Table.HeaderCell>
                    {this.props.sites.map(site =>
                        <Table.HeaderCell style={{fontSize:"1.6em",fontWeight:"700",color:"#95afc0"}} key={"cell"+site.value}>{site.text.toUpperCase()}</Table.HeaderCell>
                    )}
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {this.state.roles.map(role =>
                    <Table.Row key={"row"+role.value}>
                        <Table.Cell>
                            {role.text}
                        </Table.Cell>
                        {this.props.sites.map(site =>
                            <RoleCell key={"cell"+site.value} accounts={this.state.accounts} site={site} role={role} />
                        )}
                    </Table.Row>
                )}
            </Table.Body>
        </Table>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Roles);