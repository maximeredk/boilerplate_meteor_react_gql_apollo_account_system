import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base'
import { Header,Icon,Segment,Message,Button,Form } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';

export class Compte extends Component {

  state={
    oldPass:"",
    newPass1:"",
    newPass2:""
  }

  handleChange = e =>{
    this.setState({
      [e.target.name]:e.target.value
    });
  }

  applyPassword = () => {
    Accounts.changePassword(this.state.oldPass,this.state.newPass1,err=>{
      if(!err){
        this.props.toast({message:"Mot de passe modifié avec succès",type:"info"});
      }else{
        this.props.toast({message:err.reason,type:"error"});
      }
    });
  }

  getModalLabel = ({error,errorContent}) => {
    if(error){
      return(
        <Message style={{margin:"0",justifySelf:"start"}} color="blue" content={errorContent}/>
      )
    }
  }

  render() {
    const { newPass1,newPass2 } = this.state;
    let error = false;
    let errorContent = "";
    if(newPass1.length == 0 || newPass2.length == 0){
      error = true;
      errorContent = "Veuillez renseigner tous les champs";
    }else{
      if(newPass1 != newPass2){
        error = true;
        errorContent = "Les mots de passe sont different";
      }else{
        if(newPass1.length < 6){
          error = true;
          errorContent = "Le mot de passe nécessite 6 caractères minimum";
        }
      }
    }
    return (
      <div style={{display:"grid",gridTemplateColumns:"1fr 75% 1fr"}}>
        <Segment raised style={{display:"grid",gridColumnStart:"2",gridGap:"16px",placeSelf:"stretch",marginTop:"32px",padding:"24px 0",gridTemplateColumns:"1fr 3fr"}}>
          <div style={{display:"grid",borderRight:"6px solid #74b9ff"}}>
            <Header style={{placeSelf:"center"}} as='h3' icon>
              <Icon circular name='key'/>
              Modifier votre mot de passe
            </Header>
          </div>
          <Form style={{display:"grid",gridTemplateRows:"1fr 1fr 1fr auto",gridColumnGap:"32px",margin:"16px 48px 0 48px"}}>
            <Form.Input type="password" onChange={this.handleChange} autoComplete="off" name="oldPass" label="Ancien mot de passe"/>
            <Form.Input type="password" onChange={this.handleChange} autoComplete="off" name="newPass1" label="Nouveau mot de passe"/>
            <Form.Input type="password" onChange={this.handleChange} autoComplete="off" name="newPass2" label="Confirmez le nouveau mot de passe"/>
            <div style={{paddingTop:"16px",justifySelf:"stretch",display:"grid",gridTemplateColumns:"1fr auto"}}>
              {this.getModalLabel({error:error,errorContent:errorContent})}
              <Button onClick={this.applyPassword} style={{gridColumnStart:"2",width:"160px"}} color="blue" size="big" content="Valider"/>
            </div>
          </Form>
        </Segment>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Compte);