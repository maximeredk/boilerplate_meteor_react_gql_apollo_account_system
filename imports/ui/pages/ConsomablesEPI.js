import React, { Component } from 'react'
import { Header } from 'semantic-ui-react';
import { SiteTile } from '../molecules/SiteTile';
import { MenuTile } from '../molecules/MenuTile';
import { UserContext } from '../../contexts/UserContext';

export class ConsomablesEPI extends Component {

  state={
    email:"",
    pass:"",
    open:false,
    firstname:"",
    lastname:"",
    password:"",
    passwordAgain:"",
    error:false,
    errorContent:"",
    mail:"",
    menu:[
      {label:"Magasin",link:"/consomables/magasin",icon:"shopping cart"},
      {label:"Stocks",link:"/consomables/stock",icon:"chart bar"},
      {label:"Demandes",link:"/consomables/demandes",icon:"talk"},
      {label:"Commandes",link:"/consomables/commandes",icon:"shipping"},
      {label:"Fournisseurs",link:"/consomables/fournisseurs",icon:"briefcase"},
      {label:"Historique",link:"/consomables/historique",icon:"calendar alternate outline"}
    ]
  }

  setSite =  s  => {
    this.props.setSite(s);
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  } 

  getSiteTiles = () => {
    return (
      <div style={{gridRowStart:"3",gridColumnStart:"2",gridColumnEnd:"span 3",alignItems:"center",display:"flex",flexWrap:"wrap",justifyContent:"center"}}>
        {this.props.sites.map(s=>{
          return <SiteTile key={"tile"+s.key} callback={this.setSite} selected={this.props.site} s={s}/>
        })}
      </div>
    )
  }

  getMenuTiles = () => {
    return (
      <div style={{gridRowStart:"1",gridColumnStart:"2",gridColumnEnd:"span 3",placeSelf:"center stretch",display:"flex",flexWrap:"wrap",justifyContent:"center"}}>
        {this.state.menu.map(m=>{
          return <MenuTile key={"tile"+m.link} callback={link=>{this.props.history.push(link)}} menuItem={m}/>
        })}
      </div>
    )
  }

  render() {
    return (
      <div style={{display:"grid",marginTop:"40px",gridTemplateColumns:"80px 1fr 5fr 1fr 80px",gridTemplateRows:"1fr 120px 2fr",flexWrap:"wrap",justifyContent:"center",width:"100%"}}>
        {this.getMenuTiles()}
        <Header as="h2" style={{fontFamily:"'Quicksand', sans-serif",gridRowStart:"2",gridColumnStart:"3",placeSelf:"center"}}> SELECTION DU SITE </Header>
        {this.getSiteTiles()}
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(ConsomablesEPI);