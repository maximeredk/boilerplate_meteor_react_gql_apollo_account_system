import React, { Component,Fragment } from 'react'
import { Input,Menu,Button,Grid,Form,Modal,Table,Icon } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import FournisseurRow from '../molecules/FournisseurRow';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

export class Fournisseurs extends Component {

  state={
    fournisseursRaw:[],
    fournisseursFilter:"",
    newName:"",
    fournisseursQuery : gql`
      query Fournisseurs($site: String!){
        fournisseurs(site:$site){
          _id
          name
          deliveryDelay
        }
      }
    `,
    addFournisseurQuery : gql`
      mutation addFournisseur($name:String!,$site:String!){
        addFournisseur(name:$name,site:$site) {
          _id
          name
          deliveryDelay
        }
      }
    `,
    editFournisseurQuery : gql`
      mutation editFournisseur($_id:String!,$name:String!,$deliveryDelay:Int!,$site:String!){
        editFournisseur(_id:$_id,name:$name,deliveryDelay:$deliveryDelay,site:$site){
          _id
          name
          deliveryDelay
        }
      }
    `,
    removeFournisseurQuery : gql`
      mutation removeFournisseur($_id:String!,$site:String!){
        removeFournisseur(_id: $_id,site:$site){
          _id
          name
          deliveryDelay
        }
      }
    `,
    fournisseurs: () =>{
      if(this.state.fournisseursFilter.length != 0){
        return this.state.fournisseursRaw.filter(f => f.name.toLowerCase().includes(this.state.fournisseursFilter.toLowerCase()));
      }else{
        return this.state.fournisseursRaw;
      }
    }
  }

  handleFilter= e => {
    this.setState({
      fournisseursFilter : e.target.value
    })
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  addFournisseur = () => {
    this.close();
    this.props.client.mutate({
      mutation : this.state.addFournisseurQuery,
      variables:{
        name:this.state.newName,
        site:this.props.site
      }
    }).then(({data})=>{
      this.setState({
        fournisseursRaw:data.addFournisseur
      });
    })
  }

  removeFournisseur = _id => {
    this.props.client.mutate({
      mutation : this.state.removeFournisseurQuery,
      variables:{
          _id:_id,
          site:this.props.site
      }
    }).then(({data})=>{
      this.setState({
        fournisseursRaw:data.removeFournisseur
      });
    })
  }

  editFournisseur = (_id,name,deliveryDelay) => {
    this.props.client.mutate({
      mutation : this.state.editFournisseurQuery,
      variables:{
          _id:_id,
          name:name,
          deliveryDelay:parseInt(deliveryDelay),
          site:this.props.site
      }
    }).then(({data})=>{
      this.setState({
        fournisseursRaw:data.editFournisseur
      });
    })
  }

  loadFournisseurs = () => {
    this.props.client.query({
      query : this.state.fournisseursQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        fournisseursRaw:data.fournisseurs
      });
    })
  }

  componentDidMount = () => {
    this.loadFournisseurs();
  }

  render() {
    return (
      <Fragment>
        <div style={{height:"100%",padding:"8px",display:"grid",gridGap:"32px",gridTemplateRows:"auto 80vh",gridTemplateColumns:"auto 1fr 1fr"}}>
          <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
            <Menu.Item color="blue" name='Magasin' onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
            <Menu.Item color="blue" name='Stocks' onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
            <Menu.Item color="blue" name='Demandes' onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
            <Menu.Item color="blue" name='Commandes' onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
            <Menu.Item color="blue" name='Fournisseurs' active onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase' />Fournisseurs</Menu.Item>
            <Menu.Item color="blue" name='Historique' onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
          </Menu>
          <Input style={{placeSelf:"stretch"}} name="fournisseurFilter" onChange={this.handleFilter} icon='search' placeholder='Rechercher un fournisseur ...' />
          <Button color="blue" onClick={()=>{this.popModal()}} content='Ajouter un fournisseur' icon='plus' labelPosition='right'/>
          <div style={{gridRowStart:"2",gridColumnEnd:"span 3",display:"block",overflowY:"auto",placeSelf:"stretch"}}>
            <Table celled selectable compact>
              <Table.Header>
                <Table.Row textAlign='center'>
                  <Table.HeaderCell width={10}>Name</Table.HeaderCell>
                  <Table.HeaderCell width={4}>Delai de livraison</Table.HeaderCell>
                  <Table.HeaderCell width={2}>Actions</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
            </Table>
            <Table celled selectable color="blue" compact>
              <Table.Body>
                {this.state.fournisseurs().map(f=>
                    <FournisseurRow key={f._id} editFournisseur={this.editFournisseur} removeFournisseur={this.removeFournisseur} fournisseur={f}/>
                )}
              </Table.Body>
            </Table>
          </div>
        </div>
        <Modal dimmer="inverted" size="large" open={this.state.open} onClose={this.close}>
          <Modal.Header>
            Ajout d'un nouveau fournisseur :
          </Modal.Header>
          <Modal.Content >
            <Form onSubmit={e=>{e.preventDefault();this.addFournisseur()}}>
              <Form.Input size="big" label='Nom' placeholder='Nom' name="newName" onChange={this.handleChange}/>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.addFournisseur()}} content='Ajouter' icon='plus' labelPosition='right'/>
          </Modal.Actions>
        </Modal>
      </Fragment>
    )
  }
}

const withUserContext = WrappedComponent => props => (
<UserContext.Consumer>
    {ctx => <WrappedComponent {...ctx} {...props}/>}
</UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(Fournisseurs));
