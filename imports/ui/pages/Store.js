import React, { Component, Fragment } from 'react';
import { Input,Menu,Table,Icon,Loader,Dimmer,Button,Pagination,Modal,Form } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import SecteurPicker from '../atoms/SecteurPicker';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';
import StoreRow from '../molecules/StoreRow';
import _ from 'lodash';

export class Store extends Component {

    state={
        itemsRaw:[],
        storeFilter:"",
        objectSpecific:"",
        fromNoteSpecific:"",
        commentSpecific:"",
        expressConsos:[],
        loading:true,
        openSpecific:false,
        openExpress:false,
        express:"",
        currentPage:1,
        rowByPage:10,
        maxPage:1,
        usualConsosIds:this.props.usualConsosIds,
        usualConsomablesQuery : gql`
            query usualConsomables($ids:[String],$site:String!){
                usualConsomables(ids:$ids,site: $site){
                    _id
                    ref
                    refS
                    categorie
                    modele
                    designation
                    currentlyStored
                    avCS
                    packagingFormat
                    fournisseur{
                        _id
                        name
                        deliveryDelay
                    }
                }
            }
        `,
        removeFromUserUsualQuery : gql`
            mutation removeFromUserUsual($_id:String!,$userId:String!,$site:String!){
                removeFromUserUsual(_id:$_id,userId:$userId,site:$site)
            }
        `,
        takeFromStockQuery : gql`
            mutation takeFromStock($_id:String!,$quantity:Int!,$isCorrection:Boolean!,$note:String!,$site:String!){
                takeFromStock(_id:$_id,quantity:$quantity,isCorrection:$isCorrection,note:$note,site:$site) {
                    _id
                    currentlyStored
                }
            }
        `,
        createDemandesConsosQuery : gql`
            mutation createDemandesConsos($consosQuantities:String!,$from:String!,$site:String!){
                createDemandesConsos(consosQuantities:$consosQuantities,from:$from,site:$site)
            }
        `,
        createSpecificQuery : gql`
            mutation createSpecific($object:String!,$fromNote:String!,$comment:String!,$site:String!){
                createSpecific(object:$object,fromNote:$fromNote,comment:$comment,site:$site)
            }
        `,
        items : () => {
            if(this.state.itemsRaw.length==0){
                return(
                    <Table.Row key={"none"}>
                        <Table.Cell width={16} colSpan='5' textAlign="center">
                            Votre profil n'a aucun consommable en favoris.<br/>
                            Pour personnaliser votre interface d'expression de besoin, rendez vous dans l'onglet stock et ajouter les consommables que vous avez l'habitude d'utiliser.
                        </Table.Cell>
                    </Table.Row>
                )
            }
            let displayed = Array.from(this.state.itemsRaw);
            if(this.state.storeFilter.length>1){
                displayed = displayed.filter(i => i.designation.toLowerCase().includes(this.state.storeFilter.toLowerCase())||i.ref.toLowerCase().includes(this.state.storeFilter.toLowerCase())||i.refS.toLowerCase().includes(this.state.storeFilter.toLowerCase())||i.categorie.toLowerCase().includes(this.state.storeFilter.toLowerCase())||i.modele.toLowerCase().includes(this.state.storeFilter.toLowerCase()));
                if(displayed.length == 0){
                  return(
                    <Table.Row key={"none"}>
                      <Table.Cell width={16} colSpan='5' textAlign="center">Aucun consommable répondant à ce filtre</Table.Cell>
                    </Table.Row>
                  )
                }
            }
            displayed = displayed.slice((this.state.currentPage - 1) * this.state.rowByPage, this.state.currentPage * this.state.rowByPage);
            return displayed.map(i =>(
                <StoreRow removeFromUserUsual={this.removeFromUserUsual} takeFromStock={this.takeFromStock} askForCommand={this.askForCommand} key={i._id} item={i}/>
            ))
        }
    }

    removeFromUserUsual = _id => {
        this.props.client.mutate({
            mutation : this.state.removeFromUserUsualQuery,
            variables:{
                userId:Meteor.userId(),
                site:this.props.site,
                _id:_id
            }
        }).then(({data})=>{
            this.props.updateUsualConsosId();
            this.setState({usualConsosIds:data.removeFromUserUsual})
            this.loadStore();
        })
    }

    loadStore = () => {
        let expressConsos = [];
        const ids = this.state.usualConsosIds
        this.props.client.query({
            query : this.state.usualConsomablesQuery,
            variables:{
                site:this.props.site,
                ids:ids
            },
            fetchPolicy:"network-only"
        }).then(({data})=>{
            data.usualConsomables.map(c=>{
                expressConsos.push({_id:c._id,expressQuantity:0})
                if(c.currentlyStored <= (c.fournisseur.deliveryDelay*c.avCS)){
                    c.emergencyCode = 0;
                }
                if(((c.currentlyStored <= ((c.fournisseur.deliveryDelay+7)*c.avCS)) && (c.currentlyStored > (c.fournisseur.deliveryDelay*c.avCS)))){
                    c.emergencyCode = 1;
                }
                if((c.currentlyStored > ((c.fournisseur.deliveryDelay+7)*c.avCS))){
                    c.emergencyCode = 2;
                }
                if(c.currentlyStored/c.avCS > 1){
                  c.left = "environ " + Math.floor(c.currentlyStored/c.avCS).toString() + " j.";
                }else{
                  c.left = "moins d'un jour";
                }
              })
            this.setState({
                expressConsos:expressConsos,
                itemsRaw:data.usualConsomables,
                maxPage:Math.ceil(data.usualConsomables.length / this.state.rowByPage),
                loading:false
            });
        })
    }

    handleSecteurChange = (key,secteur) => {
        this.setState({
            [key]:secteur
        })
    }

    handlePaginationChange = (e, { activePage }) => {
        this.setState({ currentPage:activePage })
    }

    handleExpressQuantityChange = (n,_id) => {
        let expressConsos = [];
        this.state.expressConsos.map(cons=>{
            if(cons._id == _id){
                expressConsos.push({_id:cons._id,expressQuantity:parseInt(n)})
            }else{
                expressConsos.push({_id:cons._id,expressQuantity:cons.expressQuantity})    
            }
        });
        this.setState({
            expressConsos:expressConsos
        })
    }

    handleChange = e =>{
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    handleFilter = _.debounce((e)=>{
        if(e.length == 0){
            this.setState({
                storeFilter : ""
            })
        }
        if(e.length>1){
            this.setState({
                storeFilter : e
            })
        }
    },400);

    componentDidMount = () => {
        this.loadStore();
    }

    createSpecific = () => {
        this.props.client.mutate({
            mutation : this.state.createSpecificQuery,
            variables:{
              object:this.state.objectSpecific,
              fromNote:this.state.fromNoteSpecific,
              comment:this.state.commentSpecific,
              site:this.props.site
            }
        });
        this.close();
    }

    takeFromStock = (_id,quantity,isCorrection,note) => {
        this.props.client.mutate({
          mutation : this.state.takeFromStockQuery,
          variables:{
            _id:_id,
            quantity:parseInt(quantity),
            isCorrection:isCorrection,
            note:note,
            site:this.props.site
          }
        }).then(()=>{
            this.loadStore();
        });
    }

    showSpecific = () => {
        this.setState({
            openSpecific:true
        })
    }
    
    closeSpecific = () => {
        this.setState({
            openSpecific:false
        })
    }

    showExpress = () => {
        this.setState({
            openExpress:true
        })
    }
    
    closeExpress = () => {
        this.setState({
            openExpress:false
        })
    }

    express = () => {
        const expressConsos = this.state.expressConsos.filter(x=>x.expressQuantity > 0)
        this.props.client.mutate({
            mutation : this.state.createDemandesConsosQuery,
            variables:{
              consosQuantities:JSON.stringify(expressConsos),
              from:this.state.express,
              site:this.props.site
            }
        }).then(({data})=>{
            this.closeExpress();
        })
    }

    render() {
        return (
            <Fragment>
                <div style={{height:"100%",padding:"8px",display:"grid",gridGap:"32px",gridTemplateRows:"auto 1fr auto",gridTemplateColumns:"auto 3fr 1fr 1fr"}}>
                    <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
                        <Menu.Item color="blue" name='Magasin' active onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
                        <Menu.Item color="blue" name='Stocks' onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
                        <Menu.Item color="blue" name='Demandes' onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
                        <Menu.Item color="blue" name='Commandes' onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
                        <Menu.Item color="blue" name='Fournisseurs' onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase' />Fournisseurs</Menu.Item>
                        <Menu.Item color="blue" name='Historique' onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
                    </Menu>
                    <Input style={{justifySelf:"stretch"}} name="storeFilter" onChange={e=>{this.handleFilter(e.target.value)}} icon='search' placeholder='Rechercher un item ... (3 caractères minimum)' />
                    <Button color="blue" onClick={this.showExpress} content='Demande de commande' icon='talk' labelPosition='right'/>
                    <Button color="green" onClick={this.showSpecific} content='Besoin spécifique' icon='talk' labelPosition='right'/>
                    <div style={{gridRowStart:"2",gridColumnEnd:"span 4",display:"block",overflowY:"auto",justifySelf:"stretch"}}>
                        <Table style={{marginBottom:"0"}} celled selectable color="blue" compact>
                            <Table.Header>
                                <Table.Row textAlign='center'>
                                    <Table.HeaderCell width={2}>Stock</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Réference</Table.HeaderCell>
                                    <Table.HeaderCell width={8}>Désignation</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Conditionement</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Actions</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {this.state.items()}
                            </Table.Body>
                        </Table>
                        <Dimmer inverted active={this.state.loading}>
                            <Loader size='massive'>Chargement du magasin ...</Loader>
                        </Dimmer>
                    </div>
                    <Pagination style={{placeSelf:"center",gridColumnEnd:"span 4"}} onPageChange={this.handlePaginationChange} defaultActivePage={this.state.currentPage} firstItem={null} lastItem={null} pointing secondary totalPages={this.state.maxPage}/>
                </div>
                <Modal dimmer="inverted" size="large" open={this.state.openSpecific} onClose={this.closeSpecific}>
                    <Modal.Header>
                        Expimer un besoin non listé dans le catalogue :
                    </Modal.Header>
                    <Modal.Content>
                        <Form style={{display:"grid",gridTemplateRows:"auto auto auto",gridGap:"16px"}} onSubmit={e=>{e.preventDefault();this.createSpecific()}}>
                            <Form.Input size="big" label='Objet de la demdande' placeholder='Objet ...' name="objectSpecific" onChange={this.handleChange}/>
                            <Form.TextArea label='Provenance' placeholder="Provenance ..." name="fromNoteSpecific" onChange={this.handleChange} rows={6}/>
                            <Form.TextArea label='Commentaires' placeholder="Commentaires ..." name="commentSpecific" onChange={this.handleChange} rows={6}/>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="green" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.createSpecific()}} content='Ajouter' icon='plus' labelPosition='right'/>
                    </Modal.Actions>
                </Modal>
                <Modal dimmer="inverted" size="large" open={this.state.openExpress} onClose={this.closeExpress}>
                    <Modal.Header>
                        Expimer un besoin de commande pour vos consommables ajoutés en favoris :
                    </Modal.Header>
                    <Modal.Content>
                        <Form onSubmit={e=>{e.preventDefault();}}>
                            <Table color="blue" celled selectable compact>
                                <Table.Header>
                                    <Table.Row textAlign='center'>
                                        <Table.HeaderCell width={12}>
                                            Consomable
                                        </Table.HeaderCell>
                                        <Table.HeaderCell width={4}>
                                            Quantité demandé
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>
                                    {this.state.itemsRaw.map(i=>{
                                        return(
                                            <Table.Row key={"express"+i._id}>
                                                <Table.Cell style={{marginLeft:"32px"}}>
                                                    <p style={{placeSelf:"center",marginBottom:"0",fontSize:"1.2em"}}>{i.categorie + " " + i.modele + " " + i.designation}</p>
                                                    <p style={{color:"#95a5a6"}}>({i.packagingFormat})</p>
                                                </Table.Cell>
                                                <Table.Cell textAlign="center">
                                                    <Input style={{gridColumnEnd:"span 2"}} type="number" style={{gridColumnEnd:"span 2"}} defaultValue="0" onChange={e=>{this.handleExpressQuantityChange(e.target.value,i._id)}} placeholder={"Quantité"} size="huge"/>
                                                </Table.Cell>
                                            </Table.Row>
                                        )
                                    })}
                                </Table.Body>
                            </Table>
                            <div style={{display:"flex",justifyContent:"center",fontSize:"1.2em",fontWeight:"bold"}}>
                                Demande en provenance de {' '}
                                <SecteurPicker onChange={this.handleSecteurChange} target={"express"}/>
                            </div>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button disabled={this.state.express == ""} color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.express()}} content='Passer la demande' icon='right arrow' labelPosition='right'/>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(Store));