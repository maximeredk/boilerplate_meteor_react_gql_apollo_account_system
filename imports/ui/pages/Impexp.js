import React, { Component, Fragment } from 'react';
import { Menu, Button, Segment, Header, Icon, Form, Progress, Message, Modal, List } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import 'moment/locale/fr';
import { forEach } from 'async';

export class Impexp extends Component {

  state = {
    rolesUsers:[],
    accounts:[],
    file:{name:"Aucun fichier choisi",size:0},
    insertDone:0,
    colDone:0,
    insertTotal:0,
    colTotal:0,
    taskLabel:"",
    openImport:false,
    fournisseurs:[],
    deletions:[false,false,false,false,false,false,false,false,false,false,false],
    collections:[
      "users",
      "roles",
      "commandes",
      "consomables",
      "fournisseurs",
      "commandeConsos",
      "deliveries",
      "deliveryConsos",
      "demandeConsos",
      "historiqueConsos",
      "specifics"
    ],
    cleanQuery:gql`
      mutation clean($collection:String!){
        clean(collection:$collection)
      }
    `,
    writeEntryQuery:gql`
      mutation writeEntry($collection:String!,$payload:String!){
        writeEntry(collection:$collection,payload:$payload)
      }
    `,
    exportFullDBQuery : gql`
      query ExportFullDB{
        exportFullDB{
          users
          roles
          consomables
          fournisseurs
          demandeConsos
          specifics
          commandes
          commandeConsos
          deliveries
          deliveryConsos
          historiqueConsos
        }
      }
    `
  }

  fileInputRef = React.createRef();

  closeImport = () => {
    this.setState({
      openImport:false
    })
  }

  showImport = () => {
    this.setState({
      openImport:true
    })
  }

  fileChange = e => {
    let f = e.target.files[0];
    if (e.target.files.length <= 0) {
      return false;
    }
    var fr = new FileReader();
    fr.onload = e => { 
      var result = JSON.parse(e.target.result);
      this.setState({
        db : result,
        colTotal : Object.keys(result).length
      })
    }
    fr.readAsText(f)
    this.setState({file:f})
  };

  clean = (index) => {
    this.props.client.mutate({
      mutation : this.state.cleanQuery,
      variables:{
        collection:this.state.collections[index]
      }
    }).then((data)=>{
      let d = this.state.deletions;
      d[index]=true;
      this.setState({
        deletions:d
      });
      if(index+1 != this.state.collections.length){
        setTimeout(()=>{this.clean(index+1)},100);
      }else{
        setTimeout(()=>{
            this.write(0,0);
            this.setState({
              insertDone:0,
              insertTotal:this.state.db[this.state.collections[0]].length,
              taskLabel:this.state.collections[0]
            });
        },100);
      }
    });
  }

  write = (colI,entryI) => {
    if(colI < this.state.collections.length){
      if(entryI < this.state.db[this.state.collections[colI]].length){
        this.props.client.mutate({
          mutation : this.state.writeEntryQuery,
          variables:{
            collection:this.state.collections[colI],
            payload:JSON.stringify(this.state.db[this.state.collections[colI]][entryI])
          }
        }).then((data)=>{
          this.setState({
            insertDone:entryI+1
          })
          setTimeout(()=>{
            this.write(colI,entryI+1);
          },20);
        });
      }else{
        let newColI = colI+1;
        while(this.state.db[this.state.collections[newColI]].length == 0){
          if(newColI+1 == this.state.collections.length){
            break;
          }else{
            newColI++;
          }
        }
        if(newColI<this.state.colTotal){
            this.setState({
              insertDone:0,
              insertTotal:this.state.db[this.state.collections[newColI]].length,
              colDone:newColI+1,
              taskLabel:this.state.collections[newColI]
            });
            setTimeout(()=>{
              this.write(newColI,0);
            },1000);
        }else{
          this.setState({
            insertDone:1,
            insertTotal:1,
            colDone:newColI+1,
            taskLabel:" terminée"
          });
        }
      }
    }
  }

  importDB = () => {
    this.closeImport();
    this.clean(0);
  }

  downloadDB = () => {
    this.props.client.query({
      query : this.state.exportFullDBQuery,
      fetchPolicy:"network-only"
    }).then(({data})=>{
      const exp = {
        users:data.exportFullDB.users.map(x=>JSON.parse(x)),
        roles:data.exportFullDB.roles.map(x=>JSON.parse(x)),
        consomables:data.exportFullDB.consomables.map(x=>JSON.parse(x)),
        fournisseurs:data.exportFullDB.fournisseurs.map(x=>JSON.parse(x)),
        demandeConsos:data.exportFullDB.demandeConsos.map(x=>JSON.parse(x)),
        specifics:data.exportFullDB.specifics.map(x=>JSON.parse(x)),
        commandes:data.exportFullDB.commandes.map(x=>JSON.parse(x)),
        commandeConsos:data.exportFullDB.commandeConsos.map(x=>JSON.parse(x)),
        deliveries:data.exportFullDB.deliveries.map(x=>JSON.parse(x)),
        deliveryConsos:data.exportFullDB.deliveryConsos.map(x=>JSON.parse(x)),
        historiqueConsos:data.exportFullDB.historiqueConsos.map(x=>JSON.parse(x)),
      }
      saveAs(new Blob([JSON.stringify(exp)], {type: "application/json"}), "dbClone_"+(new Date().getFullYear()).toString() +"_"+ (new Date().getMonth()+1).toString().padStart(2,'0') +"_"+ new Date().getDate().toString().padStart(2,'0') +"_"+ new Date().getHours().toString().padStart(2,'0')+"_"+new Date().getMinutes().toString().padStart(2,'0')+"_"+new Date().getSeconds().toString().padStart(2,'0') + ".json");
    });
  }

  getMenu = () => {
    if(this.props.user.isOwner){
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' onClick={()=>{this.props.history.push("/administration/roles")}} />
          <Menu.Item color="red" name='Réinitialisation' onClick={()=>{this.props.history.push("/administration/init")}} />
          <Menu.Item color="green" name='Import/Export' active onClick={()=>{this.props.history.push("/administration/impexp")}} />
        </Menu>
      )
    }else{
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' onClick={()=>{this.props.history.push("/administration/roles")}} />
        </Menu>
      )
    }
  }

  render() {
    return (
      <Fragment>
        <div style={{display:"grid",gridGap:"16px",gridTemplateColumns:"auto 1fr"}}>
          {this.getMenu()}
          <Segment raised style={{gridColumnStart:"2",gridRowStart:"1",display:"grid",gridGap:"16px",padding:"32px",marginTop:"0",justifySelf:"stretch",gridTemplateColumns:"1fr 3fr"}}>
            <div style={{display:"grid",borderRight:"6px solid #74b9ff"}}>
              <Header style={{placeSelf:"center"}} as='h3' icon>
                <Icon circular name='upload'/>
                Exporter un fichier clone de la base
              </Header>
            </div>
            <Button onClick={this.downloadDB} color="green">Cloner</Button>
          </Segment>
          <Segment raised style={{gridColumnStart:"2",gridRowStart:"2",display:"grid",gridGap:"16px",padding:"32px",marginTop:"0",justifySelf:"stretch",gridTemplateColumns:"1fr 3fr"}}>
            <div style={{display:"grid",borderRight:"6px solid #74b9ff"}}>
              <Header style={{placeSelf:"center"}} as='h3' icon>
                <Icon circular name='download'/>
                Initialiser la plateforme à partir d'un fichier clone
              </Header>
            </div>
            <Form onSubmit={this.onFormSubmit} style={{display:"grid",gridGap:"8px 24px",gridTemplateColumns:"1fr 1fr 1fr",margin:"16px 48px 0 48px"}}>
              <div style={{margin:"0 0 16px 0",placeSelf:"stretch"}}>
                <Form.Field style={{width:"100%",height:"100%"}}>
                  <Button style={{width:"100%",height:"100%"}} content="choisir un fichier JSON" labelPosition="left" icon="file" onClick={() => this.fileInputRef.current.click()} />
                  <input ref={this.fileInputRef} type="file" hidden onChange={this.fileChange} />
                </Form.Field>
              </div>
              <Message style={{margin:"0 0 16px 0"}}>
                <div style={{display:"flex",justifyContent:"space-between"}}><span>fichier : </span><span>{this.state.file.name}</span></div>
                <hr/>
                <div style={{display:"flex",justifyContent:"space-between"}}><span>taille : </span><span>{(this.state.file.size/1024).toFixed(2)} Kb</span></div>
              </Message>
              <Button style={{placeSelf:"stretch",marginBottom:"16px"}} disabled={(this.state.file.size == 0 ? true : false)} type="submit" onClick={this.showImport} color="red" content={"Initialiser la plateforme"}/>
              <div style={{placeSelf:"center end"}}>Nettoyage de la base</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"2",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='value' value={this.state.deletions.filter(x=>x).length} total={this.state.deletions.length} label={this.state.deletions.filter(x=>x).length+" / "+this.state.deletions.length} indicating />
              <div style={{placeSelf:"center end"}}>Insertion du contenu</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"3",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='value' value={this.state.colDone} total={this.state.colTotal} label={this.state.colDone+" / "+this.state.colTotal} indicating />
              <div style={{placeSelf:"center end"}}>Insertion : {this.state.taskLabel}</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"4",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='value' value={(isNaN(parseFloat((this.state.insertDone/this.state.insertTotal)*100)) ? 0 : parseFloat((this.state.insertDone/this.state.insertTotal)*100).toFixed(1))} total={100} label={this.state.insertDone+" / "+this.state.insertTotal} indicating />
            </Form>
          </Segment>
        </div>
        <Modal closeOnDimmerClick={false} dimmer="inverted" open={this.state.openImport} onClose={this.closeImport} closeIcon>
          <Modal.Header>
              Avertissement !
          </Modal.Header>
          <Modal.Content >
            <Message color="red">
              Poursuivre cette actions entrainera la réinitialisation totale de la plateforme, cela inclut tous les sites<br/>
              <hr/>
              Seront définitivement supprimés :
              <List bulleted as='ul'>
                <List.Item as='li'>Les catalogue de consomable </List.Item>
                <List.Item as='li'>Les fournisseurs de ces consomables</List.Item>
                <List.Item as='li'>
                  Les commandes, passées et actuelles
                  <List.List as='ul'>
                    <List.Item as='li'>Le contenu de ces commandes</List.Item>
                    <List.Item as='li'>Les livraisons de ces commandes</List.Item>
                  </List.List>
                </List.Item>
                <List.Item as='li'>
                  Les demandes de commande
                  <List.List as='ul'>
                    <List.Item as='li'>Leur quantité et provenance</List.Item>
                    <List.Item as='li'>Y compris les demandes hors-catalogues</List.Item>
                  </List.List>
                </List.Item>
                <List.Item as='li'>L'historique entier de tous les sites</List.Item>
                <List.Item as='li'>Tous les rôles affectés aux utilisateurs</List.Item>
                <List.Item as='li'>Les utilisateurs eux-mêmes : seul le compte propriétaire de l'application ne sera pas remplacé</List.Item>
              </List>
              La base de données de l'application sera recréée à partir du contenu du fichier fourni.
              <hr/>
            </Message>
          </Modal.Content>
          <Modal.Actions>
            <Button style={{fontSize:"1.4em"}} onClick={this.importDB} color="red" animated='fade'>
              <Button.Content visible>Confirmer</Button.Content>
              <Button.Content hidden>
                  <Icon name='trash'/>
              </Button.Content>
            </Button>
          </Modal.Actions>
      </Modal>
    </Fragment>
    )
  }
}
const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Impexp);