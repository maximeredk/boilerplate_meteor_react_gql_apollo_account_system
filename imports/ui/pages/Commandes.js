import React, { Component, Fragment } from 'react';
import { Input,Menu,Table,Segment,Icon } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';
import moment from 'moment';
import 'moment/locale/fr';

import { UserContext } from '../../contexts/UserContext';
import CommandeRow from '../molecules/CommandeRow';

export class Stock extends Component {

  state={
    m:new Date().getMonth(),
    y:new Date().getFullYear(),
    panelSize:0,
    selected:null,
    commandesRaw:[],
    orderByLate:false,
    displayArchived:false,
    commandesFilter:"",
    selectedDay:new Date().getDate(),
    validationDSL_main:"",
    commandAdmin_main:"",
    validationDSL_backup:"",
    commandAdmin_backup:"",
    commandesQuery : gql`
      query Commandes($site: String!){
        commandes(site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    removeCommandeQuery : gql`
      mutation removeCommande($_id: String!,$site: String!){
        removeCommande(_id:$_id,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    removeDeliveryQuery : gql`
      mutation removeDelivery($_id: String!,$site: String!){
        removeDelivery(_id:$_id,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    recepCommandeQuery : gql`
      mutation recepCommande($_id: String!,$quantities: String!,$note: String!,$site: String!){
        recepCommande(_id:$_id,quantities:$quantities,note:$note,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    shippingCommandeQuery : gql`
      mutation shippingCommande($_id: String!,$site: String!){
        shippingCommande(_id:$_id,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    validationDSLQuery : gql`
      mutation validationDSL($_id: String!,$site: String!){
        validationDSL(_id:$_id,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    archiveCommandeQuery : gql`
      mutation archiveCommande($_id: String!,$site: String!){
        archiveCommande(_id:$_id,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    editCommandeConsoQuery : gql`
      mutation editCommandeConso($_id: String!,$commande:String!,$quantity: Int!,$site: String!){
        editCommandeConso(_id:$_id,commande:$commande,quantity:$quantity,site: $site){
          _id
          validatedDSL
          archived
          status
          internalRef
          orderDay
          orderMonth
          orderYear
          creationDay
          creationMonth
          creationYear
          fournisseur{
            _id
            name
            deliveryDelay
          }
          commandeConsos
          {
            _id
            nbPackages
            received
            consomable{
              _id
              ref
              refS
              designation
              categorie
              modele
              currentlyStored
              prix
              avCS
              packagingFormat
            }
            fromNote
          }
          deliveries{
            _id
            commande
            bl
            deliveryDay
            deliveryMonth
            deliveryYear
            deliveryConsos{
              _id
              delivery
              quantite
              consomable{
                _id
                ref
                refS
                designation
                categorie
                modele
                currentlyStored
                prix
                avCS
                packagingFormat
              }
            }
          }
        }
      }
    `,
    getUserForRoleQuery : gql`
      query getUserForRole($site: String!,$role: String!,$main: Boolean!) {
          getUserForRole(site:$site,role:$role,main:$main) {
              _id
              email
              firstname
              lastname
          }
      }
    `,
    commandes: () =>{
      let displayed = Array.from(this.state.commandesRaw);
      if(this.state.commandesRaw.length == 0){
        return(
          <Table.Row key={"none"}>
              <Table.Cell width={16} colSpan='7' textAlign="center">Aucune commande en cours</Table.Cell>
          </Table.Row>
        )
      }
      //filtrage:archivée ou non
      displayed = displayed.filter(c=>c.archived == this.state.displayArchived)
      //filtrage:par status
      if(this.state.statusFilter >=0){
        displayed = displayed.filter(c=>c.status-1 == this.state.statusFilter)
      }
      //calcul du retard
      displayed.forEach(c=>{
        if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isSame(moment(), 'day')){
          c.late = 0;
        }
        if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isAfter(moment(), 'day')){
          c.late = moment.duration(moment([c.orderYear,c.orderMonth,c.orderDay]).diff(moment())).asDays()
        }
        if(moment([c.orderYear,c.orderMonth,c.orderDay]).add(c.fournisseur.deliveryDelay,'d').isBefore(moment(), 'day')){
          c.late = - moment.duration(moment([c.orderYear,c.orderMonth,c.orderDay]).diff(moment())).asDays()
        }
      })
      //sorting by retard
      if(this.state.orderByLate){
        displayed.sort((a,b)=>{
          if ( a.late < b.late ){return 1;}
          if ( a.late > b.late ){return -1;}
          return 0;
        })
      }
      //filtrage:filtre textuel simple
      displayed = displayed.filter(h =>
        h.fournisseur.name.toLowerCase().includes(this.state.commandesFilter.toLowerCase()) ||
        h.internalRef.toLowerCase().includes(this.state.commandesFilter.toLowerCase()) ||
        //filtrage:filtre textuel profond
        this.deepTextualSearch(h)
      );
      if(displayed.length == 0){
        return(
            <Table.Row key={"none"}>
                <Table.Cell width={16} colSpan='7' textAlign="center">Aucune commande ne correspond aux filtres</Table.Cell>
            </Table.Row>
        )
      }else{
        return displayed.map(c =>(
            <CommandeRow
              key={c._id}
              commande={c}
              removeDelivery={this.removeDelivery}
              loadRoles={this.loadRoles}
              validationDSL_main={this.state.validationDSL_main}
              validationDSL_backup={this.state.validationDSL_backup}
              commandAdmin_main={this.state.commandAdmin_main}
              commandAdmin_backup={this.state.commandAdmin_backup}
              recepCommande={this.recepCommande}
              shippingCommande={this.shippingCommande}
              removeCommande={this.removeCommande}
              archiveCommande={this.archiveCommande}
              validateDSL={this.validateDSL}
              editCommandeConso={this.editCommandeConso}
            />
        ))
      }
    },
    statusFilter:-1,
    statusColors:["red","orange","yellow","green","blue"]
  }

  deepTextualSearch = h =>{
    let match = false;
    h.commandeConsos.map(cC=>{
      if(cC.consomable.designation.toLowerCase().includes(this.state.commandesFilter.toLowerCase())){
        match = true;
      }
      if(cC.consomable.categorie.toLowerCase().includes(this.state.commandesFilter.toLowerCase())){
        match = true;
      }
      if(cC.consomable.modele.toLowerCase().includes(this.state.commandesFilter.toLowerCase())){
        match = true;
      }
      JSON.parse(cC.fromNote).map(fn=>{
        if(fn.from.toLowerCase().includes(this.state.commandesFilter.toLowerCase())){
          match = true;
        }
      })
    });
    h.deliveries.map(d=>{
      if(d.bl.toLowerCase().includes(this.state.commandesFilter.toLowerCase())){
        match = true;
      }
    });
    return match;
  }

  componentWillMount = () => {
    moment.locale('fr');
  }

  handleFilter= e => {
    this.setState({
      commandesFilter : e.target.value
    })
  }

  loadCommandes = andRoles => {
    this.props.client.query({
      query : this.state.commandesQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        commandesRaw:data.commandes
      });
      if(andRoles){
        this.loadRoles();
      }
    })
  }

  componentDidMount = () => {
    this.loadCommandes(true);
  }
  
  loadRoles = () => {
    this.getUserForRole("validationDSL",true);
    this.getUserForRole("commandAdmin",true);
    this.getUserForRole("validationDSL",false);
    this.getUserForRole("commandAdmin",false);
  }

  removeCommande = _id => {
    this.props.client.mutate({
      mutation : this.state.removeCommandeQuery,
      variables:{
          _id:_id,
          site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.removeCommande
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  archiveCommande = _id => {
    this.props.client.mutate({
      mutation : this.state.archiveCommandeQuery,
      variables:{
          _id:_id,
          site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.archiveCommande
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  shippingCommande = _id => {
    this.props.client.mutate({
      mutation : this.state.shippingCommandeQuery,
      variables:{
          _id:_id,
          site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.shippingCommande
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  validateDSL = _id => {
    this.props.client.mutate({
      mutation : this.state.validationDSLQuery,
      variables:{
        _id:_id,
        site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.validationDSL
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  recepCommande = (_id,quantities,bl) => {
    this.props.client.mutate({
      mutation : this.state.recepCommandeQuery,
      variables:{
        _id:_id,
        quantities:JSON.stringify(quantities),
        note:bl,
        site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.recepCommande
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  removeDelivery = _id => {
    this.props.client.mutate({
      mutation : this.state.removeDeliveryQuery,
      variables:{
        _id:_id,
        site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.removeDelivery
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  editCommandeConso = (_id,commande,newCommandeConsoQuantity) => {
    this.props.client.mutate({
      mutation : this.state.editCommandeConsoQuery,
      variables:{
        _id:_id,
        commande:commande,
        quantity:parseInt(newCommandeConsoQuantity),
        site:this.props.site
      }
    }).then((data)=>{
      const cmds = data.data.editCommandeConso
      this.setState({
        commandesRaw:cmds
      });
    })
  }

  getUserForRole = (role,main) => {
    this.props.client.query({
        query: this.state.getUserForRoleQuery,
        name:"getUserForRole",
        fetchPolicy:"network-only",
        variables:{role:role,site:this.props.site,main:main}
    }).then(({data}) => {
      let sufixRole="";
      (main?sufixRole="_main":sufixRole="_backup")
      this.setState({
        [role+sufixRole]:data.getUserForRole
      })
    })
  }

  setStatusFilter = n => {
    if(n == this.state.statusFilter){
      this.setState({
        statusFilter : -1,
        statusColor : null
      })
    }else{
      this.setState({
        statusFilter : n,
        statusColor : this.state.statusColors[n]
      })
    }
  }

  getStatusColor = n => {
    if(n <= this.state.statusFilter){
      return this.state.statusColors[this.state.statusFilter]
    }
  }

  render() {
    return (
      <div className="commandeHeaderWrapper">
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
          <Menu.Item color="blue" name='Magasin' onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
          <Menu.Item color="blue" name='Stocks' onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
          <Menu.Item color="blue" name='Demandes' onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
          <Menu.Item color="blue" name='Commandes' active onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
          <Menu.Item color="blue" name='Fournisseurs' onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase' />Fournisseurs</Menu.Item>
          <Menu.Item color="blue" name='Historique' onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
        </Menu>
        <Input style={{justifySelf:"stretch",height:"73px"}} name="commandesFilter" onChange={this.handleFilter} icon='search' placeholder='Un fournisseur, un consommable, une référence, un secteur ou une note de livraison' />
        <div style={{gridRowStart:"2",gridColumnEnd:"span 2",justifySelf:"stretch",display:"flex",justifyContent:"space-evenly"}}>
          <div style={{display:"flex",gridColumnEnd:"span 2",alignItems:"center",placeSelf:"center"}}>
            <p style={{alignItems:"strech",marginBottom:"0",marginRight:"16px"}} >Filtrer par status : </p>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setStatusFilter(0)}} color={this.getStatusColor(0)} name='certificate'/>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setStatusFilter(1)}} color={this.getStatusColor(1)} name='certificate'/>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setStatusFilter(2)}} color={this.getStatusColor(2)} name='certificate'/>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setStatusFilter(3)}} color={this.getStatusColor(3)} name='certificate'/>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setStatusFilter(4)}} color={this.getStatusColor(4)} name='certificate'/>
          </div>
          <div style={{display:"flex",alignItems:"center",placeSelf:"center"}}>
            <p style={{alignItems:"strech",marginBottom:"0",margin:"0 16px 0 32px"}} >Affichage des archives : </p>
            <Icon className="pointerStretch" size="big" onClick={()=>{this.setState({displayArchived:!this.state.displayArchived})}} name={(this.state.displayArchived?"archive":"shipping")} color={(this.state.displayArchived?"orange":"green")}/>
          </div>
          <div style={{display:"flex",alignItems:"center",placeSelf:"center"}}>
            <p style={{alignItems:"strech",marginBottom:"0",margin:"0 16px 0 32px"}} >Trier par retard : </p>
            <Icon className="pointerStretch" loading={this.state.orderByLate} size="big" onClick={()=>{this.setState({orderByLate:!this.state.orderByLate})}} name='time'/>
          </div>
        </div>
        <Segment style={{gridRowStart:"1",gridColumnStart:"3",gridRowEnd:"span 2",justifySelf:"end",margin:"0",height:"172px"}}>
          <div style={{display:"grid",gridTemplateRows:"24px 8px 24px 24px 24px 24px 24px",gridTemplateColumns:"225px 20px 20px 20px 20px 20px"}}>
            <p style={{placeSelf:"center",margin:"2px auto",gridColumnEnd:"span 6",fontWeight:"700"}}>Légende</p>
            <hr style={{margin:"2px auto",borderWidth:"2px",width:"80%",gridColumnEnd:"span 6"}}/>
            <p style={{margin:"0",paddingRight:"16px",textAlign:"right"}}>Demande émise</p>
            <Icon name="certificate" color="red"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <p style={{margin:"0",paddingRight:"16px",textAlign:"right"}}> Validé par le DSL</p>
            <Icon name="certificate" color="orange"/>
            <Icon name="certificate" color="orange"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <p style={{margin:"0",paddingRight:"16px",textAlign:"right"}}>Commande passée</p>
            <Icon name="certificate" color="yellow"/>
            <Icon name="certificate" color="yellow"/>
            <Icon name="certificate" color="yellow"/>
            <Icon name="certificate"/>
            <Icon name="certificate"/>
            <p style={{margin:"0",paddingRight:"16px",textAlign:"right"}}>Partiellement réceptionnée</p>
            <Icon name="certificate" color="green"/>
            <Icon name="certificate" color="green"/>
            <Icon name="certificate" color="green"/>
            <Icon name="certificate" color="green"/>
            <Icon name="certificate" />
            <p style={{margin:"0",paddingRight:"16px",textAlign:"right"}}>Totalement réceptionnée</p>
            <Icon name="certificate" color="blue"/>
            <Icon name="certificate" color="blue"/>
            <Icon name="certificate" color="blue"/>
            <Icon name="certificate" color="blue"/>
            <Icon name="certificate" color="blue"/>
          </div>
        </Segment>
        <div style={{gridRowStart:"3",gridColumnEnd:"span 3",display:"block",overflowY:"auto",placeSelf:"stretch"}}>
          <Table style={{}} celled selectable color="blue" compact>
            <Table.Header>
              <Table.Row textAlign='center'>
                <Table.HeaderCell width={3}>Fournisseur</Table.HeaderCell>
                <Table.HeaderCell width={3}>Référence interne</Table.HeaderCell>
                <Table.HeaderCell width={2}>Valeur</Table.HeaderCell>
                <Table.HeaderCell width={2}>Crée le</Table.HeaderCell>
                <Table.HeaderCell width={2}>Retard</Table.HeaderCell>
                <Table.HeaderCell width={2}>Status</Table.HeaderCell>
                <Table.HeaderCell width={2}>Réception</Table.HeaderCell>
                <Table.HeaderCell width={2}>Details</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.state.commandes()}
            </Table.Body>
          </Table>
        </div>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(Stock));