import React, { Component,Fragment } from 'react'
import { Input,Menu,Icon,Table,Segment,Header } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import SpecificRow from '../molecules/SpecificRow';
import DemandeConsoRow from '../molecules/DemandeConsoRow';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

export class DemandesConso extends Component {

  state={
    demandesConsoFilter:"",
    demandesConsoRaw:[],
    specificsRaw:[],
    GDS_main:"",
    GDS_backup:"",
    demandesConsoQuery : gql`
      query DemandesConso($site: String!){
        demandesConso(site: $site){
          _id
          name
          deliveryDelay
          agglomeratedConsomable{
            _id
            ref
            refS
            categorie
            modele
            designation
            currentlyStored
            minOrder
            avCS
            packagingFormat
            prix
            fournisseur{
              _id
            }
            demandes{
              _id
              quantity
              user
              from
              entryDay
              entryMonth
              entryYear
              entryHour
              entryMinute
              entrySecond  
            }
          }
        }
      }
    `,
    specificsQuery : gql`
        query specifics($site: String!){
            specifics(site: $site){
                _id
                object
                fromNote
                comment
            }
        }
    `,
    removeDemandeConsoQuery : gql`
      mutation removeDemandeConso($consomable:String!,$site: String!){
        removeDemandeConso(consomable:$consomable,site: $site)
      }
    `,
    deleteDemandeQuery : gql`
      mutation deleteDemande($demande:String!,$site:String!){
        deleteDemande(demande:$demande,site:$site){
          _id
          name
          deliveryDelay
          agglomeratedConsomable{
            _id
            ref
            refS
            categorie
            modele
            designation
            currentlyStored
            minOrder
            avCS
            packagingFormat
            prix
            fournisseur{
              _id
            }
            demandes{
              _id
              quantity
              user
              from
              entryDay
              entryMonth
              entryYear
              entryHour
              entryMinute
              entrySecond  
            }
          }
        }
      }
    `,
    getUserForRoleQuery : gql`
      query getUserForRole($site: String!,$role: String!,$main: Boolean!) {
          getUserForRole(site:$site,role:$role,main:$main) {
              _id
              email
              firstname
              lastname
          }
      }
    `,
    specifics: () =>{
      if(this.state.specificsRaw.length != 0){
        return (
          <Table color="green" celled selectable compact>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell style={{textAlign:"center"}}>Objet de la demande</Table.HeaderCell>
                <Table.HeaderCell style={{textAlign:"center"}}>Provenence</Table.HeaderCell>
                <Table.HeaderCell style={{textAlign:"center"}}>Commentaire</Table.HeaderCell>
                <Table.HeaderCell style={{textAlign:"center"}}>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.state.specificsRaw.map(s=>
                <SpecificRow reloadSpecific={this.loadSpecifics} specific={s} key={s._id}/>
              )}
            </Table.Body>
          </Table>
        )
      }
    },
    demandesConso: () =>{
      if(this.state.demandesConsoRaw.length == 0){
        return(
          <Segment raised style={{marginBottom:"32px"}}>
            <Header as="h1">Aucune demande en cours</Header>
          </Segment>
        )
      }else{
        if(this.state.demandesConsoFilter.length != 0){
          const displayed = this.state.demandesConsoRaw.filter(f => f.name.toLowerCase().includes(this.state.demandesConsoFilter.toLowerCase()))
          if(displayed.length != 0){
            return (
              this.state.demandesConsoRaw.map(dC=>{
                return(
                  <DemandeConsoRow
                    deleteDemandeConso={this.deleteDemandeConso}
                    removeDemandByConso={this.removeDemandByConso}
                    setDemandesConsoRaw={this.setDemandesConsoRaw}
                    GDS_main={this.state.GDS_main}
                    GDS_backup={this.state.GDS_backup}
                    demandesConso={dC}
                    key={dC._id}
                  />
                )
              }) 
            );
          }else{
            return (
              <Segment raised style={{marginBottom:"32px"}}>
                <Header as="h1">Aucune demande ne correspond au filtre</Header>
              </Segment>
            );
          }
        }else{
          return (
            this.state.demandesConsoRaw.map(dC=>
              <DemandeConsoRow
                deleteDemandeConso={this.deleteDemandeConso}
                removeDemandByConso={this.removeDemandByConso}
                setDemandesConsoRaw={this.setDemandesConsoRaw}
                demandesConso={dC}
                key={dC._id}
                GDS_main={this.state.GDS_main}
                GDS_backup={this.state.GDS_backup}
              />
            )
          )
        }
      }
    }
  }

  loadRoles = () => {
    this.getUserForRole("GDS",true);
    this.getUserForRole("GDS",false);
  }

  getUserForRole = (role,main) => {
    this.props.client.query({
        query: this.state.getUserForRoleQuery,
        name:"getUserForRole",
        fetchPolicy:"network-only",
        variables:{role:role,site:this.props.site,main:main}
    }).then(({data}) => {
      let sufixRole="";
      (main?sufixRole="_main":sufixRole="_backup")
      this.setState({
        [role+sufixRole]:data.getUserForRole
      })
    })
  }

  deleteDemandeConso = demandeId => {
    this.props.client.mutate({
        mutation : this.state.deleteDemandeQuery,
        variables:{
            demande: demandeId,
            site:this.props.site
        }
    }).then(({data})=>{
        this.setDemandesConsoRaw(data.deleteDemande);
    });
  }

  handleFilter= e => {
    this.setState({
      demandesConsoFilter : e.target.value
    })
  }

  setDemandesConsoRaw = demandesConso => {
    this.setState({
      demandesConsoRaw : demandesConso
    })
  }

  removeDemandByConso = _id => {
    this.props.client.mutate({
      mutation : this.state.removeDemandeConsoQuery,
      variables:{
          consomable:_id
      }
    }).then(({data})=>{
      this.loadDemandesConso();
    })
  }

  loadDemandesConso = () => {
    this.props.client.query({
      query : this.state.demandesConsoQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        demandesConsoRaw:data.demandesConso
      });
    })
  }

  loadSpecifics = () => {
    this.props.client.query({
      query : this.state.specificsQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        specificsRaw:data.specifics
      });
    })
  }

  componentDidMount = () => {
    this.loadRoles();
    this.loadDemandesConso();
    this.loadSpecifics();
  }

  render() {
    return (
      <Fragment>
        <div style={{height:"100%",padding:"8px",display:"grid",gridGap:"32px",gridTemplateRows:"auto 80vh",gridTemplateColumns:"auto 1fr"}}>
          <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
            <Menu.Item color="blue" name='Magasin' onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
            <Menu.Item color="blue" name='Stocks' onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
            <Menu.Item color="blue" name='Demandes' active onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
            <Menu.Item color="blue" name='Commandes' onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
            <Menu.Item color="blue" name='Fournisseurs' onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase'/>Fournisseurs</Menu.Item>
            <Menu.Item color="blue" name='Historique' onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
          </Menu>
          <Input style={{justifySelf:"stretch"}} name="demandeConsoFilter" onChange={this.handleFilter} icon='search' placeholder="Rechercher les demandes d'un consomable ..." />
          <div style={{gridRowStart:"2",gridColumnEnd:"span 2",display:"block",overflowY:"auto",placeSelf:"stretch"}}>
            {this.state.specifics()}
            {this.state.demandesConso()}
          </div>
        </div>
      </Fragment>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(DemandesConso));