import React, { Component, Fragment } from 'react';
import { Segment,Modal,Table,Button,Input,Menu,Icon,Header,Dropdown } from 'semantic-ui-react';
import HistoriqueRow from '../molecules/HistoriqueRow';
import { UserContext } from '../../contexts/UserContext';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

export class HistoriqueConso extends Component {

  state={
    historiqueConsoRaw:[],
    activeItem:"home",
    months:["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"],
    openExport: false,
    month:null,
    year:null,
    historiqueConsoFilter:"",
    historiqueConsoQuery : gql`
      query HistoriqueConso($site: String!){
        historiqueConso(site:$site){
          _id
          entryDay
          entryMonth
          entryYear
          entryHour
          entryMinute
          entrySecond
          quantity
          type
          note
          consomable{
            ref
            categorie
            designation
            modele
            currentlyStored
            avCS
            packagingFormat
            fournisseur{
              _id
              name
            }
          }
        }
      }
    `,
    exportHistorique : gql`
      query exportHistorique($month:Int!,$year:Int!,$site: String!){
        exportHistorique(month:$month,year:$year,site:$site){
          _id
          entryDay
          entryMonth
          entryYear
          entryHour
          entryMinute
          entrySecond
          quantity
          type
          note
          consomable{
            ref
            categorie
            designation
            modele
            currentlyStored
            avCS
            packagingFormat
            fournisseur{
              _id
              name
            }
          }
        }
      }
    `,
    historiqueConso: () =>{
      if(this.state.historiqueConsoFilter === null){
        return(
            <Table.Row key={none}>
                <Table.Cell width={16} colSpan='5' textAlign="center">Aucune entrée ne correspond aux filtres</Table.Cell>
            </Table.Row>
        )
      }else{
        const filteredByExpression = this.state.historiqueConsoRaw.filter(h => h.consomable.categorie.toLowerCase().includes(this.state.historiqueConsoFilter.toLowerCase()) || h.consomable.modele.toLowerCase().includes(this.state.historiqueConsoFilter.toLowerCase()) || h.consomable.designation.toLowerCase().includes(this.state.historiqueConsoFilter.toLowerCase()) || h.consomable.ref.toLowerCase().includes(this.state.historiqueConsoFilter.toLowerCase()));
        filteredByExpression.sort(function(a,b){
          return new Date(b.entryYear,b.entryMonth,b.entryDay,b.entryHour,b.entryMinute,b.entrySecond) - new Date(a.entryYear,a.entryMonth,a.entryDay,a.entryHour,a.entryMinute,a.entrySecond);
        });
        return filteredByExpression.map(h =>(
            <HistoriqueRow key={h._id} entry={h} consomable={h.consomable}/>
        ))
      }
    }
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  handleFilter= e => {
    this.setState({
      historiqueConsoFilter : e.target.value
    })
  }

  loadHistoriqueConso = () => {
    this.props.client.query({
      query : this.state.historiqueConsoQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        historiqueConsoRaw:data.historiqueConso
      });
    })
  }
  
  showExport = () => {
    this.setState({ openExport:true })
  }  
  closeExport = () => {
    this.setState({ openExport: false})
  }

  componentDidMount = () => {
    this.loadHistoriqueConso();
  }

  export = () => {
    this.props.client.query({
      query : this.state.exportHistorique,
      variables:{
        month:this.state.month,
        year:this.state.year,
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      let exp = [];
      data.exportHistorique.map(h=>{
        let qtn;
        if(h.type.split("/")[1] === undefined){
          qtn = parseInt(h.quantity)
        }else{
          qtn = parseInt(h.type.split("/")[1] + h.quantity)
        }
        exp.push({cat:h.consomable.categorie,
          "Modèle":h.consomable.modele,
          "Désignation":h.consomable.designation,
          "Quantity":parseInt(qtn),
          "Date":h.entryDay.toString().padStart(2,'0')+"/"+(h.entryMonth+1).toString().padStart(2,'0')+"/"+h.entryYear,
          "Heure":h.entryHour.toString().padStart(2,'0')+":"+h.entryMinute.toString().padStart(2,'0')+":"+h.entrySecond.toString().padStart(2,'0'),
          "Note":h.note,
          "Type":h.type.split("/")[0]
        })
      });
      var ws = XLSX.utils.json_to_sheet(exp);
      var wb = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, "export "+(this.state.month+1).toString().padStart(2,'0')+' '+this.state.year);
      let wopts = { bookType:'xlsx', bookSST:false, type:'array' };
      let wbout = XLSX.write(wb,wopts);
      saveAs(new Blob([wbout],{type:"application/octet-stream"}), "export "+(this.state.month+1).toString().padStart(2,'0')+"/"+this.state.year+".xlsx");
    });
  }

  getYearsAvailables = () => {
    const minYear = this.state.historiqueConsoRaw.reduce((a,b)=>{if(b.entryYear<a){return(b.entryYear)}else{return(a)}},new Date().getFullYear());
    let years = [];
    for(let y=minYear;y<=new Date().getFullYear();y++){
      years.push({key:y,text:y,value:y})
    }
    return years;
  }

  render() {
    return (
      <Fragment>
        <div style={{height:"100%",padding:"8px",gridGap:"32px",display:"grid",gridTemplateRows:"auto 1fr",gridTemplateColumns:"auto 3fr 1fr"}}>
          <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
            <Menu.Item color="blue" name='Magasin' onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
            <Menu.Item color="blue" name='Stocks' onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
            <Menu.Item color="blue" name='Demandes' onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
            <Menu.Item color="blue" name='Commandes' onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
            <Menu.Item color="blue" name='Fournisseurs' onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase' />Fournisseurs</Menu.Item>
            <Menu.Item color="blue" name='Historique' active onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
          </Menu>
          <Input style={{placeSelf:"stretch"}} name="historiqueConsoFilter" onChange={this.handleFilter} icon='search' placeholder='Rechercher une entrée ...' />
          <Button color="blue" onClick={()=>{this.showExport()}} content='Exporter' icon='file alternate' labelPosition='right'/>
          <div style={{gridRowStart:"2",gridColumnEnd:"span 3",paddingTop:"32px",display:"block",overflowY:"auto",placeSelf:"stretch"}}>
            <Table celled selectable compact>
              <Table.Header>
                <Table.Row textAlign='center'>
                  <Table.HeaderCell width={2}>Date</Table.HeaderCell>
                  <Table.HeaderCell width={3}>Consomable</Table.HeaderCell>
                  <Table.HeaderCell width={2}>Quantité</Table.HeaderCell>
                  <Table.HeaderCell width={3}>Type</Table.HeaderCell>
                  <Table.HeaderCell width={7}>Note</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
            </Table>
            <Table celled selectable color="blue" compact>
              <Table.Body>
                {this.state.historiqueConso()}
              </Table.Body>
            </Table>
          </div>
        </div>
        <Modal open={this.state.openExport} onClose={this.closeExport}>
          <Modal.Header>
            Options d'export :
          </Modal.Header>
          <Modal.Content style={{fontSize:"1.4em"}}>
            <Segment textAlign="center">
              <div style={{display:"grid",gridTemplateColumns:"1fr 1fr 1fr 1fr"}}>
                <Header style={{placeSelf:"center",margin:"0"}} as="h3">Mois</Header>
                <Dropdown style={{placeSelf:"center"}} placeholder='Mois' onChange={(e,{value})=>{this.setState({month:value})}} dimmer="inverted" search selection options={this.state.months.map((m,i)=>{return({key:i,text:m,value:i})})} />
                <Header style={{placeSelf:"center",margin:"0"}} as="h3">Année</Header>
                <Dropdown style={{placeSelf:"center"}} placeholder='Année' onChange={(e,{value})=>{this.setState({year:value})}} dimmer="inverted" search selection options={this.getYearsAvailables()} />
              </div>
            </Segment>
            <Segment textAlign="center">
              <Header as="h3">Contenu</Header>
            </Segment>
          </Modal.Content>
          <Modal.Actions>
            <Button style={{fontSize:"1.4em"}} onClick={this.export} color="blue" animated='fade'>
              <Button.Content visible>Confirmer</Button.Content>
              <Button.Content hidden>
                <Icon name='file excel'/>
              </Button.Content>
            </Button>
          </Modal.Actions>
        </Modal>
      </Fragment>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(HistoriqueConso));