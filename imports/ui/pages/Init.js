import React, { Component, Fragment } from 'react';
import { Menu, Button, Segment, Header, Icon, Form, Progress, Message, Modal, List } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import gql from 'graphql-tag';
import 'moment/locale/fr';

export class Init extends Component {

  state = {
    rolesUsers:[],
    accounts:[],
    file:{name:"Aucun fichier choisi",size:0},
    consosDone:0,
    fournisseursDone:0,
    cleaningDone:0,
    consosTotal:1,
    fournisseursTotal:1,
    openReset:false,
    consos:[],
    fournisseurs:[],
    deletions:[false,false,false,false,false,false,false,false,false],
    collections:[
      "commandes",
      "consomables",
      "fournisseurs",
      "commandeConsos",
      "deliveries",
      "deliveryConsos",
      "demandeConsos",
      "historiqueConsos",
      "specifics"
    ],
    initConsoQuery:gql`
      mutation initConso($site:String!,$consomable:String!){
        initConso(site:$site,consomable:$consomable){
          consomables
          fournisseurs
        }
      }
    `,
    initFournisseurQuery:gql`
      mutation initFournisseur($site:String!,$fournisseur:String!){
        initFournisseur(site:$site,fournisseur:$fournisseur){
          consomables
          fournisseurs
        }
      }
    `,
    cleanSiteQuery:gql`
      mutation cleanSite($site:String!,$collection:String!){
        cleanSite(site:$site,collection:$collection)
      }
    `
  }

  fileInputRef = React.createRef();

  closeReset = () => {
    this.setState({
      openReset:false
    })
  }

  showReset = () => {
    this.setState({
      openReset:true
    })
  }

  fileChange = e => {
    let f = e.target.files[0];
    if (e.target.files.length <= 0) {
      return false;
    }
    var fr = new FileReader();
    fr.onload = e => { 
      var result = JSON.parse(e.target.result);
      let sup = [];
      result.map(x=>{
        if(!sup.includes(x['Fournisseurs'])){
          sup.push(x['Fournisseurs']);
        }
      });
      this.setState({
        fournisseurs:sup,
        fournisseursTotal:sup.length,
        consos:result,
        consosTotal:result.length
      })
    }
    fr.readAsText(f)
    this.setState({file:f})
  };

  writeConso = (index,lastIndex) => {
    this.props.client.mutate({
      mutation : this.state.initConsoQuery,
      variables:{
        site:this.props.site,
        consomable:JSON.stringify(this.state.consos[index])
      }
    }).then((data)=>{
      this.setState({
        consosDone:data.data.initConso.consomables
      })
      if(index+1 != lastIndex){
        setTimeout(()=>{this.writeConso(index+1,lastIndex)},10);
      }
    });
  }

  writeFournisseur = (index,lastIndex) => {
    this.props.client.mutate({
      mutation : this.state.initFournisseurQuery,
      variables:{
        site:this.props.site,
        fournisseur:this.state.fournisseurs[index]
      }
    }).then((data)=>{
      this.setState({
        fournisseursDone:data.data.initFournisseur.fournisseurs
      })
      if(index+1 != lastIndex){
        setTimeout(()=>{this.writeFournisseur(index+1,lastIndex)},100);
      }else{
        setTimeout(()=>{this.writeConso(0,this.state.consos.length)},500);
      }
    });
  }

  cleanSite = (index) => {
      this.props.client.mutate({
        mutation : this.state.cleanSiteQuery,
        variables:{
          site:this.props.site,
          collection:this.state.collections[index]
        }
      }).then((data)=>{
        let d = this.state.deletions;
        d[index]=true;
        this.setState({
          deletions:d
        });
        if(index+1 != this.state.collections.length){
          setTimeout(()=>{this.cleanSite(index+1)},500);
        }else{
          setTimeout(()=>{this.writeFournisseur(0,this.state.fournisseurs.length)},500);
        }
      });
  }

  resetSite = () => {
    this.closeReset();
    this.cleanSite(0);
  }

  getMenu = () => {
    if(this.props.user.isOwner){
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' onClick={()=>{this.props.history.push("/administration/roles")}} />
          <Menu.Item color="red" name='Réinitialisation' active onClick={()=>{this.props.history.push("/administration/init")}} />
          <Menu.Item color="green" name='Import/Export' onClick={()=>{this.props.history.push("/administration/impexp")}} />
        </Menu>
      )
    }else{
      return (
        <Menu style={{cursor:"pointer",marginBottom:"auto"}} vertical>
          <Menu.Item color="blue" name='Comptes' active onClick={()=>{this.props.history.push("/administration/accounts")}} />
          <Menu.Item color="blue" name='Rôles' onClick={()=>{this.props.history.push("/administration/roles")}} />
        </Menu>
      )
    }
  }

  render() {
    return (
      <Fragment>
        <div style={{display:"grid",gridGap:"16px",gridTemplateColumns:"auto 1fr"}}>
          {this.getMenu()}
          <Segment raised style={{gridColumnStart:"2",gridRowStart:"1",display:"grid",gridGap:"16px",padding:"32px",marginTop:"0",justifySelf:"stretch",gridTemplateColumns:"1fr 3fr"}}>
            <div style={{display:"grid",borderRight:"6px solid #74b9ff"}}>
              <Header style={{placeSelf:"center"}} as='h3' icon>
                <Icon circular name='erase'/>
                Réinitialiser le site {this.props.site}
              </Header>
            </div>
            <Form onSubmit={this.onFormSubmit} style={{display:"grid",gridGap:"8px 24px",gridTemplateColumns:"1fr 1fr 1fr",margin:"16px 48px 0 48px"}}>
              <div style={{margin:"0 0 16px 0",placeSelf:"stretch"}}>
                <Form.Field style={{width:"100%",height:"100%"}}>
                  <Button style={{width:"100%",height:"100%"}} content="choisir un fichier JSON" labelPosition="left" icon="file" onClick={() => this.fileInputRef.current.click()} />
                  <input ref={this.fileInputRef} type="file" hidden onChange={this.fileChange} />
                </Form.Field>
              </div>
              <Message style={{margin:"0 0 16px 0"}}>
                <div style={{display:"flex",justifyContent:"space-between"}}><span>fichier :</span><span>{this.state.file.name}</span></div>
                <hr/>
                <div style={{display:"flex",justifyContent:"space-between"}}><span>taille :</span><span>{(this.state.file.size/1024).toFixed(2)} Kb</span></div>
              </Message>
              <Button style={{placeSelf:"stretch",marginBottom:"16px"}} disabled={(this.state.file.size == 0 ? true : false)} type="submit" onClick={this.showReset} color="red" content={"Reinitialiser le site"}/>
              <div style={{placeSelf:"center end"}}>Suppression de l'ancien contenu</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"2",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='ratio' value={this.state.deletions.filter(x=>x).length} total={this.state.deletions.length} indicating/>
              <div style={{placeSelf:"center end"}}>Insertion des fournisseurs</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"3",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='value' value={parseFloat((this.state.fournisseursDone/this.state.fournisseursTotal)*100).toFixed(1)} total={100} label={this.state.fournisseursDone + "/" +this.state.fournisseurs.length + " fournisseurs"} indicating />
              <div style={{placeSelf:"center end"}}>Insertion des consomables</div>
              <Progress style={{marginTop:"4px",marginBottom:"16px",placeSelf:"center stretch",gridRowStart:"4",gridColumnStart:"2",gridColumnEnd:"span 2"}} progress='value' value={parseFloat((this.state.consosDone/this.state.consosTotal)*100).toFixed(1)} total={100} label={this.state.consosDone + "/" +this.state.consos.length + " consomables"} indicating />
            </Form>
          </Segment>
        </div>
        <Modal closeOnDimmerClick={false} dimmer="inverted" open={this.state.openReset} onClose={this.closeReset} closeIcon>
          <Modal.Header>
              Avertissement !
          </Modal.Header>
          <Modal.Content >
            <Message color="red">
              Poursuivre cette actions entrainera la réinitialisation totale du site {this.props.site}<br/>
              <hr/>
              Seront définitivement supprimés :
              <List bulleted as='ul'>
                <List.Item as='li'>Le catalogue de consomable du site</List.Item>
                <List.Item as='li'>Les fournisseurs de ces consomables</List.Item>
                <List.Item as='li'>
                  Les commandes du site, passées et actuelles
                  <List.List as='ul'>
                    <List.Item as='li'>Le contenu de ces commandes</List.Item>
                    <List.Item as='li'>Les livraisons de ces commandes</List.Item>
                  </List.List>
                </List.Item>
                <List.Item as='li'>
                  Les demandes de commande du site
                  <List.List as='ul'>
                    <List.Item as='li'>Leur quantité et provenance</List.Item>
                    <List.Item as='li'>Y compris les demandes hors-catalogues</List.Item>
                  </List.List>
                </List.Item>
                <List.Item as='li'>L'historique entier du site</List.Item>
              </List>
              Suite à cela le nouveau catalogue du site {this.props.site} sera peuplé à partir du contenu du fichier fourni.<br/>
              De même, les fournisseurs associés à ces consomables seront recréés à partir de ce même fichier.<br/>
              <hr/>
              NB : Les délai de livraisons seront à renseigner de nouveau dans l'onglet "consomables/fournisseurs"
            </Message>
          </Modal.Content>
          <Modal.Actions>
            <Button style={{fontSize:"1.4em"}} onClick={this.resetSite} color="red" animated='fade'>
              <Button.Content visible>Confirmer</Button.Content>
              <Button.Content hidden>
                  <Icon name='trash'/>
              </Button.Content>
            </Button>
          </Modal.Actions>
      </Modal>
    </Fragment>
    )
  }
}
const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Init);