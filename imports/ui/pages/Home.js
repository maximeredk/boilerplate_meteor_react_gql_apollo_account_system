import React, { Component } from 'react'
import { Button, Input, Icon, Modal, Form, Message, Image } from 'semantic-ui-react';
import { UserContext } from '../../contexts/UserContext';
import anime from 'animejs';

export class Home extends Component {

  state={
    email:"",
    pass:"",
    open:false,
    firstname:"",
    lastname:"",
    password:"",
    passwordAgain:"",
    error:false,
    errorContent:"",
    mail:""
  }

  setSite =  (e, { value })  => {
    this.props.setSite(value);
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  createAccount = error => {
    Accounts.createUser({
      email: this.state.mail,
      password: this.state.password,
      profile: {
          firstname: this.state.firstname,
          lastname: this.state.lastname
      },
      settings:{
        isAdmin:false,
        isOwner:false
      }
    },
    error=>{
        if(!error){
          this.props.client.resetStore();
        }
    });
  }

  loginUser = e => {
    e.preventDefault();
    Meteor.loginWithPassword(this.state.email, this.state.pass,
      error=>{
        if(!error){
          this.props.client.resetStore();
        }else{
          this.props.toast({
            message: error.reason,
            type: "error"
          });
        }
      }
    );
  }

  getModalLabel = ({error,errorContent}) => {
    if(error){
      return(
      <Modal.Actions style={{display:"flex",justifyContent:"space-between"}}>
        <Message style={{margin:"0"}} color="blue" content={errorContent}/>
        <Button disabled color="grey" size="small" style={{margin:"0 16px",cursor:"pointer"}} content='Créer' icon='plus' labelPosition='right'/>
      </Modal.Actions>
      )
    }else{
      return(
        <Modal.Actions>
          <Button color="blue" size="small" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.createAccount(error);this.close();}} content='Créer' icon='plus' labelPosition='right'/>
        </Modal.Actions>
      )
    }
  }

  componentDidMount = () => {
    anime({
      targets: '#titleLogo .squareC,.letterC',
      strokeDashoffset: [anime.setDashoffset, 0],
      easing: 'easeInOutSine',
      duration: 1500,
      delay: function(el, i) { return i * 250 },
      direction: 'normal',
      /*changeComplete: anim => {
        anime({
          targets: '#titleLogo .squareC',
          fill: '#E60033',
          easing: 'linear'
        });
        anime({
          targets: '#titleLogo .letterC',
          fill: '#003260',
          easing: 'linear'
        });
      }*/
    });
  }

  render() {
    const { firstname,lastname,password,passwordAgain,mail } = this.state;
    let error = false;
    let errorContent = "";
    if(password != passwordAgain){
      error = true;
      errorContent = "Les mots de passe sont different";
    }
    if(!mail.endsWith('@daher.com')){
      error = true;
      errorContent = "Le format de l'adresse mail n'est pas user@daher.com";
    }
    if(firstname == "" || lastname == "" || password == "" || passwordAgain == "" || mail == ""){
      error = true;
      errorContent = "Tous les champs doivent être renseignés";
    }
    if(this.props.user._id != null){
      return (
        <div style={{display:"grid",marginTop:"40px",gridTemplateColumns:"1fr 250px 480px 250px 1fr",gridTemplateRows:"400px 60px 60px 240px 80px",flexWrap:"wrap",justifyContent:"center",width:"100%"}}>
          <div style={{gridColumnStart:"2",gridColumnEnd:"span 3",placeSelf:"center"}}>
            <img src="res/title.png"/>
          </div>
          <p style={{placeSelf:"center",gridColumnStart:"2",gridColumnEnd:"span 3",gridRowStart:"4"}}>
            Optimisé pour Google Chome <Image style={{margin:"0 12px",display:"inline",width:"48px",height:"48px"}} src="/res/chrome.png"/>
            & Mozilla Firefox <Image style={{margin:"0 12px",display:"inline",width:"48px",height:"48px"}} src="/res/firefox.png"/>
          </p>
        </div>
      )
    }else{
      return (
        <div style={{display:"grid",gridTemplateColumns:"1fr 250px 480px 250px 1fr",marginTop:"40px",gridTemplateRows:"320px 80px 80px 64px 64px",flexWrap:"wrap",justifyContent:"center",width:"100%"}}>
          <div style={{gridColumnStart:"2",gridColumnEnd:"span 3",placeSelf:"center"}}>
            <svg id="titleLogo" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 850 337" space="preserve">
              <g id="title">
                <g><polygon id="square1" className="squareC" points="87,88 87,101 102,101 117,101 117,88 117,75 102,75 87,75 		"/></g>
                <g><polygon id="square2" className="squareC" points="144,88 144,101 159,101 174,101 174,88 174,75 159,75 144,75 		"/></g>
                <g><polygon id="square3" className="squareC" points="87,139.5 87,153 102,153 117,153 117,139.5 117,126 102,126 87,126 		"/></g>
                <g><path id="letter1" className="letterC" d="M144,139.4V153l30.3,0.3c28.7,0.3,30.5,0.5,35.9,2.6c8,3.2,15,9.9,19,18c3.2,6.4,3.3,7.2,3.3,18.2s-0.2,11.8-3.3,18.2c-7.4,15.1-19.8,20.7-45.8,20.8H174v-26.5V178h-15h-15v39.4l0,39.6l33.8,0l33.8-0.3l8.4-3c27-9.3,44.2-33.3,44.2-61.5c-0.1-29-15.5-51.6-42.1-61.2c-11.2-4.1-20-5-50.7-5H144V139.4z"/></g>
                <g><path id="letter2" className="letterC" d="M290.1,190.2c-15,35.3-28.3,66.8-28.3,66.8h15.9h14.9l5.8-14.2L304,229h26.4h26.4l5.7,13.8l5.8,14.2h16.4H400c0,0-13.6-32.3-28.5-67.4l-27-63.6h-13.6h-13.5L290.1,190.2z M346,202h-15.5H315l15.6-38.7L346,202z"/></g>
                <g><polygon id="letter3" className="letterC" points="412,191.5 412,257 426.5,257 441,257 441,231 441,205 467,205 493,205 493,231 493,257 507.5,257 522,257 522,191.5 522,126 507.5,126 493,126 493,152 493,178 467,178 441,178 441,152 441,126 426.5,126 412,126 		"/></g>
                <g><polygon id="letter4" className="letterC" points="550,191.5 550,257 600,257 650,257 650,244 650,231 615,231 580,231 580,218 580,205 610,205 640,205 640,191.5 640,178 610,178 580,178 580,165.5 580,153 614.5,153 649,153 649,139.5 649,126 599.5,126 550,126 		"/></g>
                <g><path id="letter5" className="letterC" d="M675,191.3V257h14.5H704v-20.5V216h10.8h10.9l13.6,20.5L753,257h17.1c9.4,0,17,0,17,0l-14.6-22.7c0,0-14.7-22-15-22.6c-0.2-0.7,2-2.5,5.1-4c15.1-8,23.2-25.6,20.5-45.1c-2.4-17-12.1-28.3-29-34c-6.1-2-9-2.2-42.8-2.6l-36.4-0.4V191.3z M740.8,154.5c6.2,1.9,10.8,6.2,12.2,11.7c2.3,8.4-1.3,16.7-9,20.5c-4.1,2.1-5.8,2.3-22.2,2.3H704v-18v-18h15.8C731.1,153,737.1,153.4,740.8,154.5z"/></g>
              </g>
            </svg>
          </div>
          <form style={{gridRowStart:"2",gridRowEnd:"span 3",gridColumnStart:"3",display:"grid",gridGap:"16px",gridTemplateRows:"1fr 1fr 1fr",alignSelf:"center"}} onSubmit={this.loginUser}>
            <Input onChange={this.handleChange} name="email" type="email" style={{justifySelf:"stretch",alignSelf:"center"}} icon="user" size='huge' placeholder='Adresse mail' />
            <Input onChange={this.handleChange} name="pass" type="password" style={{justifySelf:"stretch",alignSelf:"center"}} icon="key" size='huge' placeholder='Mot de passe' />
            <Button basic size="small" type="submit" style={{justifySelf:"stretch",alignSelf:"center",fontSize:"1.2em",cursor:"pointer"}} onClick={e=>{this.loginUser(e)}} color="blue" animated='fade'>
              <Button.Content visible>Connexion</Button.Content>
              <Button.Content hidden><Icon name='arrow right'/></Button.Content>
            </Button>
          </form>
          <Button basic size="small" style={{gridRowStart:"5",gridColumnStart:"3",alignSelf:"center",fontSize:"1.2em",justifyContent:"stretch",cursor:"pointer"}} onClick={()=>{this.popModal()}} color="blue" animated="fade">
            <Button.Content visible>Créer un compte</Button.Content>
            <Button.Content hidden><Icon name='edit outline'/></Button.Content>
          </Button>
          <Modal closeOnDimmerClick={false} size="small" open={this.state.open} onClose={this.close} closeIcon>
            <Modal.Header>
              Créer un compte :
            </Modal.Header>
            <Modal.Content >
              <Form autoComplete="off">
                <Form.Input readOnly={true} onFocus={e=>{e.target.removeAttribute('readonly')}} autoComplete="off" size="big" labelPosition="left" icon='user' label='Prénom' placeholder='Prénom' name="firstname" onChange={this.handleChange}/>
                <Form.Input readOnly={true} onFocus={e=>{e.target.removeAttribute('readonly')}} autoComplete="off" size="big" labelPosition="left" icon='user outline' label='Nom' placeholder='Nom' name="lastname" onChange={this.handleChange}/>
                <Form.Input readOnly={true} onFocus={e=>{e.target.removeAttribute('readonly')}} autoComplete="off" size="big" labelPosition="left" icon='mail' label='Mail' placeholder='Mail' name="mail" onChange={this.handleChange}/>
                <Form.Input readOnly={true} onFocus={e=>{e.target.removeAttribute('readonly')}} autoComplete="off" size="big" labelPosition="left" icon='key' type="password" label='Mot de passe' placeholder='Mot de passe' name="password" onChange={this.handleChange}/>
                <Form.Input readOnly={true} onFocus={e=>{e.target.removeAttribute('readonly')}} autoComplete="off" size="big" labelPosition="left" icon='key' type="password" label='Confirmez le mot de passe' placeholder='Confirmez le mot de passe' name="passwordAgain" onChange={this.handleChange}/>
              </Form>
            </Modal.Content>
            {this.getModalLabel({error:error,errorContent:errorContent})}
          </Modal>
        </div>
      )
    }
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(Home);