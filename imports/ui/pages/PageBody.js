import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom';
import Home from './Home';
import ConsomablesEPI from './ConsomablesEPI';
import Fournisseurs from './Fournisseurs';
import Stock from './Stock';
import Store from './Store';
import DemandesConso from './DemandesConso';
import Commandes from './Commandes';
import Compte from './Compte';
import Accounts from './Accounts';
import Roles from './Roles';
import Init from './Init';
import HistoriqueConso from './HistoriqueConso';
import Impexp from './Impexp';
import { UserContext } from '../../contexts/UserContext';
import { auto } from 'async';

class PageBody extends Component {

  getAvailableRoutes = () =>{
    if(!["AL1","AL2","FAL","SE","NPRS","EC","SUP"].includes(this.props.site)){
      return(
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/consomables/home' component={ConsomablesEPI}/>
          <Redirect from='*' to={'/'}/>
        </Switch>
      )
    }
    if(this.props.user.isAdmin){
      return(
        <Switch>
          <Route exact path='/' component={Home}/>

          <Route exact path='/consomables/home' component={ConsomablesEPI}/>
          <Route exact path='/consomables/magasin' component={Store}/>
          <Route exact path='/consomables/demandes' component={DemandesConso}/>
          <Route exact path='/consomables/stock' component={Stock}/>
          <Route exact path='/consomables/commandes' component={Commandes}/>
          <Route exact path='/consomables/fournisseurs' component={Fournisseurs}/>
          <Route exact path='/consomables/historique' component={HistoriqueConso}/>

          <Route exact path='/compte' component={Compte}/>
          
          <Route exact path='/administration/accounts' component={Accounts}/>
          <Route exact path='/administration/roles' component={Roles}/>
          <Route exact path='/administration/init' component={Init}/>
          <Route exact path='/administration/impexp' component={Impexp}/>
          <Redirect from='*' to={'/'}/>
        </Switch>
      );
    }else{
      return(
        <Switch>
          <Route exact path='/' component={Home}/>

          <Route exact path='/consomables/home' component={ConsomablesEPI}/>
          <Route exact path='/consomables/magasin' component={Store}/>
          <Route exact path='/consomables/demandes' component={DemandesConso}/>
          <Route exact path='/consomables/stock' component={Stock}/>
          <Route exact path='/consomables/commandes' component={Commandes}/>
          <Route exact path='/consomables/fournisseurs' component={Fournisseurs}/>
          <Route exact path='/consomables/historique' component={HistoriqueConso}/>

          <Route exact path='/compte' component={Compte}/>
          <Redirect from='*' to={'/'}/>
        </Switch>
      );
    }
  }

  render() {
    return (
      <div style={{
        width:"calc(100vw - 200px)",
        margin:"0 0 0 200px",
        padding:"32px 64px 32px 64px",
        display:"inline-block",
        backgroundColor: "#b8c6db",
        backgroundImage: "linear-gradient(315deg, #b8c6db 0%, #f5f7fa 74%)",
        backgroundRepeat:"no-repeat",
        backgroundAttachment:"fixed",
        height:"100vh"
      }}>
        {this.getAvailableRoutes()}
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default withUserContext(PageBody);