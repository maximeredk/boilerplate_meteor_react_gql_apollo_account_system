import React, { Component, Fragment } from 'react';
import { Pagination,Table,Button,Modal,Form,Input,Menu,Icon,Loader,Dimmer,Label,Dropdown } from 'semantic-ui-react';
import StockRow from '../molecules/StockRow';
import { UserContext } from '../../contexts/UserContext';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

class Stock extends Component {

  state={
    consomablesRaw:[],
    fournisseursMappedList:[],
    consomablesFilter:"",
    loading:true,
    delayBeforeEmergency:5,
    newRef:"",
    newDesignation:"",
    newRefS:"-",
    newCategorie:"",
    newModele:"",
    newPrix:0.00,
    newAvCS:0,
    newMinOrder:0,
    newFournisseur:"",
    newPackagingFormat:"unite",
    emergencyFilter:3,
    currentPage:1,
    rowByPage:10,
    maxPage:1,
    fournisseursQuery : gql`
      query Fournisseurs($site: String!){
        fournisseurs(site: $site){
          _id
          name
        }
      }
    `,
    consomablesQuery : gql`
      query Consomables($site: String!){
        consomables(site: $site){
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    removeConsomableQuery : gql`
      mutation removeConsomable($_id: String!,$site: String!){
        removeConsomable(_id: $_id,site: $site){
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    addConsomableQuery : gql`
      mutation addConsomable($ref:String!,$designation:String!,$site: String!,$refS:String!,$minOrder:Int!,$categorie:String!,$modele:String!,$prix:Float!,$avCS:Int!,$fournisseur:String!,$packagingFormat:String!){
        addConsomable(ref:$ref,designation:$designation,site:$site,refS:$refS,minOrder:$minOrder,categorie:$categorie,modele:$modele,prix:$prix,avCS:$avCS,fournisseur:$fournisseur,packagingFormat:$packagingFormat) {
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    editConsomableQuery : gql`
      mutation editConsomable($_id:String!,$ref:String!,$refS:String!,$minOrder:Int!,$categorie:String!,$designation:String!,$modele:String!,$avCS:Int!,$prix:Float!,$packagingFormat:String!,$fournisseur:String!,$site:String!){
        editConsomable(_id:$_id,ref:$ref,refS:$refS,minOrder:$minOrder,categorie:$categorie,designation:$designation,modele:$modele,avCS:$avCS,prix:$prix,packagingFormat:$packagingFormat,fournisseur:$fournisseur,site: $site) {
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    takeFromStockQuery : gql`
      mutation takeFromStock($_id:String!,$quantity:Int!,$isCorrection:Boolean!,$note:String!,$site: String!){
        takeFromStock(_id:$_id,quantity:$quantity,isCorrection:$isCorrection,note:$note,site: $site) {
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    addToStockQuery : gql`
      mutation addToStock($_id:String!,$quantity:Int!,$isCorrection:Boolean!,$note:String!,$site: String!){
        addToStock(_id:$_id,quantity:$quantity,isCorrection:$isCorrection,note:$note,site: $site) {
          _id
          ref
          refS
          minOrder
          categorie
          modele
          designation
          currentlyStored
          avCS
          packagingFormat
          prix
          fournisseur{
            _id
            name
            deliveryDelay
          }
        }
      }
    `,
    consomables: () =>{
      let displayed = Array.from(this.state.consomablesRaw);
      if(this.state.emergencyFilter < 3){
        displayed = displayed.filter(c=>c.emergencyCode == this.state.emergencyFilter)
      }
      if(this.state.consomablesFilter.length>1){
        displayed = displayed.filter(c => c.designation.toLowerCase().includes(this.state.consomablesFilter.toLowerCase())||c.ref.toLowerCase().includes(this.state.consomablesFilter.toLowerCase())||c.refS.toLowerCase().includes(this.state.consomablesFilter.toLowerCase())||c.categorie.toLowerCase().includes(this.state.consomablesFilter.toLowerCase())||c.modele.toLowerCase().includes(this.state.consomablesFilter.toLowerCase()));
        if(displayed.length == 0){
          return(
            <Table.Row key={"none"}>
              <Table.Cell width={16} colSpan='8' textAlign="center">Aucun consomable</Table.Cell>
            </Table.Row>
          )
        }
      }
      displayed = displayed.slice((this.state.currentPage - 1) * this.state.rowByPage, this.state.currentPage * this.state.rowByPage);
      return displayed.map(c =>(
        <StockRow key={c._id} takeFromStock={this.takeFromStock} addToStock={this.addToStock} fournisseursMappedList={this.state.fournisseursMappedList} removeConsomable={this.removeConsomable} editConsomable={this.editConsomable} consomable={c}/>
      ))
    }
  }

  popModal = () => {
    this.setState({
        open:true
    })
  }

  close = () => {
    this.setState({
        open:false
    })
  }

  handleChange = e => {
    this.setState({
        [e.target.name] : e.target.value
    })
  }

  handleFilter= e => {
    this.setState({
      consomablesFilter : e.target.value
    })
  }

  handleFournisseurSelection = (e,{value}) =>{
    this.setState({
        newFournisseur:value
    });
}

  loadFournisseurs = () => {
    this.props.client.query({
      query : this.state.fournisseursQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      this.setState({
        fournisseursMappedList:data.fournisseurs.map(x => { return {value:x._id,text:x.name}})
      })
    })
  }

  loadConsomables = () => {
    this.props.client.query({
      query : this.state.consomablesQuery,
      variables:{
        site:this.props.site
      },
      fetchPolicy:"network-only"
    }).then(({data})=>{
      data.consomables.map(c=>{
        if(c.currentlyStored <= (c.fournisseur.deliveryDelay*c.avCS)){
            c.emergencyCode = 0;
        }
        if(((c.currentlyStored <= ((c.fournisseur.deliveryDelay+7)*c.avCS)) && (c.currentlyStored > (c.fournisseur.deliveryDelay*c.avCS)))){
            c.emergencyCode = 1;
        }
        if((c.currentlyStored > ((c.fournisseur.deliveryDelay+7)*c.avCS))){
            c.emergencyCode = 2;
        }
        if(c.currentlyStored/c.avCS > 1){
          c.left = "environ " + Math.floor(c.currentlyStored/c.avCS).toString() + " j.";
        }else{
          c.left = "moins d'un jour";
        }
      })
      this.setState({
        consomablesRaw:data.consomables,
        maxPage:Math.ceil(data.consomables.length / this.state.rowByPage),
        loading:false
      });
    })
  }

  handlePaginationChange = (e, { activePage }) => {
    this.setState({ currentPage:activePage })
  }

  setEmergencyFilter = emergency => {
    this.setState({
      emergencyFilter : emergency
    })
  }

  addConsomable = () => {
    this.close();
    this.props.client.mutate({
      mutation : this.state.addConsomableQuery,
      variables:{
        ref:this.state.newRef,
        designation:this.state.newDesignation,
        refS:this.state.newRefS,
        categorie:this.state.newCategorie,
        modele:this.state.newModele,
        prix:parseFloat(this.state.newPrix),
        avCS:parseInt(this.state.newAvCS),
        minOrder:parseInt(this.state.newMinOrder),
        fournisseur:this.state.newFournisseur,
        packagingFormat:this.state.newPackagingFormat,
        site:this.props.site
      }
    }).then(({data})=>{
      this.loadConsomables();
    })
  }

  removeConsomable = _id => {
    this.props.client.mutate({
        mutation : this.state.removeConsomableQuery,
        variables:{
            _id:_id,
            site:this.props.site
        }
    }).then(({data})=>{
      this.loadConsomables();
    })
  }

  editConsomable = (_id,ref,refS,minOrder,categorie,designation,modele,currentlyStored,avCS,prix,packagingFormat,fournisseur) => {
    this.props.client.mutate({
        mutation : this.state.editConsomableQuery,
        variables:{
          _id:_id,
          ref:ref,
          refS:refS,
          minOrder:parseInt(minOrder),
          categorie:categorie,
          designation:designation,
          modele:modele,
          currentlyStored:parseInt(currentlyStored),
          avCS:parseInt(avCS),
          prix:parseFloat(prix),
          packagingFormat:packagingFormat,
          fournisseur:fournisseur,
          site:this.props.site
        }
    }).then(()=>{
        this.loadConsomables();
    });
  }

  takeFromStock = (_id,quantity,isCorrection,note) => {
    this.props.client.mutate({
      mutation : this.state.takeFromStockQuery,
      variables:{
        _id:_id,
        quantity:parseInt(quantity),
        isCorrection:isCorrection,
        note:note,
        site:this.props.site
      }
    }).then(()=>{
        this.loadConsomables();
    });
  }

  addToStock = (_id,quantity,isCorrection,note) => {
    this.props.client.mutate({
      mutation : this.state.addToStockQuery,
      variables:{
        _id:_id,
        quantity:parseInt(quantity),
        isCorrection:isCorrection,
        note:note,
        site:this.props.site
      }
    }).then(()=>{
        this.loadConsomables();
    });
  }

  componentDidMount = () => {
    this.loadConsomables();
    this.loadFournisseurs();
  }

  render() {
    return (
      <Fragment>
        <div style={{height:"100%",padding:"8px",display:"grid",gridGap:"32px",gridTemplateRows:"auto 1fr auto",gridTemplateColumns:"auto 2fr 2fr 1fr"}}>
          <Menu style={{cursor:"pointer",marginBottom:"auto"}} icon='labeled'>
            <Menu.Item color="blue" name='Magasin' onClick={()=>{this.props.history.push("/consomables/magasin")}}><Icon name='shopping cart'/>Magasin</Menu.Item>
            <Menu.Item color="blue" name='Stocks' active onClick={()=>{this.props.history.push("/consomables/stock")}}><Icon name='chart bar'/>Stocks</Menu.Item>
            <Menu.Item color="blue" name='Demandes' onClick={()=>{this.props.history.push("/consomables/demandes")}}><Icon name='talk'/>Demandes</Menu.Item>
            <Menu.Item color="blue" name='Commandes' onClick={()=>{this.props.history.push("/consomables/commandes")}} ><Icon name='shipping' />Commandes</Menu.Item>
            <Menu.Item color="blue" name='Fournisseurs' onClick={()=>{this.props.history.push("/consomables/fournisseurs")}} ><Icon name='briefcase' />Fournisseurs</Menu.Item>
            <Menu.Item color="blue" name='Historique' onClick={()=>{this.props.history.push("/consomables/historique")}} ><Icon name='calendar alternate outline' />Historique</Menu.Item>
          </Menu>
          <Input style={{justifySelf:"stretch"}} name="consomablesFilter" onChange={this.handleFilter} icon='search' placeholder='Rechercher un consomable ...' />
          <div style={{placeSelf:"stretch",display:"grid",gridTemplateColumns:"1fr 1fr 1fr 1fr",gridTemplateRows:"1fr 1fr",gridGap:"6px"}}>
            <Label style={{alignSelf:"center",lineHeight:"1.8em",gridColumnEnd:"span 4"}} horizontal>Filtrer selon l'urgence</Label>
            <Label style={{alignSelf:"center",lineHeight:"1.8em",cursor:"pointer"}} onClick={()=>{this.setEmergencyFilter(3)}} color='black' horizontal>Tous</Label>
            <Label style={{alignSelf:"center",lineHeight:"1.8em",cursor:"pointer"}} onClick={()=>{this.setEmergencyFilter(2)}} color='green' horizontal>Ok</Label>
            <Label style={{alignSelf:"center",lineHeight:"1.8em",cursor:"pointer"}} onClick={()=>{this.setEmergencyFilter(1)}} color='orange' horizontal>Besoin</Label>
            <Label style={{alignSelf:"center",lineHeight:"1.8em",cursor:"pointer"}} onClick={()=>{this.setEmergencyFilter(0)}} color='red' horizontal>Urgent</Label>
          </div>
          <Button color="blue" onClick={()=>{this.popModal()}} content='Ajouter un consomable' icon='plus' labelPosition='right'/>
          <div style={{gridRowStart:"2",gridColumnEnd:"span 4",display:"block",overflowY:"auto",placeSelf:"stretch"}}>
            <Table color="blue" celled selectable compact>
              <Table.Header>
                <Table.Row textAlign='center'>
                  <Table.HeaderCell width={2}>Stock</Table.HeaderCell>
                  <Table.HeaderCell width={3}>Réference</Table.HeaderCell>
                  <Table.HeaderCell width={4}>Designation</Table.HeaderCell>
                  <Table.HeaderCell width={2}>Prix</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Délai de livraison</Table.HeaderCell>
                  <Table.HeaderCell width={1}>Consomation journalière</Table.HeaderCell>
                  <Table.HeaderCell width={2}>Actions</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {this.state.consomables()}
              </Table.Body>
            </Table>
          </div>
          <Pagination style={{placeSelf:"center",gridColumnEnd:"span 4"}} onPageChange={this.handlePaginationChange} defaultActivePage={this.state.currentPage} firstItem={null} lastItem={null} pointing secondary totalPages={this.state.maxPage}/>
        </div>
        <Dimmer inverted active={this.state.loading}>
          <Loader size='massive'>Chargement des stocks ...</Loader>
        </Dimmer>
        <Modal dimmer="inverted" size="large" open={this.state.open} onClose={this.close}>
          <Modal.Header>
            Ajout d'un nouveau consomable :
          </Modal.Header>
          <Modal.Content>
          <Form style={{display:"grid",gridGap:"16px",gridTemplateColumns:"1fr 1fr 1fr"}} onSubmit={e=>{e.preventDefault();this.addConsomable()}}>
            <div style={{gridArea:"1/1/1/span 1"}}><Form.Input size="big" label='Réference' placeholder='Réference' name='newRef' onChange={this.handleChange}/></div>
            <div style={{gridArea:"1/2/1/span 1"}}><Form.Input size="big" label='Réference Fournisseur' placeholder='Réference' name='newRefS' onChange={this.handleChange}/></div>
            <div style={{gridArea:"1/3/1/span 1"}}><Dropdown clearable style={{fontSize:"1.4em",marginTop:"22px",height:"53px",width:"100%"}} placeholder="Définir un fournisseur ..." onChange={this.handleFournisseurSelection} fluid search scrolling selection options={this.state.fournisseursMappedList} /></div>
            <div style={{gridArea:"2/1/2/span 1"}}><Form.Input size="big" label='Catégorie' placeholder='Catégorie' name='newCategorie' onChange={this.handleChange}/></div>
            <div style={{gridArea:"2/2/2/span 1"}}><Form.Input size="big" label='Désignation' placeholder='Désignation' name='newDesignation' onChange={this.handleChange}/></div>
            <div style={{gridArea:"2/3/2/span 1"}}><Form.Input size="big" label='Modèle' placeholder='Modèle' name='newModele' onChange={this.handleChange}/></div>
            <div style={{gridArea:"3/1/3/span 1"}}><Form.Input size="big" type="number" label='Consomation journalière moyenne' placeholder='Consomation journalière moyenne' name='newAvCS' onChange={this.handleChange}/></div>
            <div style={{gridArea:"3/2/3/span 1"}}><Form.Input size="big" type="number" label='Prix' placeholder='Prix' name='newPrix' onChange={this.handleChange}/></div>
            <div style={{gridArea:"3/3/3/span 1"}}><Form.Input size="big" type="number" label='Seuil de commande minimum' placeholder='Seuil de commande minimum' name='newMinOrder' onChange={this.handleChange}/></div>
            <div style={{gridArea:"4/1/4/span 3"}}><Form.Input size="big" label='Conditionnement' placeholder='Conditionnement' name='newPackagingFormat' onChange={this.handleChange}/></div>
          </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="blue" style={{margin:"0 16px",cursor:"pointer"}} onClick={()=>{this.addConsomable()}} content='Ajouter' icon='plus' labelPosition='right'/>
          </Modal.Actions>
        </Modal>
      </Fragment>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(Stock));