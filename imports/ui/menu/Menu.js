import React, { Component } from 'react'
import MenuItemList from './MenuItemList';
import { Link,withRouter } from 'react-router-dom';
import { UserContext } from '../../contexts/UserContext';

class Menu extends Component {

  state={
    menuItems:[
      {
        name:"workflow",
        label:"Workflow",
        display:false
      },{
        name:"achats",
        label:"Achats",
        display:false
      },{
        name:"consomables/home",
        label:"Consommables & EPI",
        display:true
      },{
        name:"collaborateurs/"+new Date().getFullYear()+"/"+parseInt(new Date().getMonth()+1),
        label:"Collaborateurs",
        display:false
      },{
        name:"compte",
        label:"Compte",
        display:true
      }
    ],
    menuItemsAdmin:[
      {
        name:"workflow",
        active:"workflow",
        label:"Workflow",
        display:false
      },{
        name:"achats",
        active:"achats",
        label:"Achats",
        display:false
      },{
        name:"consomables/home",
        active:"consomables",
        label:"Consommables & EPI",
        display:true
      },{
        name:"collaborateurs/"+new Date().getFullYear()+"/"+parseInt(new Date().getMonth()+1),
        active:"collaborateurs",
        label:"Collaborateurs",
        display:false
      },{
        name:"compte",
        active:"compte",
        label:"Compte",
        display:true
      },{
        name:"administration/accounts",
        active:"administration",
        label:"Administration",
        display:true
      }
    ]
  }

  getMenuItemsList = () =>{
    if(this.props.user.isAdmin){
        return (this.state.menuItemsAdmin);
    }else{
        return (this.state.menuItems);
      }
  }

  setSite =  (e, { value })  => {
    this.props.setSite(value);
  }

  getSiteLabel = () => {
    const selected = this.props.sites.find(x=>x.key == this.props.site)
    if(selected === undefined){
      return "Aucun site selectionné";
    }else{
      return selected.text;
    }
  }

  render() {
    return (
      <div style={{
        width:"200px",
        height:"100vh",
        position:"fixed",
        display:"inline-block",
        backgroundImage:"linear-gradient(315deg, #ffffff 0%, #d7e1ec 74%)",
        boxShadow:"1px 0 5px black"
      }}>
          <ul style={{marginTop:"32px",padding:"0",listStyle:"none",textAlign:"center"}}>
            <Link to={"/"} style={{ textDecoration: 'none' }}>
              <li style={{cursor:"pointer"}} name="avatarWrapper">
                <img alt="homeLogo" style={{width:"128px"}} src='/res/topMenu3.png'/>
              </li>
            </Link>
            <hr style={{width:"80%"}}/>
            <p style={{fontSize:"1.2em",marginTop:"16px",fontFamily:"'Quicksand', sans-serif",color:"#4183C4",fontWeight:"bold",cursor:"pointer"}} onClick={()=>{this.props.history.push("/consomables/home")}}>Site : {this.getSiteLabel()}</p>
            <hr style={{width:"80%",marginBottom:"32px"}}/>
            <MenuItemList site={this.props.site} menuItems={this.getMenuItemsList()}/>
            <li onClick={()=>{Meteor.logout();this.props.client.resetStore();this.props.history.push("/")}} className={"menuItemRed"} style={{cursor:"pointer"}}>
              <p style={{textAlign:"right",padding:"8px 16px 8px 0",fontSize:"1.1em",fontWeight:"800",fontFamily: "'Open Sans', sans-serif"}}>
                DECONNEXION
              </p>
            </li>
          </ul>
      </div>
    )
  }
}

const withUserContext = WrappedComponent => props => (
  <UserContext.Consumer>
      {ctx => <WrappedComponent {...ctx} {...props}/>}
  </UserContext.Consumer>
)

export default wrappedInUserContext = withUserContext(withRouter(Menu));